package org.kvj.pebble.tiles.android.http;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import org.json.JSONException;
import org.json.JSONObject;
import org.kvj.pebble.tiles.android.http.log.WebBridgeLogger;

import fi.iki.elonen.NanoHTTPD;
import fi.iki.elonen.NanoHTTPD.Response.Status;

public class WebBridgeServer extends NanoHTTPD implements WebBridgeLogger {

	private static int maxIndex = 0;
	private List<IncomingConnection> connections = new ArrayList<IncomingConnection>();

	WebBridgeRequestHandler handler = null;

	class IncomingConnection {

		private boolean connected = true;
		private InputStream stream;
		StringBuilder buffer = new StringBuilder();
		int pos = 0;
		int index = ++maxIndex;

		Timer pingTimer = new Timer();

		public IncomingConnection(IHTTPSession session) {
			TimerTask task = new TimerTask() {

				@Override
				public void run() {
					if (connected) {
						// Ping
						// log(LogLevel.Debug, "Ping:", index);
						send(":\n");
					}
				}
			};
			pingTimer.schedule(task, PING_SECONDS * 1000, PING_SECONDS * 1000);
			stream = new InputStream() {

				boolean stopReading = false;

				@Override
				public int available() throws IOException {
					synchronized (buffer) {
						// log(LogLevel.Debug, "Available:", buffer.length() -
						// pos);
						return buffer.length() - pos;
					}
				}

				@Override
				public int read(byte[] b, int off, int len) throws IOException {
					// log(LogLevel.Debug, "Will read to buffer:", index, off,
					// len);
					stopReading = false;
					int bytes = 0;
					for (int i = 0; i < len; i++) {
						int data = read();
						if (-1 == data) {
							return bytes;
						}
						b[i + off] = (byte) data;
						bytes++;
						if (stopReading) {
							return bytes;
						}
					}
					return bytes;
				}

				@Override
				public boolean markSupported() {
					return false;
				}

				@Override
				public void close() throws IOException {
					// log(LogLevel.Debug, "Closed stream", index);
					connected = false;
					int cCount = 0;
					synchronized (connections) {
						cCount = connections.size();
						connections.remove(IncomingConnection.this);
					}
					if (null != handler) {
						handler.connectionsChanged(cCount, getConnectionsCount());
					}
					pingTimer.cancel();
					synchronized (buffer) {
						buffer.notify();
					}
					super.close();
				}

				@Override
				public int read() throws IOException {
					if (!connected) {
						// log(LogLevel.Debug, "Reject read - closed");
						return -1;
					}
					synchronized (buffer) {
						if (pos >= buffer.length()) {
							// No data - sleep
							try {
								// log(LogLevel.Debug, "Read will sleep");
								buffer.wait();
								if (!connected) {
									// log(LogLevel.Debug,
									// "Sleep done - closed");
									return -1;
								}
							} catch (InterruptedException e) {
								// log(LogLevel.Debug,
								// "Sleep done - terminated");
								return -1;
							}
						}
						// Have data
						int data = buffer.charAt(pos);
						pos++;
						if (pos == buffer.length()) {
							// End of buffer reached
							stopReading = true;
							buffer.setLength(0);
							pos = 0;
						}
						// log(LogLevel.Debug, "Sleep done - send data:", data);
						return data;
					}
				}
			};
			int cCount = 0;
			synchronized (connections) {
				cCount = connections.size();
				connections.add(IncomingConnection.this);
			}
			if (null != handler) {
				handler.connectionsChanged(cCount, getConnectionsCount());
			}
			// log(LogLevel.Debug, "New connection:", index);
			task.run();
		}

		public InputStream getStream() {
			return stream;
		}

		public boolean send(String text) {
			if (!connected) {
				log(LogLevel.Warning, "Attempt to write to closed stream:",
						text);
				return false;
			}
			synchronized (buffer) {
				buffer.append(text);
				buffer.notify();
			}
			return true;
		}
	}

	private WebBridgeLogger logger = null;

	private String streamUri = "/stream";
	private static final String CORS_ALLOW_ORIGIN = "Access-Control-Allow-Origin";
	private static final String CORS_ALLOW_HEADERS = "Access-Control-Allow-Headers";
	private static final String CORS_ALLOW_METHODS = "Access-Control-Allow-Methods";
	private static final String CORS_ALLOW_HEADERS_VAL = "accept,content-type";
	private static final String CORS_ALLOW_METHODS_VAL = "GET,HEAD,PUT,POST,DELETE";
	private static final int PING_SECONDS = 30;

	private static final Object CORS_ORIGIN = "Origin";

	public WebBridgeServer(String hostname, int port) {
		super(hostname, port);
	}

	public void setLogger(WebBridgeLogger logger) {
		this.logger = logger;
	}

	@Override
	public void log(LogLevel level, Object... values) {
		if (null != logger) {
			logger.log(level, values);
		}
	}

	@Override
	public void log(LogLevel level, Throwable t, Object... values) {
		if (null != logger) {
			logger.log(level, t, values);
		}
	}

	@Override
	public void start() throws IOException {
		super.start();
		log(LogLevel.Info, "Server start");
	}

	private JSONObject readFromStream(InputStream stream) throws IOException {
		StringBuilder sb = new StringBuilder();
		byte[] array = new byte[stream.available()];
		int size = -1;
		// log(LogLevel.Debug, "Incoming JSON:", "start, about to read",
		// stream.available());
		while ((size = stream.read(array)) > 0) {
			sb.append(new String(array, 0, size, "utf-8"));
			try {
				return new JSONObject(sb.toString());
			} catch (JSONException e) {
				// Not yet
			}
		}
		return null;
	}

	@Override
	public Response serve(IHTTPSession session) {
		log(LogLevel.Debug, "Incoming connection:", session.getMethod(),
				session.getUri(), session.getHeaders());
		String origin = session.getHeaders().get(CORS_ORIGIN);
		if (null == origin) {
			origin = "*";
		}
		if (session.getMethod() == Method.OPTIONS) {
			// CORS request?
			Response resp = new Response(Status.NO_CONTENT, null, "");
			resp.addHeader(CORS_ALLOW_ORIGIN, origin);
			resp.addHeader(CORS_ALLOW_HEADERS, CORS_ALLOW_HEADERS_VAL);
			resp.addHeader(CORS_ALLOW_METHODS, CORS_ALLOW_METHODS_VAL);
			return resp;
		}
		if (streamUri.equals(session.getUri())) {
			// Start streaming
			IncomingConnection conn = new IncomingConnection(session);
			Response resp = new Response(Status.OK, "text/event-stream",
					conn.getStream());
			resp.setChunkedTransfer(true);
			resp.addHeader(CORS_ALLOW_ORIGIN, origin);
			return resp;
		}
		if (null == handler) {
			// No handler
			return new Response(Status.INTERNAL_ERROR, "text/plain",
					"No handler set");
		}
		Response resp = null;
		try {
			// Everything else is JSON
			JSONObject request = null;
			if (session.getMethod() == Method.POST) {
				// Have body
				request = readFromStream(session.getInputStream());
			}
			JSONObject response = handler.serve(request, session.getUri());
			resp = new Response(Status.OK,
					"application/json; charset=UTF-8",
					response.toString());
		} catch (Exception e) {
			log(LogLevel.Error, e, "Error in Rest");
			resp = new Response(Status.INTERNAL_ERROR, "text/plain",
					"Error on Server side");
		}
		resp.addHeader(CORS_ALLOW_ORIGIN, origin);
		return resp;
	}

	public int getConnectionsCount() {
		return connections.size();
	}

	public void send(JSONObject message) {
		synchronized (connections) {
			log(LogLevel.Info, "Sending data to connections:",
					getConnectionsCount());
			for (IncomingConnection conn : connections) {
				conn.send("data:" + message.toString() + "\n\n");
			}
		}
	}

	@Override
	public void setLogLevel(LogLevel level) {
	}

	public void setHandler(WebBridgeRequestHandler handler) {
		this.handler = handler;
	}

}
