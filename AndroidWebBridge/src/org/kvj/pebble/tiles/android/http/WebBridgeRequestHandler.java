package org.kvj.pebble.tiles.android.http;

import org.json.JSONException;
import org.json.JSONObject;

public interface WebBridgeRequestHandler {

	public JSONObject serve(JSONObject data, String query) throws JSONException;

	public void connectionsChanged(int from, int connections);
}
