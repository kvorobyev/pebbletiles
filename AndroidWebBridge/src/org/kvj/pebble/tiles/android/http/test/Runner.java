package org.kvj.pebble.tiles.android.http.test;

import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;

import org.json.JSONException;
import org.json.JSONObject;
import org.kvj.pebble.tiles.android.http.WebBridgeRequestHandler;
import org.kvj.pebble.tiles.android.http.WebBridgeServer;
import org.kvj.pebble.tiles.android.http.log.WebBridgeLogger.LogLevel;
import org.kvj.pebble.tiles.android.http.log.impl.StandardStreamLogger;

public class Runner {

	public static void main(String[] args) {
		final WebBridgeServer server = new WebBridgeServer(null, 7777);
		StandardStreamLogger logger = new StandardStreamLogger();
		logger.setLogLevel(LogLevel.Info);
		server.setLogger(logger);
		server.setHandler(new WebBridgeRequestHandler() {

			@Override
			public JSONObject serve(JSONObject data, String query)
					throws JSONException {
				server.log(LogLevel.Info, "Incoming Rest:", query, data);
				JSONObject obj = new JSONObject();
				obj.put("rest", query);
				return obj;
			}

			@Override
			public void connectionsChanged(int from, int connections) {
				// TODO Auto-generated method stub

			}
		});
		try {
			server.start();
		} catch (IOException e1) {
			server.log(LogLevel.Error, e1, "Error starting");
			return;
		}
		TimerTask json = new TimerTask() {

			@Override
			public void run() {
				server.send(new JSONObject());
			}
		};
		Timer timer = new Timer();
		timer.schedule(json, 1000, 15000);
		synchronized (server) {
			try {
				server.wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}
