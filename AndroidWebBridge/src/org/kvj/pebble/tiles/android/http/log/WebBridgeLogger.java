package org.kvj.pebble.tiles.android.http.log;

public interface WebBridgeLogger {

	public enum LogLevel {
		Debug, Info, Warning, Error
	};

	public void setLogLevel(LogLevel level);

	public void log(LogLevel level, Object... values);

	public void log(LogLevel level, Throwable t, Object... values);
}
