package org.kvj.pebble.tiles.android.http.log.impl;

import org.kvj.pebble.tiles.android.http.log.WebBridgeLogger;

public class StandardStreamLogger implements WebBridgeLogger {

	private LogLevel maxLevel = LogLevel.Debug;

	private void toStringBuilder(StringBuilder sb, Object[] values) {
		for (Object obj : values) {
			sb.append(' ');
			if (null == obj) {
				sb.append("NULL");
			} else {
				sb.append(obj.toString());
			}
		}
	}

	@Override
	public void log(LogLevel level, Object... values) {
		if (level.ordinal() < maxLevel.ordinal()) {
			return;
		}
		StringBuilder sb = new StringBuilder();
		sb.append('[');
		sb.append(level.toString());
		sb.append(']');
		toStringBuilder(sb, values);
		if (level == LogLevel.Error) {
			System.err.println(sb);
		} else {
			System.out.println(sb);
		}
	}

	@Override
	public void log(LogLevel level, Throwable t, Object... values) {
		log(level, values);
		t.printStackTrace();
	}

	@Override
	public void setLogLevel(LogLevel level) {
		maxLevel = level;
	}

}
