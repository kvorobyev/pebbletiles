package org.kvj.android.webappbridge.notification;

import android.os.Build;
import android.service.notification.NotificationListenerService;
import android.service.notification.StatusBarNotification;

import org.kvj.android.webappbridge.WebBridgeApp;
import org.kvj.android.webappbridge.pebble.plugin.impl.NotificationPlugin;
import org.kvj.android.webappbridge.svc.WebBridgeController;
import org.kvj.bravo7.ApplicationContext;
import org.kvj.bravo7.ng.App;

/**
 * Created by vorobyev on 3/6/15.
 */
public class JBNotificationListener extends NotificationListenerService
    implements WebBridgeController.NotificationCancelListener {

    private WebBridgeController controller = App.controller();

    @Override
    public void onCreate() {
        super.onCreate();
        controller.setNotificationCancelListener(this);
    }

    @Override
    public void onNotificationPosted(StatusBarNotification sbn) {
        NotificationPlugin
            plugin = (NotificationPlugin) controller.getPebble().findPlugin(NotificationPlugin.ID);
        plugin.newNotification(sbn, true);
    }

    @Override
    public void onNotificationRemoved(StatusBarNotification sbn) {
        NotificationPlugin
                plugin = (NotificationPlugin) controller.getPebble().findPlugin(NotificationPlugin.ID);
        plugin.newNotification(sbn, false);
    }

    @Override
    public void cancel(StatusBarNotification notification) {
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            cancelNotification(notification.getKey());
        } else {
            cancelNotification(notification.getPackageName(), notification.getTag(),
                               notification.getId());
        }
    }

}
