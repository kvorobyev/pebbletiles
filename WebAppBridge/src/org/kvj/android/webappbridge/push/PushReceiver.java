package org.kvj.android.webappbridge.push;

import android.content.Context;
import android.content.Intent;

import com.parse.ParsePushBroadcastReceiver;

import org.json.JSONObject;
import org.kvj.android.webappbridge.WebBridgeApp;
import org.kvj.android.webappbridge.pebble.PebbleController;
import org.kvj.android.webappbridge.svc.WebBridgeController;
import org.kvj.bravo7.log.Logger;
import org.kvj.bravo7.ng.App;
import org.kvj.bravo7.util.Listeners;

/**
 * Created by vorobyev on 11/26/14.
 */
public class PushReceiver extends ParsePushBroadcastReceiver {

    Logger logger = Logger.forInstance(this);
    WebBridgeController controller = App.controller();

    @Override
    protected void onPushReceive(Context context, Intent intent) {
        String channel = intent.getStringExtra(KEY_PUSH_CHANNEL);
        String object = intent.getStringExtra(KEY_PUSH_DATA);
        logger.d("Push:", channel, object);
        try {
            JSONObject json = new JSONObject(object);
            processMessage(json);
        } catch (Exception e) {
            logger.e(e, "Failed to parse JSON");
        }
        // super.onPushReceive(context, intent);
    }

    private void processMessage(JSONObject json) {
        String type = json.optString("type", "message");
        if ("message".equals(type)) {
            String dest = json.optString("dest", "pebble");
            String name = json.optString("name", "");
            String title = json.optString("title", "Untitled");
            String message = json.optString("message", "");
            int vibro = json.optInt("vibro", -1);
            controller.processMessage(vibro, dest, name, title, message);
        }
        if ("measure".equals(type)) {
            logger.d("Received measure:", json);
            final String id = json.optString("id");
            final JSONObject measure = json.optJSONObject("measure");
            controller.getPebble().measureListeners().emit(
                new Listeners.ListenerEmitter<PebbleController.MeasureListener>() {
                    @Override
                    public boolean emit(PebbleController.MeasureListener listener) {
                        listener.measure(id, measure);
                        return true;
                    }
                });
        }
    }
}
