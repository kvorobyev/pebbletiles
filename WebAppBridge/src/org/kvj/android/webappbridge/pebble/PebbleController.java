package org.kvj.android.webappbridge.pebble;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.support.v4.app.NotificationCompat;

import com.getpebble.android.kit.PebbleKit;
import com.getpebble.android.kit.PebbleKit.PebbleAckReceiver;
import com.getpebble.android.kit.PebbleKit.PebbleDataReceiver;
import com.getpebble.android.kit.PebbleKit.PebbleNackReceiver;
import com.getpebble.android.kit.util.PebbleDictionary;
import com.parse.ParseException;
import com.parse.ParsePush;

import org.json.JSONException;
import org.json.JSONObject;
import org.kvj.android.webappbridge.R;
import org.kvj.android.webappbridge.pebble.plugin.PebblePluginInfo;
import org.kvj.android.webappbridge.pebble.plugin.impl.AlarmPlugin;
import org.kvj.android.webappbridge.pebble.plugin.impl.ChargePlugin;
import org.kvj.android.webappbridge.pebble.plugin.impl.CountdownPlugin;
import org.kvj.android.webappbridge.pebble.plugin.impl.LinkPlugin;
import org.kvj.android.webappbridge.pebble.plugin.impl.MessagePlugin;
import org.kvj.android.webappbridge.pebble.plugin.impl.NotificationPlugin;
import org.kvj.android.webappbridge.pebble.plugin.impl.StopwatchPlugin;
import org.kvj.android.webappbridge.pebble.plugin.impl.TimePlugin;
import org.kvj.android.webappbridge.pebble.plugin.instance.PebbleInstanceInfo;
import org.kvj.android.webappbridge.svc.ActionReceiver;
import org.kvj.android.webappbridge.svc.WebBridgeController;
import org.kvj.bravo7.log.Logger;
import org.kvj.bravo7.util.Listeners;
import org.kvj.bravo7.util.Tasks;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.UUID;

public class PebbleController {

    private static final String ACTION_EXTRA_INSTANCE = "action_instance_id";
    private static final String ACTION_EXTRA_INDEX = "action_extra_index";
    private final NotificationManager notificationManager;
    private Logger logger = Logger.forInstance(this);

    public static interface PebbleCommandSender<E> {

        public boolean send(int startID, PebbleDictionary dict, PebbleInstanceInfo<E> instance);
    }

    public static interface PebbleCommandReceiver<E> {

        public void receive(PebbleDictionary dict, PebbleInstanceInfo<E> instance);
    }

    public interface MeasureListener {

        public void measure(String id, JSONObject measure);
    }

    public static class Countdown {

        public int seconds;
        public boolean cycle = false;
        public int vibro = 0;

    }

    private List<PebbleInstanceInfo> _instances = new ArrayList<PebbleInstanceInfo>();

    private List<PebblePluginInfo> plugins = new ArrayList<PebblePluginInfo>();

    public static final int KEY_CMD = 0;
    public static final int KEY_ID = 1;
    public static final int KEY_TYPE = 2;
    public static final int KEY_CMD_RESET = 0;

    private static final int KEY_CMD_INSTANCE = 1;
    private static final int KEY_CMD_REQUEST_RESET = 2;
    private static final int KEY_CMD_INSTANCE_CMD = 3;
    private static final int KEY_CMD_INSTANCE_REQUEST_CMD = 4;

    private static final int MAX_STR = 40;

    private static final String TAG = "Controller";
    private WebBridgeController parent;
    private Queue<PebbleDictionary> dictQueue = new LinkedList<PebbleDictionary>();
    private UUID watchappUuid = UUID.fromString("a2943295-6ae3-42a0-96c9-c29738bf25fa");

    private WakeLock wakeLock;
    private Listeners<MeasureListener> measureListeners = new Listeners<>();

    public PebbleController(WebBridgeController parent) {
        PowerManager
                powerManager =
            (PowerManager) parent.context().getSystemService(Context.POWER_SERVICE);
        wakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "PebbleSend");
        wakeLock.setReferenceCounted(false);
        this.parent = parent;
        notificationManager = (NotificationManager) parent.context().getSystemService(Context.NOTIFICATION_SERVICE);
        createPlugins();
        PebbleKit.registerReceivedAckHandler(parent.context(),
                                             new PebbleAckReceiver(watchappUuid) {

                                                 @Override
                                                 public void receiveAck(Context context,
                                                                        int transactionId) {
                                                     logger.d("ACK:",
                                                              transactionId);
                                                     synchronized (dictQueue) {
                                                         // Remove from top
                                                         dictQueue.poll();
                                                     }
                                                     sendNow(); // Next item
                                                 }
                                             });
        PebbleKit.registerReceivedNackHandler(parent.context(),
                                              new PebbleNackReceiver(watchappUuid) {

                                                  @Override
                                                  public void receiveNack(Context context,
                                                                          int transactionId) {
                                                      logger.w("NACK:", transactionId);
                                                      clearState();
                                                  }
                                              });
        PebbleKit.registerPebbleDisconnectedReceiver(parent.context(),
                                                     new BroadcastReceiver() {

                                                         @Override
                                                         public void onReceive(Context context,
                                                                               Intent intent) {
                                                             logger
                                                                 .i("Pebble disconnected, clear queue");
                                                             clearState();
                                                         }
                                                     });
        PebbleKit.registerReceivedDataHandler(parent.context(),
                                              new PebbleDataReceiver(watchappUuid) {

                                                  @Override
                                                  public void receiveData(Context context,
                                                                          int transactionId,
                                                                          PebbleDictionary data) {
                                                      Long type = data.getInteger(KEY_CMD);
                                                      logger.i("Incoming message of type",
                                                               type, data.size());
                                                      if (type == KEY_CMD_REQUEST_RESET) {
                                                          // Reset plugins
                                                          resetInstances();
                                                      }
                                                      if (type == KEY_CMD_INSTANCE_REQUEST_CMD) {
                                                          // Asked for data
                                                          long id = data.getInteger(KEY_ID);
                                                          synchronized (_instances) {
                                                              // Look for instance
                                                              for (PebbleInstanceInfo instance : _instances) {
                                                                  if (instance.id == id) {
                                                                      // Found
                                                                      instance.plugin
                                                                          .receive(data, instance);
                                                                  }
                                                              }
                                                          }
                                                      }
                                                  }
                                              });
        readFromPreferences();
    }

    private void createPlugins() {
        plugins.add(new AlarmPlugin(this));
        plugins.add(new ChargePlugin(this));
        plugins.add(new CountdownPlugin(this));
        plugins.add(new LinkPlugin(this));
        plugins.add(new MessagePlugin(this));
        plugins.add(new NotificationPlugin(this));
        plugins.add(new StopwatchPlugin(this));
        plugins.add(new TimePlugin(this));
    }

    private boolean isConnected () {
        if (!PebbleKit.isWatchConnected(parent.context())) {
            return false;
        }
        return true;
    }

    private boolean sendNow() {
        PebbleDictionary dict = null;
        synchronized (dictQueue) {
            dict = dictQueue.peek();
        }
        if (null == dict) {
            // No items
            wakeLock.release();
            return true;
        }
        if (!isConnected()) {
            logger.w("Watch is not connected");
            clearState();
            return false;
        }
        try {
            // WebBridgeApp.logger.log(LogLevel.Info, "Will send data...",
            // dict.size());
            PebbleKit.sendDataToPebble(parent.context(), watchappUuid, dict);
            logger.d("Data sent");
            // WebBridgeApp.toast("Data sent to Pebble");
            return true;
        } catch (Exception e) {
            logger.e(e, "Error sending data");
        }
        return false;

    }

    private void send(PebbleDictionary dict) {
        if (!isConnected()) {
            return;
        }
        synchronized (dictQueue) {
            boolean isEmpty = dictQueue.isEmpty();
            dictQueue.add(dict);
            if (isEmpty) {
                // Send immediately only when first time
                wakeLock.acquire(3000); // Lock
                sendNow();
            }
        }
    }

    private PebbleDictionary dict(int key) {
        PebbleDictionary dict = new PebbleDictionary();
        dict.addUint32(KEY_CMD, key);
        return dict;
    }

    public void clearState() {
        synchronized (dictQueue) {
            dictQueue.clear();
        }
        wakeLock.release();
    }

    public void resetInstances() {
        PebbleDictionary dict = dict(KEY_CMD_RESET);

        send(dict);
        synchronized (_instances) {
            for (PebbleInstanceInfo info : _instances) {
                dict = dict(KEY_CMD_INSTANCE);
                int key = KEY_CMD + 1;
                dict.addUint32(key++, info.id); // id
                dict.addString(key++, info.plugin.getID()); // type
                dict.addInt16(key++, (short) info.bounds.left); // bounds
                dict.addInt16(key++, (short) info.bounds.top);
                dict.addInt16(key++, (short) info.bounds.right);
                dict.addInt16(key++, (short) info.bounds.bottom);
                send(dict);
                info.plugin.receive(null, info);
            }
        }
    }

    public void sendInstanceCommand(String type, PebbleCommandSender sender) {
        synchronized (_instances) {
            for (PebbleInstanceInfo info : _instances) {
                if (info.plugin.getID().equals(type)) {
                    // OK
                    PebbleDictionary dict = dict(KEY_CMD_INSTANCE_CMD);
                    int key = KEY_CMD + 1;
                    dict.addUint32(key++, info.id); // id
                    sender.send(key, dict, info);
                    send(dict);
                }
            }
        }
    }

    public void sendInstanceCommand(int id, PebbleCommandSender sender, String... strings) {
        if (null == strings) {
            strings = new String[0];
        }
        PebbleInstanceInfo<?> instance = findInstance(id);
        if (null == instance) {
            return;
        }
        if (strings.length == 0) { // Simple version
            int key = KEY_CMD + 1;
            PebbleDictionary dict = dict(KEY_CMD_INSTANCE_CMD);
            dict.addUint32(key++, instance.id);
            sender.send(key, dict, instance);
            send(dict);
            return;
        }
        int maxLength = 0;
        for (String str : strings) {
            if (str.length() > maxLength) {
                maxLength = str.length();
            }
        }
        int maxStr = MAX_STR / strings.length;
        int steps = (int) Math.ceil((float)maxLength / (float)maxStr);
        if (steps == 0) {
            steps = 1; // At least one step
        }
        int start = 0;
        for (int i = 0; i < steps; i++) { // $COMMENT
            PebbleDictionary dict = dict(KEY_CMD_INSTANCE_CMD);
            int key = KEY_CMD + 1;
            dict.addUint32(key++, instance.id); // id
            if (i == 0) {
                // First round
                dict.addInt32(key++, 0);
            } else {
                dict.addInt32(key++, 1);
            }
            for (String s : strings) { // Add lines one by one
                if (start >= s.length()) { // Empty
                    dict.addString(key++, "");
                } else if (start + maxStr >= s.length()) { // Tail
                    dict.addString(key++, s.substring(start));
                } else {
                    dict.addString(key++, s.substring(start, start+maxStr));
                }
            }
            start += maxStr;
            if (i == 0) { // Only first time
                sender.send(key, dict, instance);
            }
            logger.d("Send json:", dict.toJsonString(), steps, i, maxLength, maxStr, start);
            send(dict);
        }
    }

    public List<PebblePluginInfo> getPlugins() {
        return plugins;
    }

    public int nextInstanceID() {
        int id = 1;
        synchronized (_instances) {
            for (PebbleInstanceInfo<?> ins : _instances) {
                if (ins.id >= id) {
                    id = ins.id + 1;
                }
            }
        }
        return id;
    }

    public List<PebbleInstanceInfo> getInstances() {
        return _instances;
    }

    public PebblePluginInfo findPlugin(String id) {
        for (PebblePluginInfo info : plugins) {
            if (info.getID().equals(id)) {
                return info;
            }
        }
        return null;
    }

    private static final String SETTINGS_ID = "data_instance_ids";

    private void readFromPreferences() {
        List<String> idsStr = parent.settings().settingsList(R.string.settings_ids);
        _instances.clear();
        logger.d("Restore plugins:", idsStr);
        for (String str : idsStr) {
            try {
                // Convert to Integer
                int id = Integer.parseInt(str, 10);
                String
                        type = PebblePluginInfo.getPreferences(parent(), id).getString(PebblePluginInfo.PKEY_TYPE, "");
                logger.d("Restore", id, type);
                @SuppressWarnings("rawtypes")
                PebblePluginInfo plugin = findPlugin(type);
                if (null == plugin) {
                    // Not found
                    logger.w("Plugin not found", str, type);
                    continue;
                }
                _instances.add(plugin.read(id));
            } catch (Exception e) {
                logger.e(e, "Error reading instance", str);
            }
        }
    }

    @SuppressWarnings("rawtypes")
    public PebbleInstanceInfo newInstance(String type) {
        List<String> idsStr = parent.settings().settingsList(R.string.settings_ids);
        for (PebblePluginInfo plugin : plugins) {
            if (plugin.getID().equals(type)) {
                // Found
                PebbleInstanceInfo instance = plugin.create(nextInstanceID());
                plugin.save(instance);
                synchronized (_instances) {
                    _instances.add(instance);
                }
                if (!idsStr.contains(Integer.toString(instance.id))) {
                    idsStr.add(Integer.toString(instance.id));
                    parent.settings().listSettings(R.string.settings_ids, idsStr);
                }
                return instance;
            }
        }
        return null; // Failed to find
    }

    public void removeInstance(PebbleInstanceInfo instance) {
        List<String> idsStr = parent.settings().settingsList(R.string.settings_ids);
        synchronized (_instances) {
            _instances.remove(instance);
            instance.plugin.remove(instance);
            idsStr.remove(Integer.toString(instance.id));
            parent.settings().listSettings(R.string.settings_ids, idsStr);
        }
    }

    public void saveInstance(PebbleInstanceInfo instance) {
        instance.plugin.save(instance);
    }

    public PebbleInstanceInfo<?> findInstance(int instanceID) {
        synchronized (_instances) {
            for (PebbleInstanceInfo instance : _instances) {
                if (instance.id == instanceID) {
                    return instance;
                }
            }
        }
        return null;
    }

    public void moveInstance(int from, int before) {
        List<String> idsStr = parent.settings().settingsList(R.string.settings_ids);
        String id = idsStr.get(from);
        if (before > from) {
            idsStr.add(before, id);
            idsStr.remove(from);
        } else {
            idsStr.remove(from);
            if (-1 == before) {
                idsStr.add(id);
            } else {
                idsStr.add(before, id);
            }
        }
        parent.settings().listSettings(R.string.settings_ids, idsStr);
        readFromPreferences();
    }

    public <E> List<PebbleInstanceInfo<E>> getInstances(String id, Class<E> cl) {
        // Find all instances of plugin
        List<PebbleInstanceInfo<E>> result = new ArrayList<PebbleInstanceInfo<E>>();
        synchronized (_instances) {
            for (PebbleInstanceInfo info : _instances) {
                if (info.plugin.getID().equals(id)) {
                    result.add(info);
                }
            }
        }
        return result;

    }

    public WebBridgeController parent() {
        return parent;
    }

    private int instanceNotificationID(PebbleInstanceInfo<?> instance) {
        return instance.id + 100;
    }

    public void hidePluginNotification(PebbleInstanceInfo<?> instance) {
        notificationManager.cancel(instanceNotificationID(instance));
    }

    public void showPluginNotification(PebbleInstanceInfo<?> instance, int id, Notification notification) {
        notificationManager.notify(instance.plugin.getID(), id, notification);
    }

    public NotificationCompat.Builder newNotification(boolean visual) {
        NotificationCompat.Builder builder = new NotificationCompat.Builder(parent.context());
        builder.setSmallIcon(R.drawable.ic_stat_normal);
        if (visual) { // Show visual app
            builder.setOnlyAlertOnce(false);
            builder.setOngoing(false);
            builder.setDefaults(NotificationCompat.DEFAULT_ALL);
            builder.setPriority(Notification.PRIORITY_DEFAULT);
        } else { // Silent notification
            builder.setOnlyAlertOnce(true);
            builder.setOngoing(true);
            builder.setPriority(Notification.PRIORITY_MIN);
        }
        return builder;
    }

    public PendingIntent actionIntent(PebbleInstanceInfo<?> instance, int index) {
        Intent intent = new Intent(parent.context(), ActionReceiver.class);
        intent.putExtra(ACTION_EXTRA_INSTANCE, instance.id);
        intent.putExtra(ACTION_EXTRA_INDEX, index);
        return PendingIntent.getBroadcast(parent.context(), 0, intent, PendingIntent.FLAG_CANCEL_CURRENT);
    }

    public boolean handleAction(Intent intent) {
        PebbleInstanceInfo<?> instance = findInstance(intent.getIntExtra(ACTION_EXTRA_INSTANCE, -1));
        if (null == instance) {
            return false;
        }
        instance.plugin.handleAction(instance, intent.getIntExtra(ACTION_EXTRA_INDEX, -1));
        return true;
    }

    public void sendMeasure(String name, int measure, float value) {
        final ParsePush push = new ParsePush();
        push.setChannel("server");
        final JSONObject object = new JSONObject();
        try {
            object.put("id", name);
            object.put("value", value);
            object.put("type", "measure");
            if (measure != -1) {
                object.put("measure", measure);
            }
            push.setData(object);
        } catch (JSONException e) {
            logger.e(e, "JSON error");
            return;
        }
        new Tasks.VerySimpleTask() {

            @Override
            protected void doInBackground() {
                try {
                    push.send();
                    logger.d("Measure sent:", object, push);
                } catch (ParseException e) {
                    logger.e(e, "Failed to send measure:", object);
                }
            }
        }.exec();
    }

    public Listeners<MeasureListener> measureListeners() {
        return measureListeners;
    }
}
