package org.kvj.android.webappbridge.pebble.plugin.impl.svc;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import org.kvj.android.webappbridge.WebBridgeApp;
import org.kvj.android.webappbridge.pebble.plugin.PebblePluginInfo;
import org.kvj.android.webappbridge.svc.WebBridgeController;
import org.kvj.bravo7.log.Logger;
import org.kvj.bravo7.ng.App;

/**
 * Created by kvorobyev on 3/30/15.
 */
public class AlarmTimerReceiver extends BroadcastReceiver {

    WebBridgeController controller = App.controller();

    protected Logger logger = Logger.forInstance(this);
    @Override
    public void onReceive(Context context, Intent intent) {
        logger.d("Alarm received", intent.getData());
        if (null == controller) { // Not ready yet
            logger.w("No controller");
            return;
        }
        PebblePluginInfo plugin = controller.getPebble().findPlugin("alarm");
        if (null == plugin) { // No plugin
            logger.w("No plugin");
            return;
        }
        plugin.onReceive(context, intent);
    }
}
