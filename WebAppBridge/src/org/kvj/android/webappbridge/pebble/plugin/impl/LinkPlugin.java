package org.kvj.android.webappbridge.pebble.plugin.impl;

import android.content.SharedPreferences;

import org.kvj.android.webappbridge.pebble.PebbleController;
import org.kvj.android.webappbridge.pebble.plugin.PebblePluginInfo;
import org.kvj.android.webappbridge.pebble.plugin.instance.PebbleInstanceInfo;

public class LinkPlugin extends PebblePluginInfo<Object> {

    public LinkPlugin(PebbleController controller) {
        super(controller);
    }

    @Override
    public String getCaption() {
        return "Connection Link";
    }

    @Override
    public String getID() {
        return "link";
    }

    @Override
    protected void initNew(SharedPreferences prefs, PebbleInstanceInfo<Object> instance) {
        instance.bounds.right = 20;
        instance.bounds.bottom = 20;
    }

}
