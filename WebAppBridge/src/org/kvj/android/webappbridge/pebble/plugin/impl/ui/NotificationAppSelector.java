package org.kvj.android.webappbridge.pebble.plugin.impl.ui;

import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import org.kvj.android.webappbridge.R;
import org.kvj.android.webappbridge.WebBridgeApp;
import org.kvj.android.webappbridge.pebble.plugin.instance.PebbleInstanceInfo;
import org.kvj.android.webappbridge.svc.WebBridgeController;
import org.kvj.android.webappbridge.ui.InstanceConfigActivity;
import org.kvj.bravo7.adapter.AnotherListAdapter;
import org.kvj.bravo7.ng.App;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class NotificationAppSelector extends AppCompatActivity {

    private ListView mListView;
    private int instanceID;
    private PebbleInstanceInfo instance;
    private ListAdapter mAdapter;
    private SharedPreferences preferences;
    private Set<String> packages;
    private WebBridgeController controller = App.controller();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.notify_app_selector);
        instanceID = getIntent().getIntExtra(InstanceConfigActivity.INTENT_INSTANCE_ID, -1);
        mListView = (ListView) findViewById(R.id.app_selector_list);
        mListView.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int pos, long arg3) {
                mAdapter.getItem(pos).selected = !mAdapter.getItem(pos).selected;
                mAdapter.notifyDataSetChanged();
            }
        });
        mListView.setOnItemLongClickListener(new OnItemLongClickListener() {

            @Override
            public boolean onItemLongClick(AdapterView<?> arg0, View arg1, int pos, long arg3) {
                AppInfo info = mAdapter.getItem(pos);
                if (!info.selected) {
                    return false;
                }
                NotificationRexpEditor.newInstance(info.packageName, instanceID).show(getFragmentManager(), "editor");
                return true;
            }
        });
        onController();
    }

    public void onController() {
        instance = controller.getPebble().findInstance(instanceID);
        if (instance == null) {
            // Not found
            WebBridgeApp.toast("Invalid configuration");
            return;
        }
        preferences = instance.plugin.getPreferences(instance.id);
        packages = preferences.getStringSet("packages", new HashSet<String>());
        Thread loadThread = new Thread() {
            @Override
            public void run() {
                List<ApplicationInfo> apps = getPackageManager().getInstalledApplications(
                        PackageManager.GET_META_DATA);
                final List<AppInfo> infos = new ArrayList<NotificationAppSelector.AppInfo>();
                for (ApplicationInfo info : apps) {
                    AppInfo appInfo = new AppInfo();
                    appInfo.packageName = info.packageName;
                    appInfo.name = info.packageName;
                    appInfo.selected = packages.contains(info.packageName);
                    CharSequence desc = info.loadLabel(getPackageManager());
                    if (null != desc) {
                        appInfo.name = desc.toString();
                    }
                    appInfo.logo = info.loadIcon(getPackageManager());
                    infos.add(appInfo);
                }
                Collections.sort(infos, new Comparator<AppInfo>() {

                    @Override
                    public int compare(AppInfo lhs, AppInfo rhs) {
                        return lhs.name.compareToIgnoreCase(rhs.name);
                    }
                });
                runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        mAdapter = new ListAdapter(infos);
                        mListView.setAdapter(mAdapter);
                    }
                });
            }
        };
        loadThread.start();
    }

    static class AppInfo {

        String name;
        Drawable logo;
        String packageName;
        boolean selected = false;
    }

    class ListAdapter extends AnotherListAdapter<AppInfo> {

        public ListAdapter(List<AppInfo> data) {
            super(data, R.layout.notify_app_selector_item);
        }

        @Override
        public void customize(View view, int position) {
            AppInfo info = getItem(position);
            ImageView icon = (ImageView) view.findViewById(R.id.app_selector_item_icon);
            icon.setImageDrawable(info.logo);
            TextView text = (TextView) view.findViewById(R.id.app_selector_item_caption);
            text.setText(info.name);
            view.setBackgroundColor(info.selected ? Color.LTGRAY : Color.WHITE);
        }

    }

    @Override
    protected void onStop() {
        if (null != preferences) {
            // Save selected packages
            Set<String> packages = new HashSet<String>();
            for (int i = 0; i < mAdapter.getCount(); i++) {
                AppInfo info = mAdapter.getItem(i);
                if (info.selected) {
                    packages.add(info.packageName);
                }
            }
            preferences.edit().putStringSet("packages", packages).apply();
        }
        super.onStop();
    }

}
