package org.kvj.android.webappbridge.pebble.plugin;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Rect;

import com.getpebble.android.kit.util.PebbleDictionary;

import net.sf.junidecode.Junidecode;

import org.kvj.android.webappbridge.R;
import org.kvj.android.webappbridge.pebble.PebbleController;
import org.kvj.android.webappbridge.pebble.PebbleController.PebbleCommandReceiver;
import org.kvj.android.webappbridge.pebble.PebbleController.PebbleCommandSender;
import org.kvj.android.webappbridge.pebble.plugin.instance.PebbleInstanceInfo;
import org.kvj.android.webappbridge.svc.WebBridgeController;
import org.kvj.bravo7.log.Logger;

import java.io.UnsupportedEncodingException;

abstract public class PebblePluginInfo<E> extends BroadcastReceiver implements PebbleCommandSender<E>, PebbleCommandReceiver<E> {

    protected final Context context;
    protected Logger logger = Logger.forInstance(this);
    public static final String PKEY_TYPE = "pkey_type";
    private static final String PKEY_BOUNDS_X = "pkey_bounds_x";
    private static final String PKEY_BOUNDS_Y = "pkey_bounds_y";
    private static final String PKEY_BOUNDS_W = "pkey_bounds_w";
    private static final String PKEY_BOUNDS_H = "pkey_bounds_h";
    protected final PebbleController controller;

    public PebblePluginInfo(PebbleController controller) {
        this.controller = controller;
        this.context = controller.parent().context();
    }

    abstract public String getCaption();

    abstract public String getID();

    public boolean startConfig(PebbleInstanceInfo<E> instance, Activity startActivity) {
        return false;
    }

    protected void readFromPreferences(SharedPreferences prefs, PebbleInstanceInfo<E> instance) {
    }

    protected void saveToPreferences(Editor editor, PebbleInstanceInfo<E> instance) {
    }

    protected void clear(PebbleInstanceInfo<E> instance) {
    }

    public SharedPreferences getPreferences(int id) {
        return getPreferences(controller.parent(), id);
    }

    public static SharedPreferences getPreferences(WebBridgeController controller, int id) {
        return controller.preferences("instance_" + id);
    }

    public PebbleInstanceInfo<E> read(int id) {
        PebbleInstanceInfo<E> instance = new PebbleInstanceInfo<E>(this);
        SharedPreferences prefs = getPreferences(id);
        logger.d("read", id, prefs.getAll());
        instance.id = id;
        instance.bounds = new Rect(prefs.getInt(PKEY_BOUNDS_X, 0), prefs.getInt(PKEY_BOUNDS_Y, 0), prefs.getInt(
                PKEY_BOUNDS_W, 25), prefs.getInt(PKEY_BOUNDS_H, 25));
        readFromPreferences(prefs, instance);
        return instance;
    }

    protected int readVibro(SharedPreferences prefs, String prefName, String prefMSecName) {
        int vibro = fromSettings(prefs, prefName, 0);
        if (vibro == 4) {
            vibro = fromSettings(prefs, prefMSecName, 100);
        }
        return vibro;
    }

    public void save(PebbleInstanceInfo<E> instance) {
        SharedPreferences prefs = getPreferences(instance.id);
        Editor editor = prefs.edit();
        editor.putString(PKEY_TYPE, getID());
        editor.putInt(PKEY_BOUNDS_X, instance.bounds.left);
        editor.putInt(PKEY_BOUNDS_Y, instance.bounds.top);
        editor.putInt(PKEY_BOUNDS_W, instance.bounds.right);
        editor.putInt(PKEY_BOUNDS_H, instance.bounds.bottom);
        saveToPreferences(editor, instance);
        editor.apply();
    }

    public PebbleInstanceInfo<E> create(int id) {
        PebbleInstanceInfo<E> instance = new PebbleInstanceInfo<E>(this);
        instance.id = id;
        instance.bounds = new Rect(0, 0, 25, 25);
        SharedPreferences prefs = getPreferences(instance.id);
        initNew(prefs, instance);
        readFromPreferences(prefs, instance);
        return instance;
    }

    public void remove(PebbleInstanceInfo<E> instance) {
        SharedPreferences prefs = getPreferences(instance.id);
        Editor editor = prefs.edit();
        editor.clear();
        clear(instance);
        editor.apply();
    }

    protected int parseInt(String value, int def) {
        try {
            return Integer.parseInt(value, 10);
        } catch (Exception e) {
            return def;
        }
    }

    protected int parseTime(String time) {
        int result = 0;
        String parts[] = time.split(":");
        for (int i = 0; i < parts.length; i++) {
            try {
                int part = Integer.parseInt(parts[i], 10);
                result = result * 60 + part;
            } catch (Exception e) {
            }
        }
        return result;
    }

    public void configChanged(PebbleInstanceInfo instance) {
        SharedPreferences prefs = getPreferences(instance.id);
        readFromPreferences(prefs, instance);
    }

    protected int fromSettings(SharedPreferences prefs, String name, int def) {
        try {
            return Integer.parseInt(prefs.getString(name, Integer.toString(def)), 10);
        } catch (Exception e) {
            // TODO: handle exception
        }
        return def;
    }

    protected void initNew(SharedPreferences prefs, PebbleInstanceInfo<E> instance) {
    }

    @Override
    public boolean send(int startKey, PebbleDictionary dict, PebbleInstanceInfo<E> instance) {
        return false;
    }

    @Override
    public void receive(PebbleDictionary dict, PebbleInstanceInfo<E> instance) {
    }

    public String getInstanceTitle(PebbleInstanceInfo<?> instance) {
        return getCaption();
    }

    private String normalizeSmart(String s) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < s.length(); i++) {
            int codepoint = s.codePointAt(i);
            int hi = (codepoint >> 8) & 0xff;
            if (hi == 0x04) {
                sb.append(s.charAt(i));
            } else {
                sb.append(Junidecode.unidecode(s.substring(i, i+1)));
            }
        }
        return sb.toString();
    }

    protected String normalizedString(String s, int maxChars) {
        boolean transliterate = controller.parent().settings().settingsBoolean(R.string.settings_transliterate, true);
        String normalized = (transliterate? Junidecode.unidecode(s): normalizeSmart(s)).replace("\t", "  ");
        int len = normalized.length();
        if (len > maxChars) {
            len = maxChars;
        }
        try {
            while(normalized.substring(0, len).getBytes("utf-8").length > maxChars) {
                len--;
            }
        } catch (UnsupportedEncodingException e) {
        }
        return normalized.substring(0, len);
    }

    @Override
    public void onReceive(Context context, Intent intent) {
    }

    public void handleAction(PebbleInstanceInfo<?> instance, int index) {
    }
}
