package org.kvj.android.webappbridge.pebble.plugin.impl.ui;

import org.kvj.android.webappbridge.R;
import org.kvj.android.webappbridge.ui.InstanceConfigActivity;

/**
 * Created by kvorobyev on 9/5/14.
 */
public class TimeConfig extends InstanceConfigActivity {

    @Override
    protected int getSettingsConfig() {
        return R.xml.time_config;
    }

}
