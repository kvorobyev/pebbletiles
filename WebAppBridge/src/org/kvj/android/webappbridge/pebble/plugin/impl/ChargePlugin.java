package org.kvj.android.webappbridge.pebble.plugin.impl;

import android.app.Activity;
import android.content.SharedPreferences;
import android.text.TextUtils;

import com.getpebble.android.kit.util.PebbleDictionary;

import org.json.JSONObject;
import org.kvj.android.webappbridge.R;
import org.kvj.android.webappbridge.pebble.PebbleController;
import org.kvj.android.webappbridge.pebble.plugin.PebblePluginInfo;
import org.kvj.android.webappbridge.pebble.plugin.impl.ui.ChargeConfig;
import org.kvj.android.webappbridge.pebble.plugin.instance.PebbleInstanceInfo;
import org.kvj.android.webappbridge.ui.InstanceConfigActivity;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ChargePlugin extends PebblePluginInfo<Object>
    implements PebbleController.MeasureListener {

    private static final int MAX_REMOTE_DEVICES = 5;
    private BatteryEventGenerator batteryEventGenerator;

    private Map<Integer, Integer> remoteLevels = new HashMap<>();

    public ChargePlugin(final PebbleController controller) {
        super(controller);
        controller.measureListeners().add(this);
        batteryEventGenerator = new BatteryEventGenerator(controller.parent().context()) {
            @Override
            public void sendCurrent() {
                controller.sendInstanceCommand("charge", sendBatteryInfo(1, lastAnnounceLevel, lastAnnounceState != 0));
                sendDeviceBatteryInfo();
            }
        };
    }

    private void sendDeviceBatteryInfo() {
        List<PebbleInstanceInfo<Object>> instances = controller.getInstances("charge", Object.class);
        for (PebbleInstanceInfo<Object> ins : instances) { // $COMMENT
            sendMeasure(ins.id,
                    R.string.device_charge_id,
                    batteryEventGenerator.lastAnnounceLevel,
                    batteryEventGenerator.lastAnnounceState != 0);
        }
    }

    @Override
    public String getCaption() {
        return "Battery Level";
    }

    @Override
    public String getID() {
        return "charge";
    }

    @Override
    protected void initNew(SharedPreferences prefs, PebbleInstanceInfo<Object> instance) {
        instance.bounds.right = 15;
        instance.bounds.bottom = 60;
    }

    @Override
    public void receive(PebbleDictionary dict, PebbleInstanceInfo<Object> instance) {
        if (null == dict) {
            controller.sendInstanceCommand("charge", sendBatteryInfo(1,
                    batteryEventGenerator.lastAnnounceLevel,
                    batteryEventGenerator.lastAnnounceState != 0));
            for (int i = 0; i < MAX_REMOTE_DEVICES; i++) { // $COMMENT
                if (!remoteLevels.containsKey(i)) { // No data
                    return;
                }
                int level = remoteLevels.get(i);
                controller.sendInstanceCommand("charge", sendBatteryInfo(i+2, level, false));
            }
            return;
        }
        logger.d("Received:", instance.id, dict.toJsonString());
        try {
            int percent = dict.getUnsignedIntegerAsLong(PebbleController.KEY_ID + 1).intValue();
            boolean charging = dict.getInteger(PebbleController.KEY_ID + 2) > 0;
            sendMeasure(instance.id, R.string.pebble_charge_id, percent, charging);
        } catch (Exception e) {
            logger.e(e, "Message Failure");
        }
    }

    private void sendMeasure(int id, int configName, int value, boolean charging) {
        SharedPreferences pref = getPreferences(id);
        String configID = pref.getString(context.getString(configName), "");
        logger.d("Sending charge value:", configID, value, charging);
        if (TextUtils.isEmpty(configID)) {
            return;
        }
        controller.sendMeasure(configID, -1, value);
    }

    private PebbleController.PebbleCommandSender sendBatteryInfo(final int index, final int level, final boolean charging) {
        return new PebbleController.PebbleCommandSender() {
            @Override
            public boolean send(int startID, PebbleDictionary dict, PebbleInstanceInfo instance) {
                int KEY = startID;
                dict.addInt16(KEY++, (short) index);
                dict.addInt16(KEY++, (short) level);
                dict.addInt16(KEY++, charging ? (short) 1 : (short) 0);
                dict.addInt16(KEY++, (short) 0);
                return true;
            }
        };
    }

    @Override
    public boolean startConfig(PebbleInstanceInfo<Object> instance, Activity startActivity) {
        InstanceConfigActivity.startConfig(startActivity, ChargeConfig.class, instance);
        return true;
    }

    @Override
    public void measure(String id, final JSONObject measure) {
        if (measure.optInt("type") != 3) {
            return; // Only receive battery
        }
        int index = parseInt(id, 0);
        int level = measure.optInt("value", 0);
        remoteLevels.put(index, level);
        logger.d("Remote battery level:", index, level);
        controller.sendInstanceCommand("charge", sendBatteryInfo(index+2, level, false));
    }
}
