package org.kvj.android.webappbridge.pebble.plugin.impl;

import android.app.Activity;
import android.content.SharedPreferences;

import com.getpebble.android.kit.util.PebbleDictionary;

import org.kvj.android.webappbridge.pebble.PebbleController;
import org.kvj.android.webappbridge.pebble.plugin.PebblePluginInfo;
import org.kvj.android.webappbridge.pebble.plugin.impl.ui.TimeConfig;
import org.kvj.android.webappbridge.pebble.plugin.instance.PebbleInstanceInfo;
import org.kvj.android.webappbridge.ui.InstanceConfigActivity;

public class TimePlugin extends PebblePluginInfo<TimePlugin.TimeInfo> {

    public static class TimeInfo {
        int vibro = 0;
        int interval = 0;
        int silentStart = 0;
        int silentFinish = 0;
        int minutesShift = 0;
    }

    public TimePlugin(PebbleController controller) {
        super(controller);
    }

    @Override
    public String getCaption() {
        return "Time";
    }

    @Override
    public String getID() {
        return "time";
    }

    @Override
    public boolean startConfig(PebbleInstanceInfo instance, Activity startActivity) {
        InstanceConfigActivity.startConfig(startActivity, TimeConfig.class, instance);
        return true;
    }

    @Override
    protected void initNew(SharedPreferences prefs, PebbleInstanceInfo instance) {
        instance.bounds.right = 130;
        instance.bounds.bottom = 45;
    }

    @Override
    protected void readFromPreferences(SharedPreferences prefs, PebbleInstanceInfo<TimeInfo> instance) {
        instance.data = new TimeInfo();
        instance.data.vibro = readVibro(prefs, "vibro", "vibro_seconds");
        instance.data.interval = parseInt(prefs.getString("interval", ""), 0);
        instance.data.silentStart = parseTime(prefs.getString("silent_from", "0"));
        instance.data.silentFinish = parseTime(prefs.getString("silent_to", "0"));
        instance.data.minutesShift = parseInt(prefs.getString("time_shift", ""), 0);
    }

    @Override
    public void receive(PebbleDictionary dict, PebbleInstanceInfo<TimeInfo> instance) {
        controller.sendInstanceCommand(instance.id, this);
    }

    @Override
    public boolean send(int startKey, PebbleDictionary dict, PebbleInstanceInfo<TimeInfo> instance) {
        int KEY = startKey;
        dict.addInt16(KEY++, (short) instance.data.vibro);
        dict.addInt16(KEY++, (short) instance.data.interval);
        dict.addInt16(KEY++, (short) 1);
        dict.addInt16(KEY++, (short) instance.data.silentStart);
        dict.addInt16(KEY++, (short) instance.data.silentFinish);
        dict.addInt32(KEY++, instance.data.minutesShift);
        return true;
    }
}
