package org.kvj.android.webappbridge.pebble.plugin.impl;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.PowerManager;
import android.service.notification.StatusBarNotification;
import android.text.TextUtils;

import com.getpebble.android.kit.util.PebbleDictionary;

import org.kvj.android.webappbridge.pebble.PebbleController;
import org.kvj.android.webappbridge.pebble.plugin.PebblePluginInfo;
import org.kvj.android.webappbridge.pebble.plugin.impl.NotificationPlugin.NotificationPluginInfo;
import org.kvj.android.webappbridge.pebble.plugin.impl.ui.NotificationConfig;
import org.kvj.android.webappbridge.pebble.plugin.instance.PebbleInstanceInfo;
import org.kvj.android.webappbridge.ui.InstanceConfigActivity;
import org.kvj.bravo7.log.Logger;

import java.util.Calendar;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;

public class NotificationPlugin extends PebblePluginInfo<NotificationPluginInfo> {

    public static final String ID = "notify";
    private Logger logger = Logger.forInstance(this);


    public NotificationPlugin(PebbleController controller) {
        super(controller);
    }

    public static class NotificationPluginInfo {

        String title;
        String message;
        boolean hasMessage = false;
        int vibro = -1;
        int auto_hide = 0;
        boolean fullscreen = false;
        boolean removeNotification = false;
        StatusBarNotification lastNotification = null;
    }

    @Override
    protected void readFromPreferences(SharedPreferences prefs,
                                       PebbleInstanceInfo<NotificationPluginInfo> instance) {
        instance.data = new NotificationPluginInfo();
    }

    @Override
    public String getCaption() {
        return "Notifications";
    }

    @Override
    public String getID() {
        return ID;
    }

    @Override
    public boolean startConfig(PebbleInstanceInfo<NotificationPluginInfo> instance,
                               Activity startActivity) {
        InstanceConfigActivity.startConfig(startActivity, NotificationConfig.class, instance);
        return true;
    }

    @Override
    protected void initNew(SharedPreferences prefs,
                           PebbleInstanceInfo<NotificationPluginInfo> instance) {
        instance.bounds.bottom = 30;
        instance.bounds.right = 100;
    }

    public void newNotification(StatusBarNotification sbn, boolean visible) {
        List<PebbleInstanceInfo<NotificationPluginInfo>> instances = controller.getInstances(ID,
                NotificationPluginInfo.class);
        for (PebbleInstanceInfo<NotificationPluginInfo> info : instances) {
            newNotification(info, sbn, visible);
        }
    }

    private boolean areSameNotifications(StatusBarNotification n1, StatusBarNotification n2) {
        if (n1 == null || n2 == null) {
            return false;
        }
        if (!n1.getPackageName().equals(n2.getPackageName())) {
            return false;
        }
        if (n1.getId() != n2.getId()) {
            return false;
        }
        return true;
    }

    private boolean newNotification(PebbleInstanceInfo<NotificationPluginInfo> instance,
                                    StatusBarNotification sbn, boolean visible) {
        String packageName = sbn.getPackageName();
        logger.d("newNotification:", visible, packageName, sbn.getTag(), sbn.getId(), sbn.getNotification().tickerText);
        if (!visible) {
            // Send notification hide message
            // logger.d("Hide:", areSameNotifications(sbn, instance.data.lastNotification), instance.data.lastNotification);
            if (areSameNotifications(sbn, instance.data.lastNotification)) {
                instance.data.removeNotification = true;
                instance.data.hasMessage = false;
                instance.data.lastNotification = null;
                controller.sendInstanceCommand(instance.id, this, "", "");
                return true;
            }
            return false;
        }
        SharedPreferences prefs = getPreferences(instance.id);
        if (prefs.getBoolean("silent_now", false)) {
            // Notifications disabled
            logger.d("newNotification:", "disabled");
            return false;
        }
        Set<String> ps = prefs.getStringSet("packages", new HashSet<String>());
        if (!ps.contains(packageName)) {
            // Out of selected packages - stop
            logger.d("newNotification:", "out of set", ps);
            return false;
        }
        int silentStart = parseTime(prefs.getString("silent_from", "0"));
        int silentFinish = parseTime(prefs.getString("silent_to", "0"));
        if (silentStart > 0 || silentFinish > 0) {
            // Check is it silent time or not
            Calendar c = Calendar.getInstance();
            int nowTime = c.get(Calendar.HOUR_OF_DAY) * 60 + c.get(Calendar.MINUTE);
            if (silentStart < silentFinish) {
                // Same day
                if (silentStart <= nowTime && nowTime < silentFinish) {
                    logger.d("newNotification:", "silent1", silentStart,
                            silentFinish,
                            nowTime);
                    return false; // Silent now
                }
            } else {
                // Over midnight
                if (silentStart <= nowTime || nowTime < silentFinish) {
                    logger.d("newNotification:", "silent2", silentStart,
                            silentFinish,
                            nowTime);
                    return false; // Silent now
                }

            }
        }
        CharSequence message = sbn.getNotification().tickerText;
        if (null == message) {
            logger.d("newNotification:", "no message");
            return false; // No message
        }
        // Check regexp, if available
        boolean include = prefs.getBoolean(packageName + "_ex", false);
        String rexpStr = prefs.getString(packageName + "_rexp", "");
        int customVibro = prefs.getInt(packageName + "_vibro", 0);
        String[] arr = rexpStr.split("\n");
        boolean match = true;
        if (!TextUtils.isEmpty(rexpStr) && arr.length > 0 && null != message) {
            match = false;
            for (String str : arr) {
                try {
                    Pattern p = Pattern.compile(str, Pattern.CASE_INSENSITIVE);
                    if (p.matcher(message).find()) {
                        match = true;
                        break;
                    }
                } catch (Exception e) {
                }
            }
            if ((!match && include) || (match && !include)) {
                // Exclude notification. In include mode, if there is no match - skip. In exclude mode - if there is match - skip
                logger.d("newNotification:", "excluded", arr, include, rexpStr,
                        message);
                return false;
            }
        }
        String title = packageName;
        try {
            PackageManager pm = controller.parent().context().getPackageManager();
            ApplicationInfo info = pm.getApplicationInfo(packageName, PackageManager.GET_META_DATA);
            CharSequence appName = pm.getApplicationLabel(info);
            if (null != appName) {
                title = appName.toString();
            }
        } catch (Exception e) {
        }
        instance.data.title = normalizedString(title, 40);
        instance.data.message = normalizedString(message.toString(), 255);
        instance.data.hasMessage = true;
        instance.data.auto_hide = fromSettings(prefs, "auto_hide", 0);
        instance.data.vibro = -1; // Default
        if (customVibro > 0) { // Have custom settings
            switch (customVibro) {
                case 1: // Short
                    instance.data.vibro = 0;
                case 2: // Long
                    instance.data.vibro = 1;
                case 3: // Double
                    instance.data.vibro = 2;
                case 4: // Repeat
                    instance.data.vibro = 3;
            }
        }
        instance.data.removeNotification = false;
        instance.data.fullscreen = prefs.getBoolean("show_fullscreen", false);
        instance.data.lastNotification = sbn;
        controller
                .sendInstanceCommand(instance.id, this, instance.data.title, instance.data.message);
        logger.d("newNotification:", "OK", title, message);
        return true;
    }

    @Override
    public boolean send(int startKey, PebbleDictionary dict,
                        PebbleInstanceInfo<NotificationPluginInfo> instance) {
        if (!instance.data.hasMessage) {
            return false;
        }
        logger.d("Sending notification:", instance.data.title);
        int KEY = startKey;
        PowerManager
                pm = (PowerManager) controller.parent().context().getSystemService(Context.POWER_SERVICE);
        if (instance.data.vibro == -1) { // Read vibro from settings
            String vibroPref = "vibro_screen_off";
            String vibroPrefSec = "vibro_seconds_screen_off";
            if (pm.isScreenOn()) {
                // Screen On constants
                vibroPref = "vibro_screen_on";
                vibroPrefSec = "vibro_seconds_screen_on";
            }
            SharedPreferences prefs = getPreferences(instance.id);
            instance.data.vibro = readVibro(prefs, vibroPref, vibroPrefSec);
        }
        dict.addInt16(KEY++, (short) instance.data.vibro);
        dict.addInt16(KEY++, (short) instance.data.auto_hide);
        dict.addInt16(KEY++, (short) (instance.data.fullscreen ? 1: 0));
        return true;
    }

    @Override
    public void receive(PebbleDictionary dict,
                        PebbleInstanceInfo<NotificationPluginInfo> instance) {
        if (null == dict) {
            return;
        }
        if (null != instance.data.lastNotification) {
            controller.parent().notificationCancelListener().cancel(instance.data.lastNotification);
            instance.data.lastNotification = null;
        }
    }
}
