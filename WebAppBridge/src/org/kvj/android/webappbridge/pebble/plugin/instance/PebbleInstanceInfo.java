package org.kvj.android.webappbridge.pebble.plugin.instance;

import android.graphics.Rect;
import android.service.notification.StatusBarNotification;

import org.kvj.android.webappbridge.pebble.plugin.PebblePluginInfo;

public class PebbleInstanceInfo<E> {

    public PebbleInstanceInfo(PebblePluginInfo<E> plugin) {
        this.plugin = plugin;
    }

    public PebblePluginInfo<E> plugin;
    public Rect bounds;
    public int id;
    public E data = null;

    public String getPreferencesName() {
        return "instance_" + id;
    }

}
