package org.kvj.android.webappbridge.pebble.plugin.impl.ui;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Switch;

import org.kvj.android.webappbridge.R;
import org.kvj.android.webappbridge.pebble.plugin.PebblePluginInfo;
import org.kvj.android.webappbridge.svc.WebBridgeController;
import org.kvj.bravo7.form.FormController;
import org.kvj.bravo7.form.ViewBundleAdapter;
import org.kvj.bravo7.form.impl.ViewFinder;
import org.kvj.bravo7.form.impl.bundle.BooleanBundleAdapter;
import org.kvj.bravo7.form.impl.bundle.IntegerBundleAdapter;
import org.kvj.bravo7.form.impl.bundle.StringBundleAdapter;
import org.kvj.bravo7.form.impl.widget.TextViewCharSequenceAdapter;
import org.kvj.bravo7.form.impl.widget.TransientAdapter;
import org.kvj.bravo7.ng.App;

public class NotificationRexpEditor extends DialogFragment {

    private static final String KEY_PACKAGE = "package";
    private static final String KEY_ID = "id";
    private static final String KEY_EXCLUDE = "exclude";
    private static final String KEY_PATTERNS = "patterns";
    private static final String KEY_VIBRO = "vibro";
    private FormController form;

    private WebBridgeController controller = App.controller();

    @Override
    public void onSaveInstanceState(Bundle data) {
        super.onSaveInstanceState(data);
        form.save(data);
    }

    public static NotificationRexpEditor newInstance(String packageName, int instance) {
        WebBridgeController controller = App.controller();
        SharedPreferences preferences = PebblePluginInfo.getPreferences(controller, instance);
        NotificationRexpEditor f = new NotificationRexpEditor();
        Bundle args = new Bundle();
        args.putString(KEY_PACKAGE, packageName);
        args.putInt(KEY_ID, instance);
        try {
            args.putBoolean(KEY_EXCLUDE, preferences.getBoolean(packageName + "_ex", false));
            args.putString(KEY_PATTERNS, preferences.getString(packageName + "_rexp", ""));
            args.putInt(KEY_VIBRO, preferences.getInt(packageName + "_vibro", 0));
        } catch (Exception e) {
        }
        f.setArguments(args);
        return f;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Pattern editor");
        LayoutInflater inflater = getActivity().getLayoutInflater();
        final View view = inflater.inflate(R.layout.notify_app_rexp, null);
        form = new FormController(new ViewFinder() {
            @Override
            public View findViewById(int id) {
                return view.findViewById(id);
            }
        });
        form.add(new TransientAdapter<>(new IntegerBundleAdapter(), 0), KEY_ID);
        form.add(new TransientAdapter<>(new StringBundleAdapter(), ""), KEY_PACKAGE);
        form.add(new TextViewCharSequenceAdapter(R.id.rexp_text, ""), KEY_PATTERNS);
        ViewBundleAdapter<Switch, Boolean> switchAdapter = new ViewBundleAdapter<Switch, Boolean>(
                new BooleanBundleAdapter(), R.id.rexp_type, false) {

            @Override
            public void setWidgetValue(Boolean value, Bundle bundle) {
                getView().setChecked(value);
            }

            @Override
            public Boolean getWidgetValue(Bundle bundle) {
                return getView().isChecked();
            }
        };
        ViewBundleAdapter<Spinner, Integer> spinnerAdapter = new ViewBundleAdapter<Spinner, Integer>(
                new IntegerBundleAdapter(), R.id.rexp_vibro, 0) {

            @Override
            public Integer getWidgetValue(Bundle bundle) {
                return getView().getSelectedItemPosition();
            }

            @Override
            public void setWidgetValue(Integer value, Bundle bundle) {
                getView().setSelection(value);
            }
        };
        form.add(switchAdapter, KEY_EXCLUDE);
        form.add(spinnerAdapter, KEY_VIBRO);
        form.load(null, savedInstanceState);
        builder.setView(view);
        Button saveButton = (Button) view.findViewById(R.id.rexp_ok);
        Button cancelButton = (Button) view.findViewById(R.id.rexp_cancel);
        saveButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                int instance = form.getValue(KEY_ID, Integer.class);
                SharedPreferences preferences = PebblePluginInfo.getPreferences(controller, instance);
                String packageName = form.getValue(KEY_PACKAGE, String.class);

                Editor edit = preferences.edit();
                edit.putString(packageName + "_rexp", form.getValue(KEY_PATTERNS, String.class));
                edit.putBoolean(packageName + "_ex", form.getValue(KEY_EXCLUDE, Boolean.class));
                edit.putInt(packageName + "_vibro", form.getValue(KEY_VIBRO, Integer.class));
                edit.commit();
                dismiss();
            }
        });
        cancelButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        setCancelable(false);
        return builder.create();
    }
}
