package org.kvj.android.webappbridge.pebble.plugin.impl;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.RemoteException;
import android.support.v4.app.NotificationCompat;
import android.text.TextUtils;

import com.getpebble.android.kit.util.PebbleDictionary;

import org.kvj.android.webappbridge.WebBridgeApp;
import org.kvj.android.webappbridge.ipc.AlarmProvider;
import org.kvj.android.webappbridge.pebble.PebbleController;
import org.kvj.android.webappbridge.pebble.PebbleController.PebbleCommandSender;
import org.kvj.android.webappbridge.pebble.plugin.PebblePluginInfo;
import org.kvj.android.webappbridge.pebble.plugin.impl.svc.AlarmTimerReceiver;
import org.kvj.android.webappbridge.pebble.plugin.impl.ui.AlarmConfig;
import org.kvj.android.webappbridge.pebble.plugin.instance.PebbleInstanceInfo;
import org.kvj.android.webappbridge.svc.WebBridgeController;
import org.kvj.android.webappbridge.ui.InstanceConfigActivity;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class AlarmPlugin extends PebblePluginInfo<AlarmPlugin.AlarmPluginInfo> {

    private static class OneEvent {

        String caption = "";
        int seconds = 0;
        boolean alarm = false;
        boolean silent = false;
        StringBuilder details = new StringBuilder();

        long todayDate() {
            Calendar c = Calendar.getInstance();
            c.set(Calendar.HOUR_OF_DAY, (int) Math.floor(seconds / 60 / 60));
            c.set(Calendar.MINUTE, (seconds / 60) % 60);
            c.set(Calendar.SECOND, 0);
            return c.getTimeInMillis();
        }
    }

    public static class AlarmPluginInfo {

        public String name = "";
        public int vibro = -1;
        public int advanceSeconds = 0;
        public int deferSeconds = 0;
        public String alarmTag = "";
        public String silentTag = "";
        public boolean showNotification = false;
        public boolean visualNotification = false;
        List<OneEvent> events = new ArrayList<>();
    }

    public AlarmPlugin(PebbleController controller) {
        super(controller);
    }

    private PendingIntent alarmIntent(PebbleInstanceInfo<AlarmPluginInfo> instance, String type) {
        Intent intent = new Intent(context, AlarmTimerReceiver.class);
        intent.setData(Uri.fromParts("plugin", type, Integer.toString(instance.id)));
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_CANCEL_CURRENT);
        return pendingIntent;
    }

    public void reset(int instanceID) {
        PebbleInstanceInfo<AlarmPluginInfo> instance =
                (PebbleInstanceInfo<AlarmPluginInfo>) controller.findInstance(instanceID);
        if (null == instance) {
            return;
        }
        setSchedule(instance, "");
        controller.sendInstanceCommand(instanceID, new PebbleCommandSender() {
            @Override
            public boolean send(int startID, PebbleDictionary dict, PebbleInstanceInfo instance) {
                int KEY = startID;
                dict.addInt32(KEY++, 2); // Reset
                return true;
            }
        });
    }

    private void sendEvent(final PebbleInstanceInfo<AlarmPluginInfo> instance, final OneEvent oe, final int waitSeconds) {
        String m = normalizedString(oe.caption, 40);
        String d = normalizedString(oe.details.toString(), 255);
        logger.d("sendEvent", m, d);

        controller.sendInstanceCommand(instance.id, new PebbleCommandSender<Object>() {

            @Override
            public boolean send(int startKey, PebbleDictionary dict, PebbleInstanceInfo<Object> ins) {
                int KEY = startKey;
                dict.addInt32(KEY++, waitSeconds);
                int alarmSeconds = waitSeconds - instance.data.advanceSeconds;
                if (alarmSeconds < 0) {
                    alarmSeconds = 0;
                }
                int vibro = instance.data.vibro;
                if (oe.alarm) {
                    vibro = 3; // Repeat
                }
                if (oe.silent) { // No Vibro
                    vibro = -1;
                }
                dict.addInt32(KEY++, alarmSeconds);
                dict.addInt16(KEY++, (short) vibro);
                return true;
            }
        }, m, d);
    }

    private int nowSeconds() {
        Calendar c = Calendar.getInstance();
        return seconds(c.get(Calendar.HOUR_OF_DAY), c.get(Calendar.MINUTE), c.get(Calendar.SECOND));
    }

    private OneEvent nextEvent(final PebbleInstanceInfo<AlarmPluginInfo> instance, int shiftSec) {
        int now = nowSeconds() + shiftSec;
        for (int i = 0; i < instance.data.events.size(); i++) { // $COMMENT
            OneEvent oe = instance.data.events.get(i);
            if (oe.seconds>now) {
                return oe;
            }
        }
        return null;
    }

    private boolean reschedule(final PebbleInstanceInfo<AlarmPluginInfo> instance, boolean sendEvent) {
        WebBridgeController appController = WebBridgeApp.controller();

        appController.cancelAlarm(alarmIntent(instance, "pebble"));
        appController.cancelAlarm(alarmIntent(instance, "notify"));
        logger.d("Cancelled existing");
        OneEvent event = nextEvent(instance, instance.data.advanceSeconds + instance.data.deferSeconds);
        if (null == event) { // No upcoming events found
            saveSchedule(instance, "");
            appController.messageShort("No next events");
            return false;
        }
        appController.messageShort("Next: "+event.caption);
        long msec = event.todayDate() + 1000 * instance.data.deferSeconds; // Send next event
        appController.scheduleAlarm(new Date(msec), alarmIntent(instance, "pebble")); // Next alarm will be delivered
        logger.d("Set alarm", new Date(msec));
        if (instance.data.showNotification) { // Schedule notification
            msec = event.todayDate() - 1000 * instance.data.advanceSeconds; // Show current event
            logger.d("Set notify alarm", new Date(msec));
            appController.scheduleAlarm(new Date(msec), alarmIntent(instance, "notify")); // Next alarm will be delivered
        }
        if (sendEvent) {
            sendEvent(instance, event, event.seconds - nowSeconds());
        }
        return true;
    }

    private void saveSchedule(PebbleInstanceInfo<AlarmPluginInfo> info, String s) {
        getPreferences(info.id).edit().putString("schedule", s).commit();
    }

    AlarmProvider.Stub provider = new AlarmProvider.Stub() {

        @Override
        public boolean setAlarm(final long when, String message, final int alarmType, final long alarmWhen)
                throws RemoteException {
            return true;
        }
    };

    @Override
    public String getCaption() {
        return "Alarm";
    }

    @Override
    public String getID() {
        return "alarm";
    }

    public AlarmProvider.Stub getAlarmProvider() {
        return provider;
    }

    @Override
    public boolean startConfig(PebbleInstanceInfo<AlarmPluginInfo> instance, Activity startActivity) {
        InstanceConfigActivity.startConfig(startActivity, AlarmConfig.class, instance);
        return true;
    }

    @Override
    protected void initNew(SharedPreferences prefs, PebbleInstanceInfo<AlarmPluginInfo> instance) {
        instance.bounds.right = 80;
        instance.bounds.bottom = 20;
    }

    @Override
    protected void readFromPreferences(SharedPreferences prefs,
                                       PebbleInstanceInfo<AlarmPluginInfo> instance) {
        instance.data = new AlarmPluginInfo();
        instance.data.name = prefs.getString("name", instance.data.name);
        instance.data.vibro = readVibro(prefs, "vibro", "vibro_seconds");
        instance.data.advanceSeconds = fromSettings(prefs, "advance", 0);
        instance.data.deferSeconds = fromSettings(prefs, "next", 30);
        instance.data.alarmTag = prefs.getString("alarm_alarm_tag", "");
        instance.data.silentTag = prefs.getString("alarm_silent_tag", "");
        instance.data.showNotification = prefs.getBoolean("show_notification", false);
        instance.data.visualNotification = prefs.getBoolean("visual_notification", false);
        String schedule = prefs.getString("schedule", "");
        if (!TextUtils.isEmpty(schedule)) { // Have schedule saved before
            parseSchedule(instance, schedule);
            reschedule(instance, false);
        }
        logger.d("Restore alarm", prefs);
    }

    private static Pattern linePattern = Pattern.compile("^\\s*(\\S\\s)?(\\d{1,2}):(\\d{2})(\\s*-\\s*(\\d{1,2}):(\\d{2}))?\\s(.+)$");

    private int seconds(int hours, int minutes, int seconds) {
        return (60 * hours + minutes) * 60 + seconds;
    }

    public void setSchedule(PebbleInstanceInfo<AlarmPluginInfo> info, String text) {
        saveSchedule(info, text);
        parseSchedule(info, text);
        reschedule(info, true);
    }

    private void parseSchedule(
        PebbleInstanceInfo<AlarmPluginInfo> info, String text) {
        info.data.events.clear();
        // logger.d("parseSchedule:", info.id, text);
        if (TextUtils.isEmpty(text)) {
            return;
        }
        // Parse
        String[] lines = text.split("\n");
        OneEvent lastEvent = null;
        for (int i = 0; i < lines.length; i++) { // $COMMENT
            String line = lines[i];
            Matcher m = linePattern.matcher(line);
            if (!m.find()) {
                if (null == lastEvent || TextUtils.isEmpty(line)) {
                    continue;
                }
                if (lastEvent.details.length()>0) { // Have something
                    lastEvent.details.append('\n');
                }
                lastEvent.details.append(line.trim());
                continue;
            };
            OneEvent event = new OneEvent();
            event.caption = m.group(7).trim();
            if (!TextUtils.isEmpty(info.data.alarmTag) && event.caption.contains(" "+info.data.alarmTag)) {
                event.alarm = true;
            }
            if (!TextUtils.isEmpty(info.data.silentTag) && event.caption.contains(" "+info.data.silentTag)) {
                event.silent = true;
            }
            event.seconds = seconds(Integer.parseInt(m.group(2)), Integer.parseInt(m.group(3)), 0);
            lastEvent = event;
            logger.d("New event", event.caption, event.seconds, event.alarm, event.silent);
            info.data.events.add(event);
        }
        Collections.sort(info.data.events, new Comparator<OneEvent>() {
            @Override
            public int compare(OneEvent oneEvent, OneEvent oneEvent2) {
                return oneEvent.seconds - oneEvent2.seconds;
            }
        });
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        try {
            int instanceID = Integer.parseInt(intent.getData().getFragment());
            String type = intent.getData().getSchemeSpecificPart();
            logger.d("Alarm received:", intent, type);
            PebbleInstanceInfo<AlarmPluginInfo> info =
                    (PebbleInstanceInfo<AlarmPluginInfo>) controller.findInstance(instanceID);
            if (null == info) { // Not found
                return;
            }
            logger.d("Found instance", instanceID, info, intent.getData());
            if ("pebble".equals(type)) { // Pebble
                reschedule(info, true);
            }
            if ("notify".equals(type)) { // Show notification
                if (info.data.showNotification) { // Still active
                    OneEvent event = nextEvent(info, 0); // Closest to now
                    if (null != event) { // Have event
                        NotificationCompat.Builder builder = controller.newNotification(info.data.visualNotification);
                        if (event.silent) { // Remove all visuals
                            builder.setDefaults(0);
                        }
                        builder.setTicker(event.caption);
                        builder.setContentTitle(String.format("Alarm '%s'", info.data.name));
                        builder.setContentText(event.caption);
                        controller.showPluginNotification(info, 1, builder.build());
                        builder.setWhen(event.todayDate());
                        builder.setShowWhen(true);
                        if (!TextUtils.isEmpty(event.details)) {
                            NotificationCompat.InboxStyle inbox = new NotificationCompat.InboxStyle();
                            String[] lines = event.details.toString().split("\n");
                            for (String line : lines) {
                                inbox.addLine(line);
                            }
                            builder.setStyle(inbox);
                        }
                    }
                }
                reschedule(info, true); // Show next event, if available
            }
        } catch (Exception e) {
            logger.e(e, "Error:", intent);
        }
    }

    @Override
    public void receive(PebbleDictionary dict, PebbleInstanceInfo<AlarmPluginInfo> instance) {
        if (instance.data.events.isEmpty()) { // No data
            return;
        }
        reschedule(instance, true);
    }
}
