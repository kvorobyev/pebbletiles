package org.kvj.android.webappbridge.pebble.plugin.impl.ui;

import android.preference.Preference;

import org.kvj.android.webappbridge.R;
import org.kvj.android.webappbridge.pebble.plugin.impl.AlarmPlugin;
import org.kvj.android.webappbridge.ui.InstanceConfigActivity;

/**
 * Created by vorobyev on 3/26/15.
 */
public class AlarmConfig extends InstanceConfigActivity  {

    @Override
    protected int getSettingsConfig() {
        return R.xml.alarm_config;
    }
    @Override
    protected void onPreferencesLoaded(InstanceSettingsFragment fragment) {
        fragment.findPreference("reset").setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {

            @Override
            public boolean onPreferenceClick(Preference preference) {
                AlarmPlugin alarmPlugin = (AlarmPlugin) controller.getPebble().findPlugin("alarm");
                alarmPlugin.reset(instanceID);
                return true;
            }
        });
    }
}
