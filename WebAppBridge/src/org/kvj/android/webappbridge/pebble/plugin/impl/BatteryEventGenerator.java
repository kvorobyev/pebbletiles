package org.kvj.android.webappbridge.pebble.plugin.impl;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.BatteryManager;

import org.kvj.bravo7.log.Logger;

abstract public class BatteryEventGenerator extends BroadcastReceiver {

    private static final int MAX_LEVEL_DIFF = 3;
    int lastAnnounceLevel = -1;
    int lastAnnounceState = -1;
    private Logger logger = Logger.forInstance(this);

    public BatteryEventGenerator(Context context) {
        IntentFilter filter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
        context.registerReceiver(this, filter);
    }

    @Override
    public void onReceive(Context arg0, Intent intent) {
        int level = intent.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
        int status = intent.getIntExtra(BatteryManager.EXTRA_STATUS, -1);
        int state = 0;
        if (status == BatteryManager.BATTERY_STATUS_CHARGING
                || status == BatteryManager.BATTERY_STATUS_FULL) {
            state = 1;
        }
//        logger.d("Battery: ", level, state);
        if (state != lastAnnounceState || Math.abs(level - lastAnnounceLevel) >= MAX_LEVEL_DIFF) {
            // Send message
//            logger.d("Battery - send: ", level, state);
            lastAnnounceState = state;
            lastAnnounceLevel = level;
            sendCurrent();
        }
    }

    abstract public void sendCurrent();
}
