package org.kvj.android.webappbridge.pebble.plugin.impl.ui;

import org.kvj.android.webappbridge.R;
import org.kvj.android.webappbridge.ui.InstanceConfigActivity;

public class CountdownConfig extends InstanceConfigActivity {

    @Override
    protected int getSettingsConfig() {
        return R.xml.cdown_config;
    }

}
