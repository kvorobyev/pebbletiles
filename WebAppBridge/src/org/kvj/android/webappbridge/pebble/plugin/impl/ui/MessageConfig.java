package org.kvj.android.webappbridge.pebble.plugin.impl.ui;

import org.kvj.android.webappbridge.R;
import org.kvj.android.webappbridge.ui.InstanceConfigActivity;

/**
 * Created by kvorobyev on 12/27/14.
 */
public class MessageConfig extends InstanceConfigActivity {
    @Override
    protected int getSettingsConfig() {
        return R.xml.message_config;
    }
}
