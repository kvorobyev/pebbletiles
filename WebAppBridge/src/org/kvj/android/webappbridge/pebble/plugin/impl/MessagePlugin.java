package org.kvj.android.webappbridge.pebble.plugin.impl;

import android.app.Activity;
import android.app.Notification;
import android.content.SharedPreferences;
import android.support.v4.app.NotificationCompat;
import android.text.TextUtils;

import com.getpebble.android.kit.util.PebbleDictionary;

import org.kvj.android.webappbridge.R;
import org.kvj.android.webappbridge.pebble.PebbleController;
import org.kvj.android.webappbridge.pebble.plugin.PebblePluginInfo;
import org.kvj.android.webappbridge.pebble.plugin.impl.ui.MessageConfig;
import org.kvj.android.webappbridge.pebble.plugin.instance.PebbleInstanceInfo;
import org.kvj.android.webappbridge.ui.InstanceConfigActivity;
import org.kvj.android.webappbridge.ui.appwidget.Widget00;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 * Created by kvorobyev on 12/27/14.
 */
public class MessagePlugin extends PebblePluginInfo<MessagePlugin.MessagePluginInfo> {

    public static final String ID = "message";

    public MessagePlugin(PebbleController controller) {
        super(controller);
    }

    @Override
    public String getCaption() {
        return "Message";
    }

    @Override
    public String getID() {
        return ID;
    }

    public static class MessagePluginInfo {
        String name = "";
        String title = "";
        String detail = "";
        int vibro = -1;
        int font = 0;
        boolean asNotification = false;
        Date when = null;
        public boolean visualNofitication = false;
    }

    public void newMessage(int vibro, String type, String title, String message) {
        List<PebbleInstanceInfo<MessagePluginInfo>> instances = controller.getInstances(ID,
                MessagePluginInfo.class);
        for (PebbleInstanceInfo<MessagePluginInfo> instance : instances) {
            if (instance.data.name.equals(type)) { // Type OK
                sendMessage(instance, vibro, title, message);
            }
        }
    }


    private static DateFormat tsFormat = new SimpleDateFormat("H:mm M/d");
    public void sendMessage(PebbleInstanceInfo<MessagePluginInfo> instance, int vibro, String title, String message) {
        logger.d("send:", instance.data.title);
        instance.data.title = title;
        instance.data.when = new Date();
        int nIdx = instance.data.title.indexOf('\n');
        if (nIdx != -1) {
            instance.data.title = instance.data.title.substring(0, nIdx);
        }
        if (TextUtils.isEmpty(message)) {
            instance.data.detail = "";
        } else {
            instance.data.detail = message+"\n// "+tsFormat.format(instance.data.when);
        }
        instance.data.vibro = vibro;
        SharedPreferences.Editor prefs = getPreferences(instance.id).edit();
        prefs.putString("title", instance.data.title);
        prefs.putString("detail", instance.data.detail);
        prefs.commit();
        sendInstance(instance);
        refreshNotification(instance);
    }

    private void sendInstance(PebbleInstanceInfo<MessagePluginInfo> instance) {
        controller.sendInstanceCommand(instance.id, this,
                                       normalizedString(instance.data.title, 40),
                                       normalizedString(instance.data.detail, 255));
    }

    @Override
    public boolean send(int startKey, PebbleDictionary dict, PebbleInstanceInfo<MessagePluginInfo> instance) {
        logger.d("send2:", instance.data.title);
        int KEY = startKey;
        dict.addInt16(KEY++, (short) instance.data.vibro);
        dict.addInt16(KEY++, (short) instance.data.font);
        return true;
    }

    @Override
    protected void readFromPreferences(SharedPreferences prefs,
                                       PebbleInstanceInfo<MessagePlugin.MessagePluginInfo> instance) {
        instance.data = new MessagePlugin.MessagePluginInfo();
        instance.data.name = prefs.getString("name", "");
        instance.data.font = prefs.getBoolean("bold_font", false) ? 1 : 0;
        instance.data.title = prefs.getString("title", "");
        instance.data.detail = prefs.getString("detail", "");
        instance.data.asNotification = prefs.getBoolean("show_notification", false);
        instance.data.visualNofitication = prefs.getBoolean("visual_notification", false);
        refreshNotification(instance);
    }

    private void refreshNotification(PebbleInstanceInfo<MessagePlugin.MessagePluginInfo> instance) {
        if (!TextUtils.isEmpty(instance.data.detail)) {
            List<String> lines = new ArrayList<>();
            Collections.addAll(lines, instance.data.detail.split("\n"));
            Widget00.loadText(context, instance.data.title, lines, instance.data.name);
        }
        if (TextUtils.isEmpty(instance.data.title) || !instance.data.asNotification) {
            controller.hidePluginNotification(instance);
        } else {
            NotificationCompat.Builder builder = new NotificationCompat.Builder(context);
            builder.setContentTitle(String.format("Message '%s'", instance.data.name));
            builder.setSmallIcon(R.drawable.ic_stat_normal);
            builder.setContentText(instance.data.title);
            if (!TextUtils.isEmpty(instance.data.detail)) {
                NotificationCompat.InboxStyle inbox = new NotificationCompat.InboxStyle();
                String[] lines = instance.data.detail.split("\n");
                for (String line : lines) {
                    inbox.addLine(line);
                }
                builder.setStyle(inbox);
            }
            builder.setTicker(instance.data.title);
            if (null != instance.data.when) {
                builder.setWhen(instance.data.when.getTime());
            }
            if (instance.data.visualNofitication) { // Show visual app
                builder.setOnlyAlertOnce(false);
                builder.setOngoing(false);
                builder.setDefaults(NotificationCompat.DEFAULT_ALL);
                builder.setPriority(Notification.PRIORITY_DEFAULT);
            } else { // Silent notification
                builder.setOnlyAlertOnce(true);
                builder.setOngoing(true);
                builder.setPriority(Notification.PRIORITY_MIN);
            }
            builder.addAction(R.drawable.ic_stat_normal, "Dismiss", controller.actionIntent(instance, 0));
            controller.showPluginNotification(instance, instance.id, builder.build());
        }
    }

    @Override
    public boolean startConfig(PebbleInstanceInfo instance, Activity startActivity) {
        InstanceConfigActivity.startConfig(startActivity, MessageConfig.class, instance);
        return true;
    }

    @Override
    protected void initNew(SharedPreferences prefs, PebbleInstanceInfo<MessagePluginInfo> instance) {
        super.initNew(prefs, instance);
        instance.bounds.right = 80;
        instance.bounds.bottom = 20;
    }

    @Override
    public String getInstanceTitle(PebbleInstanceInfo<?> instance) {
        PebbleInstanceInfo<MessagePluginInfo> _instance = (PebbleInstanceInfo<MessagePluginInfo>) instance;
        return getCaption() + " '" + _instance.data.name + "'";
    }

    @Override
    public void receive(PebbleDictionary dict, PebbleInstanceInfo<MessagePluginInfo> instance) {
        if (TextUtils.isEmpty(instance.data.title)) { // No data
            return;
        }
        sendInstance(instance);
    }

    @Override
    public void handleAction(PebbleInstanceInfo<?> instance, int index) {
        PebbleInstanceInfo<MessagePluginInfo> _instance = (PebbleInstanceInfo<MessagePluginInfo>) instance;
        logger.d("Clicked on action", index);
        if (0 == index) {
            sendMessage(_instance, -1, "", ""); // Will clear notification
        }
    }
}
