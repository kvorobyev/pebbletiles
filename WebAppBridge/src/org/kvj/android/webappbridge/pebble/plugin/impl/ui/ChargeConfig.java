package org.kvj.android.webappbridge.pebble.plugin.impl.ui;

import org.kvj.android.webappbridge.R;
import org.kvj.android.webappbridge.ui.InstanceConfigActivity;

/**
 * Created by vorobyev on 3/26/15.
 */
public class ChargeConfig extends InstanceConfigActivity  {

    @Override
    protected int getSettingsConfig() {
        return R.xml.charge_config;
    }
}
