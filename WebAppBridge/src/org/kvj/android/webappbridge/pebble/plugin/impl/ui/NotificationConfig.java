package org.kvj.android.webappbridge.pebble.plugin.impl.ui;

import android.preference.Preference;
import android.preference.Preference.OnPreferenceClickListener;

import org.kvj.android.webappbridge.R;
import org.kvj.android.webappbridge.ui.InstanceConfigActivity;

public class NotificationConfig extends InstanceConfigActivity {

    @Override
    protected int getSettingsConfig() {
        return R.xml.notify_config;
    }

    @Override
    protected void onPreferencesLoaded(InstanceSettingsFragment fragment) {
        fragment.findPreference("apps").setOnPreferenceClickListener(new OnPreferenceClickListener() {

            @Override
            public boolean onPreferenceClick(Preference preference) {
                startConfig(NotificationConfig.this, NotificationAppSelector.class, instance);
                return true;
            }
        });
    }

}
