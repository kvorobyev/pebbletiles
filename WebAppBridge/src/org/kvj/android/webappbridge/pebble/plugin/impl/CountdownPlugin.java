package org.kvj.android.webappbridge.pebble.plugin.impl;

import android.app.Activity;
import android.content.SharedPreferences;

import com.getpebble.android.kit.util.PebbleDictionary;

import org.kvj.android.webappbridge.pebble.PebbleController;
import org.kvj.android.webappbridge.pebble.plugin.PebblePluginInfo;
import org.kvj.android.webappbridge.pebble.plugin.impl.CountdownPlugin.CoundownInfo;
import org.kvj.android.webappbridge.pebble.plugin.impl.ui.CountdownConfig;
import org.kvj.android.webappbridge.pebble.plugin.instance.PebbleInstanceInfo;
import org.kvj.android.webappbridge.ui.InstanceConfigActivity;

public class CountdownPlugin extends PebblePluginInfo<CoundownInfo> {

    public CountdownPlugin(PebbleController controller) {
        super(controller);
    }

    public static class CoundownInfo {
        public int seconds;
        public boolean cycle = false;
        public int vibro = 0;
    }

    @Override
    public String getCaption() {
        return "Countdown";
    }

    @Override
    public String getID() {
        return "cdown";
    }

    @Override
    protected void readFromPreferences(SharedPreferences prefs, PebbleInstanceInfo<CoundownInfo> instance) {
        instance.data = new CoundownInfo();
        instance.data.seconds = parseTime(prefs.getString("seconds", "0"));
        instance.data.cycle = prefs.getBoolean("cycle", false);
        instance.data.vibro = readVibro(prefs, "vibro", "vibro_seconds");
    }

    @Override
    public boolean startConfig(PebbleInstanceInfo<CoundownInfo> instance, Activity startActivity) {
        InstanceConfigActivity.startConfig(startActivity, CountdownConfig.class, instance);
        return true;
    }

    @Override
    protected void initNew(SharedPreferences prefs, PebbleInstanceInfo<CoundownInfo> instance) {
        instance.bounds.right = 40;
        instance.bounds.bottom = 20;
    }

    @Override
    public void receive(PebbleDictionary dict, PebbleInstanceInfo<CoundownInfo> instance) {
        controller.sendInstanceCommand(instance.id, this);
    }

    @Override
    public boolean send(int startKey, PebbleDictionary dict, PebbleInstanceInfo<CoundownInfo> instance) {
        int KEY = startKey;
        dict.addInt16(KEY++, (short) instance.data.seconds);
        dict.addInt16(KEY++, (short) (instance.data.cycle ? 1 : 0));
        dict.addInt16(KEY++, (short) instance.data.vibro);
        dict.addInt16(KEY++, (short) 1);
        return true;
    }

    @Override
    public String getInstanceTitle(PebbleInstanceInfo instance) {
        return super.getInstanceTitle(instance) + " " + getPreferences(instance.id).getString("seconds", "00:00");
    }

}
