package org.kvj.android.webappbridge.pebble.plugin.impl;

import android.content.SharedPreferences;

import com.getpebble.android.kit.util.PebbleDictionary;

import org.kvj.android.webappbridge.pebble.PebbleController;
import org.kvj.android.webappbridge.pebble.plugin.PebblePluginInfo;
import org.kvj.android.webappbridge.pebble.plugin.instance.PebbleInstanceInfo;

/**
 * Created by kvorobyev on 9/26/14.
 */
public class StopwatchPlugin extends PebblePluginInfo<Object> {

    public StopwatchPlugin(PebbleController controller) {
        super(controller);
    }

    @Override
    public String getCaption() {
        return "Stopwatch";
    }

    @Override
    public String getID() {
        return "stopwatch";
    }

    @Override
    protected void initNew(SharedPreferences prefs, PebbleInstanceInfo<Object> instance) {
        instance.bounds.right = 60;
        instance.bounds.bottom = 20;
    }

    @Override
    public void receive(PebbleDictionary dict, PebbleInstanceInfo<Object> instance) {
        if (null == dict) { // No data
            return;
        }
    }
}
