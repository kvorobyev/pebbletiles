package org.kvj.android.webappbridge.ui;

import android.content.ActivityNotFoundException;
import android.content.ClipData;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.DragEvent;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnDragListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.ParseInstallation;

import org.kvj.android.webappbridge.BuildConfig;
import org.kvj.android.webappbridge.R;
import org.kvj.android.webappbridge.WebBridgeApp;
import org.kvj.android.webappbridge.pebble.plugin.PebblePluginInfo;
import org.kvj.android.webappbridge.pebble.plugin.impl.MessagePlugin;
import org.kvj.android.webappbridge.pebble.plugin.instance.PebbleInstanceInfo;
import org.kvj.android.webappbridge.svc.BackgroundService;
import org.kvj.android.webappbridge.svc.WebBridgeController;
import org.kvj.android.webappbridge.ui.adapter.InstanceListAdapter;
import org.kvj.android.webappbridge.ui.adapter.InstanceListAdapter.InstanceListListener;
import org.kvj.android.webappbridge.ui.view.ScreenCanvas;
import org.kvj.bravo7.SuperActivity;
import org.kvj.bravo7.log.Logger;
import org.kvj.bravo7.ng.App;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class LayoutConfig extends AppCompatActivity implements InstanceListListener {

    private class MoveButtonListener implements View.OnClickListener {

        private int x;
        private int y;

        public MoveButtonListener(int x, int y) {
            this.x = x;
            this.y = y;
        }

        @Override
        public void onClick(View v) {
            if (null != mDrawerListAdapter) {
                mConfigScreen.move(x, y);
            }
        }

    }

    protected static final String INSTANCE_DRAG = "PebbleInstance";

    protected static final String INSTANCE_MTYPE = "pebble/plugin-instance";

    private DrawerLayout mDrawerLayout;
    private ImageButton mAddInstanceBtn;
    private InstanceListAdapter mDrawerListAdapter;
    private ListView mDrawerList;
    private ScreenCanvas mConfigScreen;
    private ImageButton mRemoveInstanceBtn;
    private ImageButton mConfigInstanceBtn;
    private TextView mTitleInstance;
    private Toolbar mToolbar = null;

    private Logger logger = Logger.forInstance(this);
    private WebBridgeController controller = App.controller();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_config);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer);
        mDrawerList = (ListView) findViewById(R.id.drawer_list);
        mConfigScreen = (ScreenCanvas) findViewById(R.id.editor_screen_canvas);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        setupToolbar(mToolbar);
//        getSupportActionBar().setHomeButtonEnabled(true);
        mAddInstanceBtn = (ImageButton) findViewById(R.id.drawer_add_instance);
        mAddInstanceBtn.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                startAddInstance();
            }
        });
        registerForContextMenu(mAddInstanceBtn);
        mDrawerList.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int index, long id) {
                mDrawerListAdapter.select(index);
                mDrawerLayout.closeDrawers();
            }
        });
        mDrawerList.setOnItemLongClickListener(new OnItemLongClickListener() {

            @Override
            public boolean onItemLongClick(AdapterView<?> arg0, View view, int index, long id) {
                Intent intent = new Intent(INSTANCE_DRAG);
                intent.putExtra("index", index);
                ClipData.Item item = new ClipData.Item(intent);
                ClipData dragData = new ClipData(INSTANCE_DRAG, new String[]{INSTANCE_MTYPE}, item);
                View.DragShadowBuilder myShadow = new SimpleShadowBuilder(view);
                view.startDrag(dragData, // the data to be dragged
                        myShadow, // the drag shadow builder
                        null, // no need to use local data
                        0 // flags (not currently used, set to 0)
                );
                return true;
            }
        });

        mDrawerList.setOnDragListener(new OnDragListener() {

            @Override
            public boolean onDrag(View v, DragEvent event) {
                final int action = event.getAction();
                switch (action) {
                    case DragEvent.ACTION_DRAG_STARTED:
                        if (event.getClipDescription().hasMimeType(INSTANCE_MTYPE)) {
                            logger.d("Drag:", mDrawerList.pointToPosition((int) event.getX(), (int) event.getY()));
                            return true;
                        }
                        return true;
                    case DragEvent.ACTION_DRAG_LOCATION:
                        int onIndex = mDrawerList.pointToPosition((int) event.getX(), (int) event.getY());
                        if (mDrawerListAdapter.getDropIndex() != onIndex) {
                            mDrawerListAdapter.setDropIndex(onIndex);
                        }
                        return true;
                    case DragEvent.ACTION_DRAG_ENDED:
                        mDrawerListAdapter.setDropIndex(-1);
                        return true;
                    case DragEvent.ACTION_DROP:
                        int index = event.getClipData().getItemAt(0).getIntent().getIntExtra("index", -1);
                        controller.getPebble().moveInstance(index, mDrawerListAdapter.getDropIndex());
                        mDrawerListAdapter.select(-1);
                        return true;
                }
                return false;
            }
        });
        findViewById(R.id.editor_screen_b_top).setOnClickListener(new MoveButtonListener(0, -1));
        findViewById(R.id.editor_screen_b_bottom).setOnClickListener(new MoveButtonListener(0, 1));
        findViewById(R.id.editor_screen_b_left).setOnClickListener(new MoveButtonListener(-1, 0));
        findViewById(R.id.editor_screen_b_right).setOnClickListener(new MoveButtonListener(1, 0));
        mRemoveInstanceBtn = (ImageButton) findViewById(R.id.editor_screen_remove);
        mConfigInstanceBtn = (ImageButton) findViewById(R.id.editor_screen_config);
        mRemoveInstanceBtn.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                removeSelectedInstance();
            }
        });
        mConfigInstanceBtn.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                configSelectedInstance();
            }
        });
        mTitleInstance = (TextView) findViewById(R.id.editor_title_instance);
        onController();
        if (controller.getPebble().getInstances().isEmpty()) { // Can create default
            createDefault();
        }
        startService(new Intent(this, BackgroundService.class));
    }

    private void createDefault() {
        // Time, battery level, connection link, notifications
        PebbleInstanceInfo instance = controller.getPebble().newInstance("time");
        instance.bounds = new Rect(0, 75, 120, 55);
        controller.getPebble().saveInstance(instance);
        instance = controller.getPebble().newInstance("charge");
        instance.bounds = new Rect(120, 75, 20, 70);
        controller.getPebble().saveInstance(instance);
        instance = controller.getPebble().newInstance("link");
        instance.bounds = new Rect(120, 145, 20, 20);
        controller.getPebble().saveInstance(instance);
        instance = controller.getPebble().newInstance("stopwatch");
        instance.bounds = new Rect(0, 145, 40, 20);
        controller.getPebble().saveInstance(instance);
        instance = controller.getPebble().newInstance("notify");
        instance.bounds = new Rect(0, 0, 140, 55);
        controller.getPebble().saveInstance(instance);
        mConfigScreen.showInstances();
        mDrawerListAdapter.notifyDataSetChanged();
    }

    private void setupToolbar(Toolbar mToolbar) {
        mToolbar.setNavigationIcon(getDrawerToggleDelegate().getThemeUpIndicator());
        mToolbar.setNavigationContentDescription(R.string.app_name);
        mToolbar.setNavigationOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mDrawerLayout.isDrawerOpen(Gravity.LEFT)) {
                    mDrawerLayout.closeDrawer(Gravity.LEFT);
                } else {
                    mDrawerLayout.openDrawer(Gravity.LEFT);
                }
            }
        });
    }

    protected void configSelectedInstance() {
        int index = mDrawerListAdapter.indexByID(mDrawerListAdapter.getSelected());
        if (-1 == index) {
            return;
        }
        PebbleInstanceInfo instance = mDrawerListAdapter.getItem(index);
        if (!instance.plugin.startConfig(instance, this)) {
            WebBridgeApp.toast("No configuration inteface available");
        }

    }

    protected void removeSelectedInstance() {
        int index = mDrawerListAdapter.indexByID(mDrawerListAdapter.getSelected());
        if (-1 == index) {
            return;
        }
        final PebbleInstanceInfo instance = mDrawerListAdapter.getItem(index);
        SuperActivity.showQuestionDialog(this, "Remove instance?", "Are you sure want to remove selected instance?", new Runnable() {

            @Override
            public void run() {
                controller.getPebble().removeInstance(instance);
                mDrawerListAdapter.select(-1);
                mConfigScreen.showInstances();
            }
        });
    }

    protected void startAddInstance() {
        openContextMenu(mAddInstanceBtn);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v,
                                    ContextMenuInfo menuInfo) {
        menu.clear();
        int index = 0;
        for (PebblePluginInfo pinfo : controller.getPebble().getPlugins()) {
            MenuItem item = menu.add(0, index++, 0, pinfo.getCaption());
            item.setActionView(mAddInstanceBtn);
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        if (mAddInstanceBtn.equals(item.getActionView())) {
            @SuppressWarnings("rawtypes")
            PebblePluginInfo pluginInfo = controller.getPebble().getPlugins().get(item.getItemId());
            PebbleInstanceInfo<?> instance = controller.getPebble().newInstance(pluginInfo.getID());
            if (null != instance) {
                // Refresh adapter
                mConfigScreen.showInstances();
                mDrawerListAdapter.select(controller.getPebble().getInstances().size() - 1);
                // Select last
            }
        }
        return true;
    }

    public void onController() {
        mDrawerListAdapter = new InstanceListAdapter(controller.getPebble());
        mDrawerList.setAdapter(mDrawerListAdapter);
        mConfigScreen.setAdapter(mDrawerListAdapter);
        mDrawerListAdapter.addListener(this);
        Intent startIntent = getIntent();
        if (null != startIntent && Intent.ACTION_SEND.equals(startIntent.getAction())) {
            // Share text - send to widget/message
            handleSharing(startIntent.getStringExtra(Intent.EXTRA_TEXT));
        }
        mConfigScreen.showInstances();
    }

    private void handleSharing(final String stringExtra) {
        // Show dialog
        final List<PebbleInstanceInfo<MessagePlugin.MessagePluginInfo>>
            instances =
            controller.getPebble().getInstances(MessagePlugin.ID, MessagePlugin.MessagePluginInfo.class);
        final MessagePlugin plugin = (MessagePlugin) controller.getPebble().findPlugin(MessagePlugin.ID);
        CharSequence titles[] = new CharSequence[instances.size()];
        int index = 0;
        for (PebbleInstanceInfo<MessagePlugin.MessagePluginInfo> instance : instances) {
            String instanceTitle = plugin.getInstanceTitle(instance);
            titles[index++] = instanceTitle;
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Select Destination");
        final Set<Integer> selectors = new HashSet<>();
        builder.setMultiChoiceItems(titles, null,
            new DialogInterface.OnMultiChoiceClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i,
                                    boolean b) {
                    if(b) {
                        selectors.add(i);
                    } else {
                        selectors.remove(i);
                    }
                }
            });
        builder.setPositiveButton("Push", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                logger.d("Will share with:", selectors);
                for (int index: selectors) {
                    plugin.sendMessage(instances.get(index), -1, stringExtra, stringExtra);
                }
                finish();
            }
        });
        builder.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);
        menu.findItem(R.id.s_menu_show_id).setVisible(BuildConfig.DEBUG);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.s_menu_install:
                Intent browserIntent = new Intent(Intent.ACTION_VIEW);
                browserIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                browserIntent.setData(Uri.parse("pebble://appstore/549e223cef9cfaabe000004b"));
                try {
                    startActivity(browserIntent);
                } catch (ActivityNotFoundException e) {
                    logger.e(e, "Failed to open Activity");
                    Toast toast = Toast.makeText(this, "No Pebble App?", Toast.LENGTH_LONG);
                    toast.show();
                }
                break;
            case R.id.s_menu_init:
                controller.getPebble().clearState();
                controller.getPebble().resetInstances();
                break;
            case R.id.s_menu_settings:
                startActivity(new Intent(getApplicationContext(), SettingsActivity.class));
                break;
            case R.id.s_menu_show_id:
                AlertDialog.Builder dialog = new AlertDialog.Builder(this);
                dialog.setMessage(String.format("My ID: %s", ParseInstallation.getCurrentInstallation().getInstallationId()));
                dialog.setPositiveButton("Dismiss", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                    }
                });
                dialog.show();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void itemSelected(int id) {
        mRemoveInstanceBtn.setEnabled(id != -1);
        mConfigInstanceBtn.setEnabled(id != -1);
        if (id == -1) {
            mTitleInstance.setText("");
        } else {
            int index = mDrawerListAdapter.indexByID(id);
            PebbleInstanceInfo instance = mDrawerListAdapter.getItem(index);
            mTitleInstance.setText(instance.plugin.getInstanceTitle(instance));
        }
    }

    @Override
    public void itemChanged(int id) {
        int index = mDrawerListAdapter.indexByID(id);
        PebbleInstanceInfo instance = mDrawerListAdapter.getItem(index);
        controller.getPebble().saveInstance(instance);
    }
}
