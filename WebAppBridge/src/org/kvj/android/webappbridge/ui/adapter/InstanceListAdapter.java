package org.kvj.android.webappbridge.ui.adapter;

import android.graphics.Color;
import android.view.View;
import android.widget.TextView;

import org.kvj.android.webappbridge.R;
import org.kvj.android.webappbridge.pebble.PebbleController;
import org.kvj.android.webappbridge.pebble.plugin.instance.PebbleInstanceInfo;
import org.kvj.bravo7.adapter.AnotherListAdapter;

import java.util.ArrayList;
import java.util.List;

public class InstanceListAdapter extends AnotherListAdapter<PebbleInstanceInfo> {

    public static interface InstanceListListener {
        public void itemSelected(int id);

        public void itemChanged(int id);
    }

    private PebbleController controller;
    private int selected = -1;
    private int dropIndex = -1;
    private List<InstanceListListener> listeners = new ArrayList<InstanceListAdapter.InstanceListListener>();

    public InstanceListAdapter(PebbleController controller) {
        super(controller.getInstances(), R.layout.instance_list_item);
        this.controller = controller;
    }

    @Override
    public void customize(View view, int position) {
        PebbleInstanceInfo<?> instance = getItem(position);
        TextView caption = (TextView) view.findViewById(R.id.instance_list_item_caption);
        caption.setText(instance.plugin.getInstanceTitle(instance));
        view.setBackgroundColor(Color.rgb(0x10, 0x10, 0x10));
        if (selected == instance.id) {
            view.setBackgroundColor(Color.DKGRAY);
        }
        if (position == dropIndex) {
            view.setBackgroundColor(Color.GRAY);
        }
    }

    public void addListener(InstanceListListener listener) {
        if (!listeners.contains(listener)) {
            listeners.add(listener);
            listener.itemSelected(selected);
        }
    }

    public void removeListener(InstanceListListener listener) {
        listeners.remove(listener);
    }

    public void select(int index) {
        if (index == -1) {
            // Drop selection
            selected = -1;
        } else {
            selected = getItem(index).id;
        }
        for (InstanceListListener listener : listeners) {
            listener.itemSelected(selected);
        }
        notifyDataSetChanged();
    }

    public int indexByID(int id) {
        if (-1 == id) {
            return -1;
        }
        for (int i = 0; i < getCount(); i++) {
            if (getItem(i).id == id) {
                return i;
            }
        }
        return -1;
    }

    public int getSelected() {
        return selected;
    }

    public void notifyChanged(int id) {
        for (InstanceListListener listener : listeners) {
            listener.itemChanged(id);
        }
    }

    public int getDropIndex() {
        return dropIndex;
    }

    public void setDropIndex(int dropIndex) {
        this.dropIndex = dropIndex;
        notifyDataSetChanged();
    }

}
