package org.kvj.android.webappbridge.ui.appwidget;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.os.Parcel;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.widget.RemoteViews;

import org.kvj.android.webappbridge.R;
import org.kvj.bravo7.log.Logger;
import org.kvj.bravo7.ng.conf.Configurator;
import org.kvj.bravo7.ng.widget.AppWidget;
import org.kvj.bravo7.ng.widget.AppWidgetController;
import org.kvj.bravo7.ng.widget.AppWidgetRemote;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by vorobyev on 7/27/15.
 */
public class Widget00 extends AppWidget implements AppWidget.AppWidgetUpdate {

    Logger logger = Logger.forInstance(this);

    @Override
    public RemoteViews update(AppWidgetController controller, int id) {
        logger.d("Update widget", id);
        RemoteViews rv = controller.create(R.layout.widget_00);
        Configurator conf = controller.configurator(id);
        String title = conf.settingsString(R.string.conf_widget_title, "");
        String theme = conf.settingsString(R.string.conf_widget_theme, "light");
        if (TextUtils.isEmpty(title)) {
            title = conf.settingsString(R.string.conf_widget_label, "<Untitled>");
        }
        rv.setTextViewText(R.id.widget_00_title, title);
        rv.setTextColor(R.id.widget_00_title, textColorByTheme(theme));
        rv.setTextColor(R.id.widget_00_empty, textColorByTheme(theme));
        rv.setRemoteAdapter(R.id.widget_00_list,
                            controller.remoteIntent(id, Widget00.Service.class));
        rv.setEmptyView(R.id.widget_00_list, R.id.widget_00_empty);
        controller.notify(id, R.id.widget_00_list);
        return rv;
    }

    private static int textColorByTheme(String theme) {
        if ("light".equals(theme)) {
            return Color.BLACK;
        }
        return Color.WHITE;
    }

    @Override
    public AppWidgetUpdate updater() {
        return this;
    }

    public static void updateAll(Context context) {
        AppWidgetController controller = AppWidgetController.instance(context);
        int[] ids = controller.ids(Widget00.class);
        Widget00 instance = new Widget00();
        for (int id : ids) {
            controller.update(id, instance);
        }
    }

    public static boolean loadText(Context context, String title, List<String> text, String name) {
        // Push text to widget by name
        AppWidgetController controller = AppWidgetController.instance(context);
        int[] ids = controller.ids(Widget00.class);
        Widget00 instance = new Widget00();
        for (int id : ids) {
            // Search by name
            Configurator configurator = controller.configurator(id);
            if (configurator.settingsString(R.string.conf_widget_name, "").equals(name)) {
                // Found
                configurator.stringSettings(R.string.conf_widget_mode, "text");
                configurator.stringSettings(R.string.conf_widget_title, title);
                configurator.listSettings(R.string.conf_widget_data, text);
                controller.update(id, instance);
            }
        }
        return false;
    }

    @Override
    public String title(AppWidgetController controller, int id) {
        Configurator conf = controller.configurator(id);
        String title = conf.settingsString(R.string.conf_widget_label, "");
        if (!TextUtils.isEmpty(title)) {
            return title;
        }
        return super.title(controller, id);
    }

    static class Adapter extends AppWidgetRemote.AppWidgetRemoteAdapter {

        List<CharSequence> lines = new ArrayList<>();
        private boolean wordWrap = false;
        private String theme = null;

        @Override
        public void onDataSetChanged() {
            super.onDataSetChanged();
            lines.clear();
            Configurator conf = controller.configurator(id);
            wordWrap = conf.settingsBoolean(R.string.conf_widget_line_wrap, false);
            theme = conf.settingsString(R.string.conf_widget_theme, "light");
            String url = conf.settingsString(R.string.conf_widget_url, "");
            logger.d("Dataset changed:", url);
            if (TextUtils.isEmpty(url)) {
                lines.addAll(conf.settingsList(R.string.conf_widget_data));
                return; // Simple case
            }
            // Parse URL
            List<String> parts = conf.settingsList(R.string.conf_widget_url);
            Cursor c = null;
            try {
                Uri uri = Uri.parse(parts.get(0));
                boolean richText = conf.settingsBoolean(R.string.conf_widget_rich, false);
                String[] proj = richText?
                                new String[]{"id", "text", "colored"}:
                                new String[]{"id", "text"};
                String sel = null;
                if (parts.size() > 0) {
                    sel = parts.get(1);
                }
                String[] args = null;
                if (parts.size() > 1) {
                    args = parts.subList(2, parts.size()).toArray(new String[0]);
                }
//                logger.d("Query ready:", uri, sel, args, id, parts.subList(2, parts.size()));
                c = context.getContentResolver().query(uri, proj, sel, args, null);
//                logger.d("Query done:", uri, sel, args, parts);
                while (c.moveToNext()) {
                    if (richText) {
                        SpannableStringBuilder b = new SpannableStringBuilder();
                        Parcel p = Parcel.obtain();
                        byte[] bb = c.getBlob(2);
                        p.unmarshall(bb, 0, bb.length);
                        p.setDataPosition(0);
                        CharSequence seq = TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(p);
                        lines.add(seq);
                    } else {
                        lines.add(c.getString(1));
                    }
                }
            } catch (Exception e) {
                logger.e(e, "Failed to load data:", parts);
            } finally {
                if (null != c) {
                    c.close();
                }
            }
        }

        @Override
        public int getCount() {
            return lines.size();
        }

        @Override
        public RemoteViews getViewAt(int i) {
            RemoteViews rv = controller.create(R.layout.widget_line);
            rv.setTextViewText(R.id.widget_00_line_text, lines.get(i));
            rv.setBoolean(R.id.widget_00_line_text, "setSingleLine", !wordWrap);
            rv.setTextColor(R.id.widget_00_line_text, textColorByTheme(theme));
            return rv;
        }
    }

    public static class Service extends AppWidgetRemote.AppWidgetRemoteService {

        @Override
        protected AppWidgetRemote.AppWidgetRemoteAdapter adapter() {
            return new Adapter();
        }
    }
}
