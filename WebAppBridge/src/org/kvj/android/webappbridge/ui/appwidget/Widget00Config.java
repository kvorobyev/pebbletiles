package org.kvj.android.webappbridge.ui.appwidget;

import org.kvj.android.webappbridge.R;
import org.kvj.bravo7.ng.widget.AppWidget;
import org.kvj.bravo7.ng.widget.AppWidgetConfigActivity;

/**
 * Created by vorobyev on 7/27/15.
 */
public class Widget00Config extends AppWidgetConfigActivity {

    @Override
    protected int preferenceXML() {
        return R.xml.widget_00_config;
    }

    @Override
    protected AppWidget.AppWidgetUpdate appWidget() {
        return new Widget00();
    }
}
