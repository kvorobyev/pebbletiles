package org.kvj.android.webappbridge.ui;

import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceFragment;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import org.kvj.android.webappbridge.R;
import org.kvj.android.webappbridge.ui.appwidget.Widget00;
import org.kvj.bravo7.ng.widget.AppWidgetController;

public class SettingsFragment extends PreferenceFragment {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.settings);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.settings, menu);
        final AppWidgetController controller = AppWidgetController.instance(getActivity());
        int[] ids = controller.ids(Widget00.class);
        MenuItem item = menu.findItem(R.id.s_menu_widgets);
        item.getSubMenu().clear();
        Widget00 widget = new Widget00();
        for (final int id : ids) {
            MenuItem subItem = item.getSubMenu().add(controller.title(id, widget));
            subItem.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem menuItem) {
                    Intent intent = controller.configIntent(id);
                    startActivity(intent);
                    return true;
                }
            });
        }
    }
}
