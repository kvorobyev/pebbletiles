package org.kvj.android.webappbridge.ui;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import org.kvj.android.webappbridge.pebble.plugin.impl.AlarmPlugin;
import org.kvj.android.webappbridge.pebble.plugin.instance.PebbleInstanceInfo;
import org.kvj.android.webappbridge.svc.WebBridgeController;
import org.kvj.bravo7.ng.App;

import java.util.List;

/**
 * Created by vorobyev on 3/26/15.
 */
public class ScheduleReceiver extends AppCompatActivity {

    private WebBridgeController controller = App.controller();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // First time - show dialog etc
        Intent startIntent = getIntent();
        if (null != startIntent && Intent.ACTION_SEND.equals(startIntent.getAction())) {
            // Share text - send to widget/message
            handleSharing(startIntent.getStringExtra(Intent.EXTRA_TEXT));
        }
    }

    private void handleSharing(final String text) {
        final List<PebbleInstanceInfo<AlarmPlugin.AlarmPluginInfo>>
            instances =
            controller.getPebble().getInstances("alarm", AlarmPlugin.AlarmPluginInfo.class);
        if (0 == instances.size()) {
            finish();
            return;
        }
        final AlarmPlugin plugin = (AlarmPlugin) controller.getPebble().findPlugin("alarm");
        if (1 == instances.size()) {
            // Only one choice
            plugin.setSchedule(instances.get(0), text);
            finish();
            return;
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Select Destination");
        CharSequence titles[] = new CharSequence[instances.size()];
        int index = 0;
        for (PebbleInstanceInfo<AlarmPlugin.AlarmPluginInfo> instance : instances) {
            String instanceTitle = plugin.getInstanceTitle(instance);
            titles[index++] = instanceTitle;
        }
        builder.setSingleChoiceItems(titles, -1, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                plugin.setSchedule(instances.get(i), text);
                dialogInterface.dismiss();
                finish();
            }
        });
        builder.show();
    }
}
