package org.kvj.android.webappbridge.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import org.kvj.android.webappbridge.R;
import org.kvj.android.webappbridge.svc.WebBridgeController;
import org.kvj.android.webappbridge.ui.appwidget.Widget00;
import org.kvj.bravo7.log.Logger;
import org.kvj.bravo7.ng.App;
import org.kvj.bravo7.ng.widget.AppWidgetController;

public class SettingsActivity extends AppCompatActivity {

    private WebBridgeController controller = App.controller();
    Logger logger = Logger.forInstance(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_settings);
        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getFragmentManager().beginTransaction()
                .replace(R.id.settings_pane, new SettingsFragment()).commit();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        logger.i("Intent:", intent);
        super.onNewIntent(intent);
    }
}
