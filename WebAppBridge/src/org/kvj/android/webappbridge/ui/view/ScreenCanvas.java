package org.kvj.android.webappbridge.ui.view;

import android.content.ClipData;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.DragEvent;
import android.view.View;
import android.widget.RelativeLayout;

import org.kvj.android.webappbridge.pebble.plugin.instance.PebbleInstanceInfo;
import org.kvj.android.webappbridge.ui.adapter.InstanceListAdapter;
import org.kvj.android.webappbridge.ui.adapter.InstanceListAdapter.InstanceListListener;
import org.kvj.bravo7.log.Logger;

public class ScreenCanvas extends RelativeLayout implements View.OnClickListener, InstanceListListener, View.OnLongClickListener, View.OnDragListener {
    private static final CharSequence CANVAS_ITEM = "CanvasItem";
    private Logger logger = Logger.forInstance(this);

    @Override
    public boolean onDrag(View v, DragEvent event) {
        final int action = event.getAction();
        if (event.getClipDescription() == null
                || !event.getClipDescription().getLabel().equals(CANVAS_ITEM)) { // Ignore
            return false;
        }
//        logger.d("Drag:", action, event.getClipDescription().getLabel());
        switch (action) {
            case DragEvent.ACTION_DRAG_STARTED:
                return true;
            case DragEvent.ACTION_DROP:
                Intent intent = event.getClipData().getItemAt(0).getIntent();
                int id = intent.getIntExtra("id", -1);
                int index = adapter.indexByID(id);
                if (index == -1) {
                    // Not found
                    return true;
                }
                PebbleInstanceInfo instance = adapter.getItem(index);
                int x = Math.round((event.getX() / zoom - instance.bounds.right  / 2) / PEBBLE_STEP) * PEBBLE_STEP;
                int y = Math.round((event.getY() / zoom - instance.bounds.bottom / 2) / PEBBLE_STEP) * PEBBLE_STEP;
                instance.bounds.left = x;
                instance.bounds.top  = y;
                if (instance.bounds.left < 0) instance.bounds.left = 0;
                if (instance.bounds.top < 0) instance.bounds.top = 0;
                if (instance.bounds.right + instance.bounds.left > PEBBLE_WIDTH)
                    instance.bounds.left = (int)PEBBLE_WIDTH - instance.bounds.right;
                if (instance.bounds.bottom + instance.bounds.top > PEBBLE_HEIGHT)
                    instance.bounds.top = (int)PEBBLE_HEIGHT - instance.bounds.bottom;
                adapter.notifyChanged(id);

        }
        return false;
    }

    public static final float PEBBLE_HEIGHT = 165;
    public static final float PEBBLE_WIDTH = 140;
    public static final int PEBBLE_STEP = 5;
    private static final float WIDTH_GAP = 60;
    private static final float HEIGHT_GAP = 90;
    private static final float MAX_WIDTH = 200;
    private InstanceListAdapter adapter;
    private float zoom = 0;

    public ScreenCanvas(Context context, AttributeSet attrs) {
        super(context, attrs);
        setBackgroundColor(Color.LTGRAY);
        setOnDragListener(this);
    }

    @Override
    protected int getSuggestedMinimumWidth() {
//		Rect winRect = new Rect();
//		getWindowVisibleDisplayFrame(winRect);
//		WebBridgeApp.logger.log(LogLevel.Debug, "Measure", getMeasuredWidth(), winRect);
        return (int) (PEBBLE_WIDTH * zoom);
    }

    @Override
    protected int getSuggestedMinimumHeight() {
        return (int) (PEBBLE_HEIGHT * zoom);
    }

    public void showInstances() {
        if (zoom == 0 || null == adapter) {
            return; // Will be rendered after measure
        }
        removeAllViews();
        logger.d("Render all:", adapter.getCount());
        for (int i = 0; i < adapter.getCount(); i++) {
            renderInstance(adapter.getItem(i)); // Not selected
        }
        requestLayout();
    }

    private void renderInstance(@SuppressWarnings("rawtypes") PebbleInstanceInfo instance) {
        ScreenCanvasItem view = new ScreenCanvasItem(getContext());
        int left = (int) (instance.bounds.left * zoom);
        int top = (int) (instance.bounds.top * zoom);
        int right = (int) ((instance.bounds.right + instance.bounds.left) * zoom);
        int bottom = (int) ((instance.bounds.bottom + instance.bounds.top) * zoom);
        RelativeLayout.LayoutParams params = new LayoutParams(right - left - 1, bottom - top - 1);
        params.leftMargin = left;
        params.topMargin = top;
        // logger.d("Render:", instance.bounds, instance.id, zoom);
        view.setID(instance.id);
        view.setOnClickListener(this);
        view.setOnLongClickListener(this);
        addView(view, params);
    }

    @Override
    public boolean onLongClick(View v) {
        if (null == adapter) {
            return false;
        }
        onClick(v);
        ScreenCanvasItem canvasItem = (ScreenCanvasItem) v;
        Intent intent = new Intent();
        intent.putExtra("id", canvasItem.getID());

        ClipData dragData = ClipData.newIntent(CANVAS_ITEM, intent);
        v.startDrag(dragData, new MyDragShadowBuilder(v), null, 0);
        return true;
    }

    private static class MyDragShadowBuilder extends View.DragShadowBuilder {
        private static Drawable shadow;

        // Defines the constructor for myDragShadowBuilder
        public MyDragShadowBuilder(View v) {

            // Stores the View parameter passed to myDragShadowBuilder.
            super(v);

            // Creates a draggable image that will fill the Canvas provided by the system.
            shadow = new ColorDrawable(ScreenCanvasItem.DRAG_COLOR);
        }

        // Defines a callback that sends the drag shadow dimensions and touch point back to the
        // system.
        @Override
        public void onProvideShadowMetrics (Point size, Point touch) {
        // Defines local variables
        int width, height;

        // Sets the width of the shadow to half the width of the original View
        width = getView().getWidth();

        // Sets the height of the shadow to half the height of the original View
        height = getView().getHeight();

        // The drag shadow is a ColorDrawable. This sets its dimensions to be the same as the
        // Canvas that the system will provide. As a result, the drag shadow will fill the
        // Canvas.
        shadow.setBounds(0, 0, width, height);

        // Sets the size parameter's width and height values. These get back to the system
        // through the size parameter.
        size.set(width, height);

        // Sets the touch point's position to be in the middle of the drag shadow
        touch.set(width / 2, height / 2);
    }

    // Defines a callback that draws the drag shadow in a Canvas that the system constructs
    // from the dimensions passed in onProvideShadowMetrics().
    @Override
    public void onDrawShadow(Canvas canvas) {

        // Draws the ColorDrawable in the Canvas passed in from the system.
        shadow.draw(canvas);
    }

}

    @Override
    public void onClick(View v) {
        ScreenCanvasItem canvasItem = (ScreenCanvasItem) v;
        Integer id = canvasItem.getID();
        if (null != adapter) {
            int index = adapter.indexByID(id);
            if (-1 != index) { // OK
                adapter.select(index);
                logger.d("Click On:", id, adapter.getItem(index).bounds);
            }
        }
    }

    public void setAdapter(InstanceListAdapter adapter) {
        this.adapter = adapter;
        adapter.addListener(this);
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (null != adapter) {
            adapter.removeListener(this);
        }
    }

    private ScreenCanvasItem findByID(int id) {
        for (int i = 0; i < getChildCount(); i++) {
            ScreenCanvasItem child = (ScreenCanvasItem) getChildAt(i);
            int childID = child.getID();
            if (id == childID) {
                return child;
            }
        }
        return null;
    }

    @Override
    public void itemSelected(int id) {
        View frontView = null;
        for (int i = 0; i < getChildCount(); i++) {
            ScreenCanvasItem child = (ScreenCanvasItem) getChildAt(i);
            int childID = child.getID();
            if (id == childID) {
                child.setSelected(true);
                frontView = child;
            } else {
                child.setSelected(false);
            }
        }
        if (null != frontView) {
            bringChildToFront(frontView);
        }
    }

    @Override
    public void itemChanged(int id) {
        View view = findByID(id);
        int index = adapter.indexByID(id);
        if (null == view) {
            return;
        }
        PebbleInstanceInfo instance = adapter.getItem(index);
        RelativeLayout.LayoutParams params = (LayoutParams) view.getLayoutParams();
        params.leftMargin = (int) (instance.bounds.left * zoom);
        params.topMargin = (int) (instance.bounds.top * zoom);
        params.width = (int) (instance.bounds.right * zoom);
        params.height = (int) (instance.bounds.bottom * zoom);
        view.setLayoutParams(params);
    }

    public void move(int x, int y) {
        int index = adapter.indexByID(adapter.getSelected());
        if (index == -1) {
            // Not found
            return;
        }
        PebbleInstanceInfo instance = adapter.getItem(index);
        if (x < 0 && instance.bounds.right >= PEBBLE_STEP * 2) {
            instance.bounds.right -= PEBBLE_STEP;
        }
        if (x > 0 && instance.bounds.left + instance.bounds.right <= PEBBLE_WIDTH - PEBBLE_STEP) {
            instance.bounds.right += PEBBLE_STEP;
        }
        if (y < 0 && instance.bounds.bottom >= PEBBLE_STEP * 2) {
            instance.bounds.bottom -= PEBBLE_STEP;
        }
        if (y > 0 && instance.bounds.top + instance.bounds.bottom <= PEBBLE_HEIGHT - PEBBLE_STEP) {
            instance.bounds.bottom += PEBBLE_STEP;
        }
        adapter.notifyChanged(instance.id);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        if (zoom > 0) {
            this.setMeasuredDimension((int) ((PEBBLE_WIDTH) * zoom), (int) (PEBBLE_HEIGHT * zoom));
            super.onMeasure(widthMeasureSpec, heightMeasureSpec);
            return;
        }
        Resources r = getResources();
        int widthGap = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, WIDTH_GAP, r.getDisplayMetrics());
        int heightGap = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, HEIGHT_GAP, r.getDisplayMetrics());
        int maxWidth = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, MAX_WIDTH, r.getDisplayMetrics());
        int parentWidth = MeasureSpec.getSize(widthMeasureSpec);
        int parentHeight = MeasureSpec.getSize(heightMeasureSpec);
        float zoomX = (parentWidth - widthGap) / PEBBLE_WIDTH;
        float zoomY = (parentHeight - heightGap) / PEBBLE_HEIGHT;
        zoom = Math.min(zoomX, zoomY);
        if (PEBBLE_WIDTH * zoom > maxWidth) {
            // Too big
            zoom = maxWidth / PEBBLE_WIDTH;
        }
        logger.d("Measure:", zoom);

        this.setMeasuredDimension((int) ((PEBBLE_WIDTH) * zoom), (int) (PEBBLE_HEIGHT * zoom));
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        showInstances(); // Render after zoom has been defined
    }

}
