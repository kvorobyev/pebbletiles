package org.kvj.android.webappbridge.ui.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.RectF;
import android.view.View;

public class ScreenCanvasItem extends View {

    private static final float BORDER_SIZE = 1.0f;
    private static final float PADDING_SIZE = 1.0f;
    static int BORDER_COLOR = Color.parseColor("#37474f");
    static int SELECTED_COLOR = Color.parseColor("#ff5722");
    static int DRAG_COLOR = Color.parseColor("#BF360C");
    static int NOT_SELECTED_COLOR = Color.parseColor("#607d8b");
    private Paint paint = new Paint();
    private float density;

    private boolean selected = false;
    private int id;

    public ScreenCanvasItem(Context context) {
        super(context);
        density = getContext().getResources().getDisplayMetrics().density;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        int width = getWidth();
        int height = getHeight();
        super.onDraw(canvas);
        paint.setColor(BORDER_COLOR);
        paint.setStyle(Style.FILL);
        float padding = density * PADDING_SIZE;
        canvas.drawRect(new RectF(padding, padding, width - 2 * padding, height - 2 * padding), paint);
        float boxWidth = width - BORDER_SIZE * density - 2 * padding;
        float boxHeight = height - BORDER_SIZE * density - 2 * padding;
        if (selected) {
            paint.setColor(SELECTED_COLOR);
        } else {
            paint.setColor(NOT_SELECTED_COLOR);
        }
        canvas.drawRect(new RectF(BORDER_SIZE * density + padding, BORDER_SIZE * density + padding, boxWidth, boxHeight), paint);
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
        invalidate();
    }

    public void setID(int id) {
        this.id = id;
    }

    public int getID() {
        return this.id;
    }

}
