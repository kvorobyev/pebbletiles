package org.kvj.android.webappbridge.ui;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceFragment;
import android.support.v7.app.AppCompatActivity;

import org.kvj.android.webappbridge.WebBridgeApp;
import org.kvj.android.webappbridge.pebble.plugin.instance.PebbleInstanceInfo;
import org.kvj.android.webappbridge.svc.WebBridgeController;
import org.kvj.bravo7.ng.App;

abstract public class InstanceConfigActivity extends AppCompatActivity {

    public static class InstanceSettingsFragment extends PreferenceFragment {

        private InstanceConfigActivity instanceConfigActivity;

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            getPreferenceManager().setSharedPreferencesName(instanceConfigActivity.instance.getPreferencesName());
            addPreferencesFromResource(instanceConfigActivity.getSettingsConfig());
            instanceConfigActivity.onPreferencesLoaded(this);
        }

        public void setActivity(InstanceConfigActivity instanceConfigActivity) {
            this.instanceConfigActivity = instanceConfigActivity;
        }
    }

    public static final String INTENT_INSTANCE_ID = "instance_id";

    protected WebBridgeController controller = App.controller();
    protected PebbleInstanceInfo<?> instance;

    protected int instanceID;

    protected void onPreferencesLoaded(InstanceSettingsFragment fragment) {
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        instanceID = getIntent().getIntExtra(INTENT_INSTANCE_ID, -1);
        super.onCreate(savedInstanceState);
        onController();
    }
    @Override
    protected void onStop() {
        super.onStop();
        if (null != instance && null != controller) {
            instance.plugin.configChanged(instance);
        }
    }

    public void onController() {
        // Create config
        instance = controller.getPebble().findInstance(instanceID);
        if (instance == null) {
            // Not found
            WebBridgeApp.toast("Invalid configuration");
            return;
        }
        InstanceSettingsFragment fragment = new InstanceSettingsFragment();
        fragment.setActivity(this);
        getFragmentManager().beginTransaction().replace(android.R.id.content, fragment).commit();
    }

    abstract protected int getSettingsConfig();

    public static <E extends Activity> void startConfig(Context ctx, Class<E> cl,
                                                        PebbleInstanceInfo instance) {
        Intent intent = new Intent(ctx, cl);
        intent.putExtra(INTENT_INSTANCE_ID, instance.id);
        ctx.startActivity(intent);
    }

}

