package org.kvj.android.webappbridge;

import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseInstallation;
import com.parse.ParsePush;
import com.parse.SaveCallback;

import org.kvj.android.webappbridge.svc.WebBridgeController;
import org.kvj.bravo7.SuperActivity;
import org.kvj.bravo7.log.Logger;
import org.kvj.bravo7.ng.App;

public class WebBridgeApp extends App<WebBridgeController> {

    Logger logger = Logger.forInstance(this);

    @Override
    protected WebBridgeController create() {
        return new WebBridgeController(this);
    }

    @Override
    protected void init() {
        logger.i("Application started");
        Parse.initialize(this, getString(R.string.parse_app_id), getString(R.string.parse_client_key));
        ParseInstallation.getCurrentInstallation().saveEventually(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                ParsePush.subscribeInBackground(
                    "c" + ParseInstallation.getCurrentInstallation().getInstallationId(),
                    new SaveCallback() {
                        @Override
                        public void done(ParseException e) {
                            if (e == null) {
                                logger.d("Subscribed to the broadcast channel",
                                         ParseInstallation.getCurrentInstallation()
                                             .getInstallationId());
                            } else {
                                logger.e(e, "failed to subscribe for push", e);
                            }
                        }
                    });
                WebBridgeController controller = controller();
                controller.changeAlias(controller.settings().settingsString(R.string.settings_alias, "").trim().toLowerCase());
            }
        });
    }

    public static void toast(String message) {
        SuperActivity.notifyUser(App.app(), message);
    }

}
