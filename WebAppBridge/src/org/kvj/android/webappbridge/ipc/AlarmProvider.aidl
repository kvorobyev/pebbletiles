package org.kvj.android.webappbridge.ipc;

interface AlarmProvider {
	boolean setAlarm(in long when, in String message, in int alarmType, in long alarmWhen);
}