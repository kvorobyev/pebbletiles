package org.kvj.android.webappbridge.svc;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import org.kvj.bravo7.ng.App;

public class ActionReceiver extends BroadcastReceiver {

    WebBridgeController controller = App.controller();

    @Override
    public void onReceive(Context context, Intent intent) {
        controller.getPebble().handleAction(intent);
    }
}
