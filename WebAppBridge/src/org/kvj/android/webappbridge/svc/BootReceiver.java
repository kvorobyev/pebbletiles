package org.kvj.android.webappbridge.svc;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import org.kvj.android.webappbridge.ui.appwidget.Widget00;

/**
 * Created by vorobyev on 11/26/14.
 */
public class BootReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        context.startService(new Intent(context, BackgroundService.class));
        Widget00.updateAll(context);
    }
}
