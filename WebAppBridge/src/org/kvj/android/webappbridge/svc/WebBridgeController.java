package org.kvj.android.webappbridge.svc;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.service.notification.StatusBarNotification;
import android.text.TextUtils;

import com.parse.ParseException;
import com.parse.ParsePush;
import com.parse.SaveCallback;

import org.kvj.android.webappbridge.R;
import org.kvj.android.webappbridge.pebble.PebbleController;
import org.kvj.android.webappbridge.pebble.plugin.impl.MessagePlugin;
import org.kvj.bravo7.log.Logger;
import org.kvj.bravo7.ng.Controller;

public class WebBridgeController extends Controller {

    private final SharedPreferences preferences;
    private final SharedPreferences.OnSharedPreferenceChangeListener aliasListener = new SharedPreferences.OnSharedPreferenceChangeListener() {
        @Override
        public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
            logger.d("Settings changed:", key);
            if (key.equals(context.getString(R.string.settings_alias))) { // Alias changed
                changeAlias(settings().settingsString(R.string.settings_alias, "").trim().toLowerCase());
            }
        }
    };
    private NotificationCancelListener notificationCancelListener = null;
    private String alias = null;

    public static interface NotificationCancelListener {
        public void cancel(StatusBarNotification notification);
    }

    private Logger logger = Logger.forInstance(this);

    public void setNotificationCancelListener(NotificationCancelListener listener) {
        this.notificationCancelListener = listener;
    }

    public NotificationCancelListener notificationCancelListener() {
        return notificationCancelListener;
    }

    private PebbleController pebble;

    public WebBridgeController(final Context context) {
        super(context, "PebbleTiles:");
        this.preferences = PreferenceManager.getDefaultSharedPreferences(context);
        preferences.registerOnSharedPreferenceChangeListener(aliasListener);
    }

    @Override
    protected void init() {
        super.init();
        pebble = new PebbleController(this);
    }

    public void changeAlias(final String newAlias) {
        logger.d("Change alias:", alias, newAlias);
        if (newAlias.equalsIgnoreCase(alias)) { // Same
            return; // Nothing to do
        }
        if (!TextUtils.isEmpty(alias)) { // Unsubscribe
            ParsePush.unsubscribeInBackground("a" + alias, new SaveCallback() {
                @Override
                public void done(ParseException e) {
                    logger.d("Unsubscribed:", e);
                }
            });
        }
        if (!TextUtils.isEmpty(newAlias)) { // New alias not empty - subscribe
            ParsePush.subscribeInBackground("a" + newAlias, new SaveCallback() {
                @Override
                public void done(ParseException e) {
                    logger.d("Subscribed to alias channel", newAlias, e);
                    if (null == e) { // OK
                        messageShort("Set alias: "+newAlias);
                    }
                }
            });
        }
        alias = newAlias;
    }

    public PebbleController getPebble() {
        return pebble;
    }

    public void processMessage(int vibro, String dest, String name, String title, String message) {
        logger.d("New message:", vibro, dest, name, title);
        if (dest.equals("pebble")) { // Send to pebble
            MessagePlugin messagePlugin = (MessagePlugin) pebble.findPlugin("message");
            logger.d("Will send message:", title, messagePlugin);
            if (null != messagePlugin) {
                messagePlugin.newMessage(vibro, name, title, message);
            }
        }
    }

}
