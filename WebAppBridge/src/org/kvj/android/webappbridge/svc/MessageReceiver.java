package org.kvj.android.webappbridge.svc;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import org.kvj.android.webappbridge.WebBridgeApp;
import org.kvj.bravo7.ng.App;

/**
 * Created by kvorobyev on 12/30/14.
 */
public class MessageReceiver extends BroadcastReceiver {

    WebBridgeController controller = App.controller();

    @Override
    public void onReceive(Context context, Intent intent) {
        int vibro = intent.getIntExtra("vibro", -1);
        String title = intent.getStringExtra("title");
        String message = intent.getStringExtra("message");
        String type = intent.getStringExtra("name");
        String dest = "pebble";
        if (intent.hasExtra("dest")) {
            dest = intent.getStringExtra("dest");
        }
        controller.processMessage(vibro, dest, type, title, message);
    }
}
