package org.kvj.android.webappbridge.svc;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.IBinder;

import org.kvj.android.webappbridge.R;
import org.kvj.android.webappbridge.ui.LayoutConfig;
import org.kvj.android.webappbridge.ui.appwidget.Widget00;

public class BackgroundService extends Service {

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return Service.START_STICKY;
    }

    private BroadcastReceiver screenOn = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Widget00.updateAll(context);
        }
    };

    @Override
    public void onCreate() {
        super.onCreate();
        Notification.Builder fn = new Notification.Builder(this);
        fn
                .setContentTitle(getString(R.string.app_name))
                .setContentText("Application is active")
                .setSmallIcon(R.drawable.ic_stat_normal)
                .setOngoing(true)
                .setOnlyAlertOnce(true)
                .setContentIntent(PendingIntent.getActivity(this, 0, new Intent(this, LayoutConfig.class), PendingIntent.FLAG_UPDATE_CURRENT))
                .setPriority(Notification.PRIORITY_MIN);
        startForeground(1, fn.build());
        registerReceiver(screenOn, new IntentFilter(Intent.ACTION_SCREEN_ON));
    }
}
