#include "ptiles.h"
#include "ptiles_layers.h"
#include "ptiles_digital.h"
#include "ptiles_storage.h"

#define ALARM_STORAGE_V 2

struct ALARM_INFO {
	AppTimer *alarm_timer;
	AppTimer *cdown_timer;
	Layer *layer;
	Layer *flayer;
	char title[41];
	char details[256];
	char when_buffer[6];
	// char prev_when_buffer[6];
	char cdown_buffer[6];
	int vibro;
	uint32_t when;
	// uint32_t prev_when;
	uint32_t alarm;
	uint32_t start_time;
	int line;
};

static void alarm_on_start(PB_INFO_P) {
	struct ALARM_INFO *pinfo = malloc(sizeof(struct ALARM_INFO));
	info->data = pinfo;
	pinfo->cdown_timer = NULL;
	pinfo->alarm_timer = NULL;
	strcpy((char *)&pinfo->title, "");
	strcpy((char *)&pinfo->details, "");
	pinfo->when = 0;
}

static void alarm_on_stop(PB_INFO_P) {
	PINFO(ALARM_INFO)
	TIMER_STOP(pinfo->alarm_timer)
	TIMER_STOP(pinfo->cdown_timer)
	layers_destroy(pinfo->layer);
	layers_destroy(pinfo->flayer);
	free(info->data);
}

static void alarm_render(struct Layer *layer, GContext *ctx) {
	LAYERS_EXTRACT_PB_INFO(layer)
	GRect rect = layer_get_bounds(layer);
	PINFO(ALARM_INFO)
	if (pinfo->when == 0) {
		return; // Nothing to render
	}
	uint32_t tm = (uint32_t)time(NULL);
	uint32_t when_time = pinfo->when;
	strftime((char *)&pinfo->when_buffer, 6, "%H:%M", localtime((time_t *)&when_time));

	int time_height = 12;
	int time_width = time_height * 2.5;
	graphics_context_set_fill_color(ctx, GColorBlack);
	graphics_context_set_text_color(ctx, GColorWhite);
	int main_y = FONTS_TOP_GAP;
	struct PB_Area_Info areaInfo;
	areaInfo.layer = layer;
	areaInfo.ctx = ctx;
	areaInfo.frame = GRect(1, 1, time_width, time_height);
	areaInfo.bold = false;
	digital_render(areaInfo, (char *)&pinfo->when_buffer);
	areaInfo.frame = GRect(rect.size.w - 2 - time_width, 1, time_width, time_height);
	digital_render(areaInfo, (char *)&pinfo->cdown_buffer);
	graphics_draw_text(ctx, (char *)&pinfo->title, fonts_get_by_index(0), GRect(2+time_width, FONTS_TOP_GAP, rect.size.w - 2 * time_width, time_height), GTextOverflowModeTrailingEllipsis, GTextAlignmentLeft, NULL);
	if (strlen(pinfo->details) > 0) {
		if (IN_FULLSCREEN(info)) {
			// Also print details
			int startFrom = 0;
			if (pinfo->line>0) {
				// Skip
				int found = 0;
				for (unsigned int i = 0; i < strlen(pinfo->details); i++) {
					if (pinfo->details[i] == '\n') {
						found++;
						startFrom = i+1;
						if (found == pinfo->line) {
							break;
						}
					}
				}
				pinfo->line = found;
			}
			graphics_draw_text(ctx, (char *)&pinfo->details[startFrom], fonts_get_by_index(0), GRect(2, FONTS_TOP_GAP + time_height + 2, rect.size.w - 4, rect.size.h - time_height-4), GTextOverflowModeTrailingEllipsis, GTextAlignmentLeft, NULL);
		} else {
			// Draw line
			graphics_context_set_stroke_color(ctx, GColorWhite);
			graphics_draw_line(ctx, GPoint(2, rect.size.h - 2), GPoint(rect.size.w - 2, rect.size.h - 2));
		}
	}
}

static void alarm_on_button(PB_INFO_P, ButtonId btn) {
	PINFO(ALARM_INFO)
	if (btn == BUTTON_ID_SELECT) {
		root_exit_selection(info);
	}
	if (btn == BUTTON_ID_DOWN) {
		pinfo->line++;
	}
	if (btn == BUTTON_ID_UP) {
		if (pinfo->line>0) {
			pinfo->line--;
		}
	}
}

static void alarm_on_render(PB_INFO_P) {
	PINFO(ALARM_INFO)
	GRect rect = LAYERS_INSTANCE_RECT(info);
	{
		pinfo->layer = layers_create(rect, info->layer, &alarm_render, PB_INFO_P_SIZE);
		LAYERS_SET_PB_INFO(pinfo->layer)
		layer_add_child(info->layer, pinfo->layer);
	}
	{
		pinfo->flayer = layers_create(FULLSCREEN_RECT, info->flayer, &alarm_render, PB_INFO_P_SIZE);
		LAYERS_SET_PB_INFO(pinfo->flayer)
		layer_add_child(info->flayer, pinfo->flayer);
	}
}

static void alarm_on_alarm(PB_INFO_P) {
	PINFO(ALARM_INFO)
	// TIMER_STOP(pinfo->alarm_timer)
	TIMER_STOP(pinfo->alarm_timer)
	root_vibro(pinfo->vibro);
	if (strlen(pinfo->details) > 0) {
		// Have details - go to fullscreen
		root_request_select(info);
	}
}

static void alarm_on_cdown(PB_INFO_P) {
	PINFO(ALARM_INFO)
	// TIMER_STOP(pinfo->cdown_timer)
	TIMER_STOP(pinfo->cdown_timer)
	if (pinfo->when == 0) {
		return; // Nothing to render
	}
	uint32_t tm = (uint32_t)time(NULL);
	if (pinfo->when > tm) {
		// In the future
		uint32_t left = pinfo->when - tm;
		uint32_t when = root_smart_timer(left);
		pinfo->cdown_timer = app_timer_register(when*1000, (AppTimerCallback)&alarm_on_cdown, info);
		uint32_t sec = left - when; // MM:SS
		bool min_mode = false;
		if (sec > 60*60) {
			min_mode = true;
			// More than 60 min - convert to HH:MM
			sec = sec / 60;
		}
		uint32_t min = sec / 60;
		int sec_part = (int)(sec - min*60);
		if (min>1) {
			if (min_mode) {
				sec_part = (sec_part / 30) * 30;
			} else
				sec_part = 0;
		}
		snprintf((char *)&pinfo->cdown_buffer, 6, "%02d:%02d", (int)min, sec_part);
	} else {
		snprintf((char *)&pinfo->cdown_buffer, 6, "00:00");
	}
	root_request_redraw();
}

static void alarm_start(PB_INFO_P) {
	PINFO(ALARM_INFO)
	uint32_t tm = (uint32_t)time(NULL);
	if (pinfo->when > tm) {
		pinfo->start_time = tm; // Will use it for remaining time calc
		pinfo->alarm_timer = app_timer_register((pinfo->alarm - pinfo->start_time)*1000, (AppTimerCallback)&alarm_on_alarm, info);
	}
}

static void alarm_on_message(PB_INFO_P, DictionaryIterator *it) {
	PINFO(ALARM_INFO)
	FIRST_TUPLE(it, tuple)
	int read_type = tuple->value->int16;
	if (read_type == 0) {
		// New message
		TIMER_STOP(pinfo->alarm_timer)
		TIMER_STOP(pinfo->cdown_timer)
		NEXT_TUPLE(it, tuple)
		strcpy((char *)&pinfo->title, tuple->value->cstring);
		NEXT_TUPLE(it, tuple)
		strcpy((char *)&pinfo->details, tuple->value->cstring);
		NEXT_TUPLE(it, tuple)
		uint32_t tm = (uint32_t)time(NULL);
		pinfo->when = tuple->value->int32+tm;
		NEXT_TUPLE(it, tuple)
		pinfo->alarm = tuple->value->int32+tm;
		NEXT_TUPLE(it, tuple)
		pinfo->vibro = tuple->value->int16;
		pinfo->line = 0;
		alarm_start(info);
		alarm_on_cdown(info);
		root_exit_selection(info);
	} else {
		if (read_type == 2) {
			// Reset
			pinfo->when = 0;
			TIMER_STOP(pinfo->alarm_timer)
			TIMER_STOP(pinfo->cdown_timer)
			root_exit_selection(info);
		} else {
			// Append strings
			NEXT_TUPLE(it, tuple)
			strcat((char *)&pinfo->title, tuple->value->cstring);
			NEXT_TUPLE(it, tuple)
			strcat((char *)&pinfo->details, tuple->value->cstring);
		}
	}
	persist_write_int(storage_key(info, 0), ALARM_STORAGE_V);
	persist_write_string(storage_key(info, 1), (char *)pinfo->title);
	persist_write_string(storage_key(info, 2), (char *)pinfo->details);
	persist_write_int(storage_key(info, 3), pinfo->when);
	persist_write_int(storage_key(info, 4), pinfo->alarm);
	persist_write_int(storage_key(info, 5), pinfo->vibro);
	root_request_redraw();
}

static void alarm_on_restore(PB_INFO_P) {
	PINFO(ALARM_INFO)
	if (persist_read_int(storage_key(info, 0)) != ALARM_STORAGE_V) {
		return;
	}
	persist_read_string(storage_key(info, 1), (char *)pinfo->title, 40);
	persist_read_string(storage_key(info, 2), (char *)pinfo->details, 256);
	pinfo->when = persist_read_int(storage_key(info, 3));
	pinfo->alarm = persist_read_int(storage_key(info, 4));
	pinfo->vibro = persist_read_int(storage_key(info, 5));
	alarm_start(info);
	alarm_on_cdown(info);
	pinfo->line = 0;
}
