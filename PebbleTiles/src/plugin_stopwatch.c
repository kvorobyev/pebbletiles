#include "ptiles.h"
#include "ptiles_digital.h"

#define SW_MODE_PASSIVE 0
#define SW_MODE_ACTIVE 1
#define SW_MODE_PAUSE 2

#define SW_SPLITS 5

struct STOP_INFO {
	AppTimer *timer;
	Layer *layer;
	Layer *flayer;
	int mode; // SW_MODE
	uint32_t begin_tm;
	uint32_t finish_tm;
	uint32_t start; // start time, time() - seconds
	uint32_t seconds; // paused/stopped seconds, time() - start
	uint32_t split[SW_SPLITS]; // seconds value when split is pressed
	uint32_t split_tm[SW_SPLITS]; // time() value when split is pressed
	int splits;
	char texts[SW_SPLITS][10];
	char text[10];
};

static void stop_reset(struct STOP_INFO *pinfo) {
	pinfo->start = 0;
	pinfo->seconds = 0;
	pinfo->splits = 0;
	pinfo->mode = SW_MODE_PASSIVE;
	pinfo->begin_tm = 0;
	pinfo->finish_tm = 0;
}

static void stop_on_start(PB_INFO_P) {
	struct STOP_INFO *pinfo = malloc(sizeof(struct STOP_INFO));
	stop_reset(pinfo);
	info->data = pinfo;
	pinfo->timer = NULL;
}

static void stop_on_stop(PB_INFO_P) {
	PINFO(STOP_INFO)
	STOP_TIMER(pinfo->timer)
	layers_destroy(pinfo->layer);
	layers_destroy(pinfo->flayer);
	free(info->data);
}

static void stop_render_seconds(uint32_t value, char *buffer) {
	strcpy(buffer, "00:00:00");
	uint32_t secs = value;
	// Convert seconds to 00:11:22
	for (int i = 0; i < 3; i++) {
		uint32_t val = secs % 60;
		secs /= 60;
		int padding = 6 - i * 3;
		buffer[padding] = '0'+ (val / 10);
		buffer[padding+1] = '0'+ (val % 10);
	}
}

static void stop_render_time(struct STOP_INFO *pinfo) {
	uint32_t seconds = pinfo->seconds;
	if (pinfo->mode == SW_MODE_ACTIVE) {
		// Dynamic
		time_t tm = time(NULL);
		seconds = (uint32_t) tm - pinfo->start;
	}
	stop_render_seconds(seconds, pinfo->text);
}

#define TIME_HEIGHT_CENTER 40
#define SPLIT_ROWS 5
static void stop_render_fullscreen(struct Layer *layer, GContext *ctx) {
	LAYERS_EXTRACT_PB_INFO(layer)
	GRect rect = LAYERS_INSTANCE_RECT(info);
	PINFO(STOP_INFO)
	stop_render_time(pinfo);
	struct PB_Area_Info areaInfo;
	areaInfo.layer = layer;
	areaInfo.ctx = ctx;
	areaInfo.frame = GRect(0, 1, FULLSCREEN_SIZE, TIME_HEIGHT_CENTER - 3);
	areaInfo.bold = pinfo->mode == SW_MODE_ACTIVE;
	digital_render(areaInfo, (char *)&pinfo->text);
	if (pinfo->mode != SW_MODE_PASSIVE) {
		// Draw line
		graphics_context_set_stroke_color(ctx, GColorWhite);
		graphics_draw_line(ctx, GPoint(2, TIME_HEIGHT_CENTER), GPoint(FULLSCREEN_SIZE - 2, TIME_HEIGHT_CENTER));
	}
	int width = FULLSCREEN_SIZE / 2;
	int height = (FULLSCREEN_SIZE - TIME_HEIGHT_CENTER) / SPLIT_ROWS;
	for (int i = 0; i < pinfo->splits; i++) {
		int col = i / SPLIT_ROWS;
		int row = i % SPLIT_ROWS;
		stop_render_seconds(pinfo->split[i], pinfo->texts[i]);
		areaInfo.frame = GRect(width * col + 1, TIME_HEIGHT_CENTER + row * height + 2, width - 2, height - 3);
		areaInfo.bold = false;
		digital_render(areaInfo, (char *)&pinfo->texts[i]);
	}

}

static void stop_render(struct Layer *layer, GContext *ctx) {
	LAYERS_EXTRACT_PB_INFO(layer)
	GRect rect = LAYERS_INSTANCE_RECT(info);
	PINFO(STOP_INFO)
	stop_render_time(pinfo);
	if (pinfo->mode != SW_MODE_PASSIVE) {
		// Printout
		graphics_context_set_stroke_color(ctx, GColorWhite);
		graphics_draw_line(ctx, GPoint(2, rect.size.h - 2), GPoint(rect.size.w - 2, rect.size.h - 2));
	}
	struct PB_Area_Info areaInfo;
	areaInfo.layer = layer;
	areaInfo.ctx = ctx;
	areaInfo.frame = GRect(1, 1, rect.size.w-2, rect.size.h - 4);
	areaInfo.bold = pinfo->mode == SW_MODE_ACTIVE;
	int width = rect.size.w;
	int height = rect.size.h;
	digital_render(areaInfo, (char *)&pinfo->text);
}

static void stop_show_time(PB_INFO_P) {
	root_request_redraw();
}

static void stop_reschedule(PB_INFO_P);

static void stop_on_timer(PB_INFO_P) {
	PINFO(STOP_INFO)
	stop_show_time(info);
	stop_reschedule(info);
}

static void stop_reschedule(PB_INFO_P) {
	PINFO(STOP_INFO)
	uint32_t msec = 1000;
	STOP_TIMER(pinfo->timer)
	if (pinfo->mode != SW_MODE_ACTIVE) {
		// No timer
		return;
	}
	if (!IN_FULLSCREEN(info)) {
		return;
	}
	pinfo->timer = app_timer_register(msec, (AppTimerCallback)&stop_on_timer, info);
}

static void stop_on_enter(PB_INFO_P) {
	PINFO(STOP_INFO)
	stop_show_time(info);
	stop_reschedule(info);
}

static void stop_send_data(PB_INFO_P) {
	PINFO(STOP_INFO)
}

static void stop_on_button(PB_INFO_P, ButtonId btn) {
	PINFO(STOP_INFO)
	time_t tm = time(NULL);
	if (btn == BUTTON_ID_SELECT) {
		if (pinfo->mode == SW_MODE_PASSIVE) {
			// Start
			pinfo->mode = SW_MODE_ACTIVE;
			pinfo->seconds = 0;
			pinfo->start = (uint32_t) tm;
			pinfo->begin_tm = (uint32_t) tm;
		} else
		if (pinfo->mode == SW_MODE_PAUSE) {
			// Unpause
			pinfo->mode = SW_MODE_ACTIVE;
			pinfo->start = (uint32_t) tm - pinfo->seconds;
		} else
		if (pinfo->mode == SW_MODE_ACTIVE) {
			// Pause
			pinfo->mode = SW_MODE_PAUSE;
			pinfo->seconds = (uint32_t) tm - pinfo->start; // Save seconds
		}
	}
	if (btn == BUTTON_ID_UP) {
		if (pinfo->mode != SW_MODE_PASSIVE) {
			// Not in passive - stop
			if (pinfo->mode == SW_MODE_ACTIVE) {
				pinfo->seconds = (uint32_t) tm - pinfo->start; // Save seconds
			}
			pinfo->mode = SW_MODE_PASSIVE;
			pinfo->finish_tm = (uint32_t) tm;
		} else {
			// Reset
			stop_reset(pinfo);
		}
	}
	if (btn == BUTTON_ID_DOWN) {
		// Make splits
		if (pinfo->mode != SW_MODE_PASSIVE) {
			// Active or paused
			if (pinfo->splits >= SW_SPLITS) {
				// Too many
				return;
			}
			uint32_t seconds = pinfo->seconds;
			if (pinfo->mode == SW_MODE_ACTIVE) {
				// Counting now - get live info
				seconds = (uint32_t) tm - pinfo->start; // Save seconds
			}
			pinfo->split_tm[pinfo->splits] = (uint32_t) tm;
			pinfo->split[pinfo->splits] = seconds;
			pinfo->splits++;
		}
	}
	stop_reschedule(info);
	stop_show_time(info);
}

static void stop_on_render(PB_INFO_P) {
	PINFO(STOP_INFO)
	GRect rect = LAYERS_INSTANCE_RECT(info);
	{
		pinfo->layer = layers_create(rect, info->layer, &stop_render, PB_INFO_P_SIZE);
		LAYERS_SET_PB_INFO(pinfo->layer)
		layer_add_child(info->layer, pinfo->layer);
	}
	{
		pinfo->flayer = layers_create(FULLSCREEN_RECT, info->flayer, &stop_render_fullscreen, PB_INFO_P_SIZE);
		LAYERS_SET_PB_INFO(pinfo->flayer)
		layer_add_child(info->flayer, pinfo->flayer);
	}
	stop_show_time(info);
}
