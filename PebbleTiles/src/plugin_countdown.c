#include "ptiles.h"
#include "ptiles_digital.h"
#include "ptiles_storage.h"

#define CDOWN_TYPE_ONE_SHOT 0
#define CDOWN_TYPE_CYCLIC 1
#define CDOWN_VIBRO_SHORT 0
#define CDOWN_VIBRO_LONG 1
#define CDOWN_VIBRO_DOUBLE 2
#define CDOWN_VIBRO_REPEAT 3

struct CDOWN_INFO {
	int sec;
	int sec_pass;
	int type;
	int font;
	int vibro_type;
	int start_sec;
	AppTimer *timer;
	Layer *layer;
	char buffer[10];
	bool digitalFont;
};

static void cdown_timer(PB_INFO_P);

static void cdown_start_timer(PB_INFO_P) {
	PINFO(CDOWN_INFO)
	time_t tm = time(NULL);
	if (0 == pinfo->sec_pass) {
		// First time
		pinfo->start_sec = (int) tm;
	}
	int sec = pinfo->sec - pinfo->sec_pass;
	int next_timer = root_smart_timer(sec);

	int next_timer_fixed = next_timer + pinfo->sec_pass - (((int)tm) - pinfo->start_sec);
	pinfo->sec_pass += next_timer;
	if (next_timer_fixed <= 0) {
		// No need to start
		pinfo->sec_pass = pinfo->sec;
		cdown_timer(info);
		return;
	}
	pinfo->timer = app_timer_register(next_timer_fixed*1000, (AppTimerCallback)&cdown_timer, info);
}

static void cdown_timer(PB_INFO_P) {
	PINFO(CDOWN_INFO)
	pinfo->timer = NULL;
	if (0 == pinfo->sec) {
		return;
	}
	int sec = pinfo->sec - pinfo->sec_pass;
	if (sec <= 0) {
		// Finished
		pinfo->sec_pass = 0;
		// Vibro
		root_vibro(pinfo->vibro_type);
		if (pinfo->type == CDOWN_TYPE_CYCLIC) {
			cdown_start_timer(info);
		}
		light_enable_interaction();
	} else {
		cdown_start_timer(info);
	}
	root_request_redraw();
}

static void cdown_start(PB_INFO_P) {
	struct CDOWN_INFO *pinfo = malloc(sizeof(struct CDOWN_INFO));
	info->data = pinfo;
	pinfo->sec = 0;
	pinfo->sec_pass = 0;
	pinfo->type = CDOWN_TYPE_ONE_SHOT;
	pinfo->timer = NULL;
	// root_request_instance_data(info);
}

static void cdown_stop_timer(PB_INFO_P) {
	PINFO(CDOWN_INFO)
	if (NULL != pinfo->timer) {
		app_timer_cancel(pinfo->timer);
		pinfo->timer = NULL;
	}
	pinfo->sec_pass = 0;
}

static void cdown_stop(PB_INFO_P) {
	PINFO(CDOWN_INFO)
	if (NULL != pinfo->timer) {
		app_timer_cancel(pinfo->timer);
	}
	layers_destroy(pinfo->layer);
	free(info->data);
}

static void cdown_render(struct Layer *layer, GContext *ctx) {
	LAYERS_EXTRACT_PB_INFO(layer)
	GRect rect = LAYERS_INSTANCE_RECT(info);
	PINFO(CDOWN_INFO)
	graphics_context_set_stroke_color(ctx, GColorWhite);
	int sec = pinfo->sec;
	if (sec == 0) {
		// Not set
		return;
	}
	if (pinfo->sec_pass>0) {
		sec -= pinfo->sec_pass;
		// Printout
		graphics_draw_line(ctx, GPoint(2, rect.size.h - 2), GPoint(rect.size.w - 2, rect.size.h - 2));
	}
	int min = sec / 60;
	sec = sec % 60;
	int hour = min / 60;
	min -= hour * 60;
	if (pinfo->sec > 3600) {
		// Render with hours
		snprintf((char *)&pinfo->buffer, 9, "%02d:%02d:%02d", hour, min, sec);
	} else {
		snprintf((char *)&pinfo->buffer, 6, "%02d:%02d", min, sec);
	}
	if (pinfo->digitalFont) {
		// Digital font
		struct PB_Area_Info areaInfo;
		areaInfo.layer = layer;
		areaInfo.ctx = ctx;
		// areaInfo.frame = GRect(rect.origin.x, rect.origin.y, rect.size.w-2, rect.size.h - 4);
		areaInfo.frame = GRect(1, 1, rect.size.w-2, rect.size.h - 4);
		areaInfo.bold = false;
		digital_render(areaInfo, (char *)&pinfo->buffer);
	} else {
		// Standard font
		graphics_draw_text(ctx, (char *)&pinfo->buffer, fonts_get_by_index(pinfo->font), GRect(0, -3, rect.size.w, rect.size.h), GTextOverflowModeTrailingEllipsis, GTextAlignmentCenter, NULL);
	}
}

static void cdown_button(PB_INFO_P, ButtonId btn) {
	PINFO(CDOWN_INFO)
	if (btn == BUTTON_ID_SELECT) {
		// Toggle timer
		if (NULL != pinfo->timer) {
			cdown_stop_timer(info);
		} else {
			cdown_start_timer(info);
		}
	}
}

static void cdown_on_render(PB_INFO_P) {
	PINFO(CDOWN_INFO)
	GRect rect = LAYERS_INSTANCE_RECT(info);
	pinfo->font = fonts_find_font(rect.size, 5, 1);
	pinfo->layer = layers_create(rect, info->layer, &cdown_render, PB_INFO_P_SIZE);
	LAYERS_SET_PB_INFO(pinfo->layer)
	layer_add_child(info->layer, pinfo->layer);
}

static void cdown_message(PB_INFO_P, DictionaryIterator *it) {
	PINFO(CDOWN_INFO)
	FIRST_TUPLE(it, tuple)
	pinfo->sec = tuple->value->int16;
	NEXT_TUPLE(it, tuple)
	pinfo->type = tuple->value->int16;
	NEXT_TUPLE(it, tuple)
	pinfo->vibro_type = tuple->value->int16;
	NEXT_TUPLE(it, tuple)
	pinfo->digitalFont = tuple->value->int16 != 0;
	cdown_stop_timer(info);
	persist_write_int(storage_key(info, 0), pinfo->sec);
	persist_write_int(storage_key(info, 1), pinfo->type);
	persist_write_int(storage_key(info, 2), pinfo->vibro_type);
	persist_write_bool(storage_key(info, 3), pinfo->digitalFont);
	root_request_redraw();
}

static void cdown_restore(PB_INFO_P) {
	PINFO(CDOWN_INFO)
	pinfo->sec = persist_read_int(storage_key(info, 0));
	pinfo->type = persist_read_int(storage_key(info, 1));
	pinfo->vibro_type = persist_read_int(storage_key(info, 2));
	pinfo->digitalFont = persist_read_bool(storage_key(info, 3));
}
