#include "ptiles_digital.h"

#define D_POS_V_TOP 0
#define D_POS_V_CENTER 1
#define D_POS_V_BOTTOM 2
#define D_POS_H_TOP_LEFT 3
#define D_POS_H_TOP_RIGHT 4
#define D_POS_H_BOTTOM_LEFT 5
#define D_POS_H_BOTTOM_RIGHT 6
#define D_POS_COLON 7

void digital_one_segment(struct PB_Area_Info info, int x, int width, int height, unsigned int segment) {
	int thick = height / (info.bold ? 3: 6);
	if (3*thick > width) {
		// Too thick for width
		thick = height / 6;
	}
	int gap = 1; // Padding
	if (thick < 2) {
		thick = (info.bold ? 2: 1); // Minimum height
		gap = 0;
	}
	int swidth = width - 8 * gap;
	int sheight = height - 5 * gap;
	GRect rect = GRect(0, 0, 0, 0); // Segment size here
	switch(segment) {
		case D_POS_V_TOP:
			rect = GRect(4*gap, 1*gap, swidth, thick);
			break;
		case D_POS_V_BOTTOM:
			rect = GRect(4*gap, 2*height - gap - thick, swidth, thick);
			break;
		case D_POS_V_CENTER:
			rect = GRect(4*gap, sheight + 5*gap - thick/2, swidth, thick);
			break;
		case D_POS_H_TOP_LEFT:
			rect = GRect(1*gap, 4*gap, thick, sheight);
			break;
		case D_POS_H_TOP_RIGHT:
			rect = GRect(width - thick - gap, 4*gap, thick, sheight);
			break;
		case D_POS_H_BOTTOM_LEFT:
			rect = GRect(1*gap, 6*gap + sheight, thick, sheight);
			break;
		case D_POS_H_BOTTOM_RIGHT:
			rect = GRect(width - thick - gap, 6*gap + sheight, thick, sheight);
			break;
		case D_POS_COLON:
			rect = GRect(width / 4 - thick / 2, height / 2, thick, sheight / 2);
			graphics_fill_rect(info.ctx, GRect(
						info.frame.origin.x + x + rect.origin.x,
						info.frame.origin.y + rect.origin.y,
						rect.size.w,
						rect.size.h
						), 0, GCornerNone);
			rect = GRect(width / 4 - thick / 2, height, thick, sheight / 2);
			graphics_fill_rect(info.ctx, GRect(
						info.frame.origin.x + x + rect.origin.x,
						info.frame.origin.y + rect.origin.y+1,
						rect.size.w,
						rect.size.h
						), 0, GCornerNone);
			return;
	}
	graphics_fill_rect(info.ctx, GRect(
				info.frame.origin.x + x + rect.origin.x,
				info.frame.origin.y + rect.origin.y,
				rect.size.w,
				rect.size.h
				), 0, GCornerNone);
}


void digital_render(struct PB_Area_Info info, const char *text) {
	// Calculate width, in half-symbols. Number = 2, other = 1
	int symbols = 0;
	for (unsigned int i = 0; i < strlen(text); i++) {
		char ch = text[i];
		if (ch >= '0' && ch <= '9') {
			symbols += 2;
		} else
		if (ch == '-') {
			symbols += 2;
		} else {
			symbols += 1;
		}
	}
	float height = info.frame.size.h / 2;
	int padding = 0;
	float width = 0;
	if (height * symbols < info.frame.size.w) {
		// Width OK
		width = height;
	} else {
		// Too narrow
		width = info.frame.size.w / symbols;
	}
	padding = (info.frame.size.w - width * symbols) / 2 + 1;
	#define SEGMENT(TYPE) digital_one_segment(info, x, 2*width-1, height, TYPE)
	int x = padding;
	graphics_context_set_fill_color(info.ctx, GColorWhite);
	for (unsigned int i = 0; i < strlen(text); i++) {
		char ch = text[i];
		switch (ch) {
			case '0':
				SEGMENT(D_POS_V_TOP);
				SEGMENT(D_POS_V_BOTTOM);
				SEGMENT(D_POS_H_TOP_LEFT);
				SEGMENT(D_POS_H_TOP_RIGHT);
				SEGMENT(D_POS_H_BOTTOM_LEFT);
				SEGMENT(D_POS_H_BOTTOM_RIGHT);
				x += 2 * width;
				break;
			case '1':
				SEGMENT(D_POS_H_TOP_RIGHT);
				SEGMENT(D_POS_H_BOTTOM_RIGHT);
				x += 2 * width;
				break;
			case '2':
				SEGMENT(D_POS_V_TOP);
				SEGMENT(D_POS_V_CENTER);
				SEGMENT(D_POS_V_BOTTOM);
				SEGMENT(D_POS_H_TOP_RIGHT);
				SEGMENT(D_POS_H_BOTTOM_LEFT);
				x += 2 * width;
				break;
			case '3':
				SEGMENT(D_POS_V_TOP);
				SEGMENT(D_POS_V_CENTER);
				SEGMENT(D_POS_V_BOTTOM);
				SEGMENT(D_POS_H_TOP_RIGHT);
				SEGMENT(D_POS_H_BOTTOM_RIGHT);
				x += 2 * width;
				break;
			case '4':
				SEGMENT(D_POS_H_TOP_RIGHT);
				SEGMENT(D_POS_H_BOTTOM_RIGHT);
				SEGMENT(D_POS_H_TOP_LEFT);
				SEGMENT(D_POS_V_CENTER);
				x += 2 * width;
				break;
			case '5':
				SEGMENT(D_POS_V_TOP);
				SEGMENT(D_POS_V_CENTER);
				SEGMENT(D_POS_V_BOTTOM);
				SEGMENT(D_POS_H_TOP_LEFT);
				SEGMENT(D_POS_H_BOTTOM_RIGHT);
				x += 2 * width;
				break;
			case '6':
				SEGMENT(D_POS_V_TOP);
				SEGMENT(D_POS_V_CENTER);
				SEGMENT(D_POS_V_BOTTOM);
				SEGMENT(D_POS_H_TOP_LEFT);
				SEGMENT(D_POS_H_BOTTOM_LEFT);
				SEGMENT(D_POS_H_BOTTOM_RIGHT);
				x += 2 * width;
				break;
			case '7':
				SEGMENT(D_POS_V_TOP);
				SEGMENT(D_POS_H_TOP_LEFT);
				SEGMENT(D_POS_H_TOP_RIGHT);
				SEGMENT(D_POS_H_BOTTOM_RIGHT);
				x += 2 * width;
				break;
			case '8':
				SEGMENT(D_POS_V_TOP);
				SEGMENT(D_POS_V_CENTER);
				SEGMENT(D_POS_V_BOTTOM);
				SEGMENT(D_POS_H_TOP_LEFT);
				SEGMENT(D_POS_H_TOP_RIGHT);
				SEGMENT(D_POS_H_BOTTOM_LEFT);
				SEGMENT(D_POS_H_BOTTOM_RIGHT);
				x += 2 * width;
				break;
			case '9':
				SEGMENT(D_POS_V_TOP);
				SEGMENT(D_POS_V_CENTER);
				SEGMENT(D_POS_V_BOTTOM);
				SEGMENT(D_POS_H_TOP_LEFT);
				SEGMENT(D_POS_H_TOP_RIGHT);
				SEGMENT(D_POS_H_BOTTOM_RIGHT);
				x += 2 * width;
				break;
			case '-':
				SEGMENT(D_POS_V_CENTER);
				x += 2 * width;
				break;
			case ' ':
				x += width;
				break;
			case ':':
				SEGMENT(D_POS_COLON);
				x += width;
				break;
		}
	}


}
