#include "ptiles.h"
#include "ptiles_layers.h"

#define MAX_DEVICES 5

#define CH_FULL_WIDTH 5
// 1, charge ind 1 border 10 border top border 3
#define CH_FULL_HEIGHT 20
#define CH_FULL_TOP_HEIGHT 2
#define CH_TEXT_HEIGHT 14
#define CH_TEXT_WIDTH 24
#define CH_CHARGE_HEIGHT 2

struct CHARGE_INFO {
	uint8_t percent[MAX_DEVICES];
	bool is_charging[MAX_DEVICES];
	bool is_plugged[MAX_DEVICES];
	int devices;
	Layer *layer;
};

static void charge_on_start(PB_INFO_P) {
	struct CHARGE_INFO *pinfo = malloc(sizeof(struct CHARGE_INFO));
	info->data = pinfo;
	pinfo->devices = 1;
	pinfo->percent[0] = 0;
}

static void charge_on_stop(PB_INFO_P) {
	PINFO(CHARGE_INFO)
	layers_destroy(pinfo->layer);
	free(info->data);
}

static void charge_render(struct Layer *layer, GContext *ctx) {
	LAYERS_EXTRACT_PB_INFO(layer)
	PINFO(CHARGE_INFO)
	GRect rect = LAYERS_INSTANCE_RECT(info);
	int width = rect.size.w / pinfo->devices;
	bool full_width = false;
	int b_height = rect.size.h - 2 - CH_CHARGE_HEIGHT;
	if (width >= CH_FULL_WIDTH) {
		full_width = true;
		b_height -= CH_FULL_TOP_HEIGHT;
	}
	bool have_text = false;
	if (rect.size.h > CH_FULL_HEIGHT+CH_TEXT_HEIGHT && width >= CH_TEXT_WIDTH) {
		// Have space
		have_text = true;
		b_height -= CH_TEXT_HEIGHT;
	}
	int x = 0;
	graphics_context_set_stroke_color(ctx, GColorWhite);
	graphics_context_set_fill_color(ctx, GColorWhite);
	for (int i = 0; i < pinfo->devices; i++) {
		// Draw battery
		// graphics_context_set_fill_color(ctx, GColorBlack);
		int y = 1;
		if (full_width) {
			graphics_draw_rect(ctx, GRect(x+2, y, width - 4, CH_FULL_TOP_HEIGHT));
			y += CH_FULL_TOP_HEIGHT;
		}
		// graphics_fill_rect(ctx, GRect(x+1, y, width - 2, b_height), 0, GCornerNone);
		if (full_width) {
			graphics_draw_rect(ctx, GRect(x+1, y, width - 2, b_height));
		}
		int c_height = b_height - b_height * pinfo->percent[i] / 100;
		graphics_fill_rect(ctx, GRect(x+1, y+c_height, width - 2, b_height-c_height), 0, GCornerNone);
		if (pinfo->is_charging[i]) {
			graphics_draw_line(ctx, GPoint(x+1, y+b_height+CH_CHARGE_HEIGHT), GPoint(x+width-2, y+b_height+CH_CHARGE_HEIGHT));
		}

		if (have_text) {
			// Print percents
			char buffer[4];
			snprintf((char *)&buffer, 4, "%d%%", pinfo->percent[i]);
			graphics_draw_text(ctx, (char *)&buffer, fonts_get_by_index(0), GRect(x+1, y+b_height, width-2, CH_TEXT_HEIGHT), GTextOverflowModeTrailingEllipsis, GTextAlignmentCenter, NULL);
		}
		x += width;
	}
}

static void charge_on_message(PB_INFO_P, DictionaryIterator *it) {
	PINFO(CHARGE_INFO)
	FIRST_TUPLE(it, tuple)
	int deviceNo = tuple->value->int16;
	NEXT_TUPLE(it, tuple)
	int percent = tuple->value->int16;
	NEXT_TUPLE(it, tuple)
	bool is_charging = tuple->value->int16 != 0;
	NEXT_TUPLE(it, tuple)
	int is_plugged = tuple->value->int16 != 0;
	if (deviceNo>=pinfo->devices) {
		pinfo->devices = deviceNo+1;
	}
	pinfo->percent[deviceNo] = percent;
	pinfo->is_charging[deviceNo] = is_charging;
	pinfo->is_plugged[deviceNo] = is_plugged;
	root_request_redraw();
}

static void charge_on_render(PB_INFO_P) {
	PINFO(CHARGE_INFO)
	pinfo->layer = layers_create(LAYERS_INSTANCE_RECT(info), info->layer, &charge_render, PB_INFO_P_SIZE);
	LAYERS_SET_PB_INFO(pinfo->layer)
	root_request_battery_state(info);
}

static void charge_on_battery(PB_INFO_P, uint8_t percent, bool is_charging, bool is_plugged) {
	PINFO(CHARGE_INFO)
	pinfo->percent[0] = percent;
	pinfo->is_charging[0] = is_charging;
	pinfo->is_plugged[0] = is_plugged;
	if (NULL != pinfo->layer) {
		// Refresh layer
		root_request_redraw();
	}
	if (info->offline) {
		return;
	}
	Tuplet data[] = {
		root_request_instance_data(info),
		TupletInteger(KEY_ID + 1, percent),
		TupletInteger(KEY_ID + 2, is_charging? 1: 0),
	};
	root_send_message(KEY_CMD_INSTANCE_REQUEST_CMD, data, 3);
}
