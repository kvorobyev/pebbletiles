#include "ptiles.h"
#include "ptiles_digital.h"
#include "ptiles_storage.h"

#define SEC_WIDTH 22
#define MIN_SECONDS_INTERVAL 300

struct TIME_INFO {
	bool seconds;
	bool digitalFont;
	int vibro_type;
	int interval;
	AppTimer *timer;
	AppTimer *vibro_timer;
	Layer *layer;
	char timeBuffer[9];
	int silent_start;
	int silent_finish;
	int minutes_shift;
};

static void time_on_start(PB_INFO_P) {
	struct TIME_INFO *pinfo = malloc(sizeof(struct TIME_INFO));
	info->data = pinfo;
	pinfo->seconds = false;
	pinfo->timer = NULL;
	pinfo->vibro_timer = NULL;
	pinfo->interval = 0;
	pinfo->silent_start = 0;
	pinfo->silent_finish = 0;
	pinfo->minutes_shift = 0;
}

static void time_on_stop(PB_INFO_P) {
	PINFO(TIME_INFO)
	if (NULL != pinfo->timer) {
		app_timer_cancel(pinfo->timer);
	}
	if (NULL != pinfo->vibro_timer) {
		app_timer_cancel(pinfo->vibro_timer);
	}
	layers_destroy(pinfo->layer);
	free(info->data);
}

static int seconds_now() {
	// Return number of seconds from 00:00:00
	char patterns[3][3] = {"%H", "%M", "%S"};
	char buf[4];
	int result = 0;
	time_t tm = time(NULL);
	for (int i = 0; i < 3; i++) {
		strftime((char *)&buf, 3, patterns[i], localtime(&tm));
		result = result * 60 + atoi((char *)&buf);
	}
	return result;
}

static void time_render(struct Layer *layer, GContext *ctx) {
	LAYERS_EXTRACT_PB_INFO(layer)
	GRect rect = LAYERS_INSTANCE_RECT(info);
	PINFO(TIME_INFO)
	struct PB_Area_Info areaInfo;
	areaInfo.layer = layer;
	areaInfo.ctx = ctx;
	areaInfo.frame = GRect(1, 1, rect.size.w-2, rect.size.h - 4);
	areaInfo.bold = true;
	if (rect.size.h <= 20) {
		// Too small
		areaInfo.bold = false;
	}
	int width = rect.size.w;
	int height = rect.size.h;
	time_t tm = time(NULL);
	if (pinfo->minutes_shift != 0) {
		tm = (time_t) ((long)tm+(pinfo->minutes_shift*60));
	}
	if (!pinfo->seconds) {
		tm = (time_t) ((long)tm+30);
	}
	strftime((char *)&pinfo->timeBuffer, 9, pinfo->seconds? "%H:%M:%S": "%H:%M", localtime(&tm));
	if (pinfo->digitalFont) {
		digital_render(areaInfo, (char *)&pinfo->timeBuffer);
		return;
	}
	int font_index = fonts_find_font(rect.size, pinfo->seconds? 7: 5, 1);
	int time_height = fonts_height_by_index(font_index);

	graphics_draw_text(ctx, (char *)&pinfo->timeBuffer, fonts_get_by_index(font_index), GRect(0, -3, rect.size.w, fonts_height_by_index(font_index)+2), GTextOverflowModeTrailingEllipsis, GTextAlignmentCenter, NULL);
}

static void time_show_time(PB_INFO_P) {
	PINFO(TIME_INFO)
	root_request_redraw();
}

static void time_reschedule(PB_INFO_P);

static void time_schedule_vibro(PB_INFO_P);

static void time_on_timer(PB_INFO_P) {
	PINFO(TIME_INFO)
	pinfo->timer = NULL;
	time_reschedule(info);
	time_show_time(info);
}

static void time_on_vibro_timer(PB_INFO_P) {
	PINFO(TIME_INFO)
	pinfo->vibro_timer = NULL;
	time_schedule_vibro(info);
	time_show_time(info);
	// Check if current time is not within silent_start, silent_finish
	int now_seconds = seconds_now();
	if ((pinfo->silent_start > 0) && (now_seconds > 60 * pinfo->silent_start)) {
		// Too late
		return;
	}
	if ((pinfo->silent_finish > 0) && (now_seconds < 60 * pinfo->silent_finish)) {
		// Too early
		return;
	}
	if (-1 != pinfo->vibro_type) {
		root_vibro(pinfo->vibro_type);
	}
}

static void time_schedule_vibro(PB_INFO_P) {
	// If interval > 0 then calculate next vibro moment and schedule it
	PINFO(TIME_INFO)
	STOP_TIMER(pinfo->vibro_timer)
	if (pinfo->interval > 0) {
		// Have interval - calculate next moment
		int now_seconds = seconds_now();
		int add_seconds = pinfo->interval * 60 - (now_seconds % (pinfo->interval * 60));
		if (add_seconds < MIN_SECONDS_INTERVAL) { // Too close to now time
			add_seconds += pinfo->interval * 60; // Next turn
		}
		// Mod from interval in seconds
		pinfo->vibro_timer = app_timer_register(add_seconds * 1000, (AppTimerCallback)&time_on_vibro_timer, info);
	}
}

static void time_reschedule(PB_INFO_P) {
	PINFO(TIME_INFO)
	uint32_t msec = pinfo->seconds? 1000: 1000*30;
	if (!pinfo->seconds) {
		return; // Use system timer
	}
	if (NULL != pinfo->timer) {
		// Have timer
		app_timer_reschedule(pinfo->timer, msec);
	} else {
		// New timer
		pinfo->timer = app_timer_register(msec, (AppTimerCallback)&time_on_timer, info);
	}
	// time_on_timer(info);
}

static void time_on_enter(PB_INFO_P) {
	PINFO(TIME_INFO)
	time_reschedule(info);
}

static void time_on_button(PB_INFO_P, ButtonId btn) {
	PINFO(TIME_INFO)
	if (btn == BUTTON_ID_SELECT) {
		// Toggle msec
		pinfo->seconds = !pinfo->seconds;
		time_reschedule(info);
		time_show_time(info);
	}
}

static void time_on_render(PB_INFO_P) {
	PINFO(TIME_INFO)
	GRect rect = LAYERS_INSTANCE_RECT(info);
	{
		pinfo->layer = layers_create(rect, info->layer, &time_render, PB_INFO_P_SIZE);
		LAYERS_SET_PB_INFO(pinfo->layer)
		layer_add_child(info->layer, pinfo->layer);
	}
	time_reschedule(info);
	time_show_time(info);
}

static void time_on_message(PB_INFO_P, DictionaryIterator *it) {
	PINFO(TIME_INFO)
	FIRST_TUPLE(it, tuple)
	pinfo->vibro_type = tuple->value->int16;
	NEXT_TUPLE(it, tuple)
	pinfo->interval = tuple->value->int16;
	NEXT_TUPLE(it, tuple)
	pinfo->digitalFont = tuple->value->int16 > 0;
	NEXT_TUPLE(it, tuple)
	pinfo->silent_start = tuple->value->int16;
	NEXT_TUPLE(it, tuple)
	pinfo->silent_finish = tuple->value->int16;
	NEXT_TUPLE(it, tuple)
	pinfo->minutes_shift = tuple->value->int32;
	persist_write_bool(storage_key(info, 0), pinfo->digitalFont);
	persist_write_int(storage_key(info, 1), pinfo->vibro_type);
	persist_write_int(storage_key(info, 2), pinfo->interval);
	persist_write_int(storage_key(info, 3), pinfo->silent_start);
	persist_write_int(storage_key(info, 4), pinfo->silent_finish);
	persist_write_int(storage_key(info, 5), pinfo->minutes_shift);
	time_schedule_vibro(info);
	root_request_redraw();
}

static void time_on_restore(PB_INFO_P) {
	PINFO(TIME_INFO)
	pinfo->digitalFont = persist_read_bool(storage_key(info, 0));
	pinfo->vibro_type = persist_read_int(storage_key(info, 1));
	pinfo->interval = persist_read_int(storage_key(info, 2));
	pinfo->silent_start = persist_read_int(storage_key(info, 3));
	pinfo->silent_finish = persist_read_int(storage_key(info, 4));
	pinfo->minutes_shift = persist_read_int(storage_key(info, 5));
	time_schedule_vibro(info);
}
