#include "ptiles_layers.h"

void read_grect(GRect *frame, DictionaryIterator *iterator, Tuple *t) {
	// Read 4 numbers from message and fills frame
	Tuple *tuple = t;
	tuple = root_next_tuple(iterator, tuple, 0);
	CHECK_TUPLE
	frame->origin.x = tuple->value->int16;
	tuple = root_next_tuple(iterator, tuple, 0);
	CHECK_TUPLE
	frame->origin.y = tuple->value->int16;
	tuple = root_next_tuple(iterator, tuple, 0);
	CHECK_TUPLE
	frame->size.w = tuple->value->int16;
	tuple = root_next_tuple(iterator, tuple, 0);
	CHECK_TUPLE
	frame->size.h = tuple->value->int16;
}

Layer* layers_create(GRect frame, Layer *parent, LayerUpdateProc update_proc, size_t sz) {
	Layer* layer = NULL;
	if (sz>0) {
		// With size
		layer = layer_create_with_data(frame, sz);
	} else {
		// No size
		layer = layer_create(frame);
	}
	layer_set_update_proc(layer, update_proc);
	layer_add_child(parent, layer);
	return layer;
}

void layers_black_box(struct Layer *layer, GContext *ctx) {
	// Draw blackbox
	graphics_context_set_fill_color(ctx, GColorBlack);
	graphics_fill_rect(ctx, layer_get_bounds(layer), 0, GCornerNone);
}

void layers_white_stroke_box(struct Layer *layer, GContext *ctx) {
	// Draw blackbox
	graphics_context_set_stroke_color(ctx, GColorWhite);
	graphics_draw_rect(ctx, layer_get_bounds(layer));
}

#define PB_Fonts_Num 2

// 144 x 168

static struct PB_Font_Info PB_Fonts[PB_Fonts_Num] = {{
	.height = 14,
	.width_x_10 = 70,
	.name = FONT_KEY_GOTHIC_14
}, {
	.height = 14,
	.width_x_10 = 75,
	.name = FONT_KEY_GOTHIC_14_BOLD
}};

#define FONTS_HEIGHT_MUL 1.2

int fonts_find_font(GSize size, int chars, int lines) {
	int index = 0; // Smallest
	for (int i = 0; i < PB_Fonts_Num; i++) {
		struct PB_Font_Info pinfo = PB_Fonts[i];
		if (pinfo.height*lines*FONTS_HEIGHT_MUL<(size.h+1*LAYERS_GAP) && pinfo.width_x_10*chars/10<(size.w)) {
			// Fits into size
			index = i;
		}
	}
	WHEN_DEBUG APP_LOG(APP_LOG_LEVEL_DEBUG, "Selected: %d %s %d %d", index, PB_Fonts[index].name, size.h, (int)(PB_Fonts[index].height*FONTS_HEIGHT_MUL));
	return index;
}

GFont fonts_get_by_index(int index) {
	return fonts_get_system_font(PB_Fonts[index].name);
}

int fonts_height_by_index(int index) {
	return PB_Fonts[index].height*FONTS_HEIGHT_MUL;
}

int fonts_width_by_index(int index, int chars) {
	return PB_Fonts[index].width_x_10*chars/10;
}

void init_text_layer(TextLayer *layer, struct PB_Font_Info *info) {
	text_layer_set_background_color(layer, GColorClear);
	text_layer_set_text_color(layer, GColorWhite);
	text_layer_set_font(layer, fonts_get_system_font(info->name));
	text_layer_set_overflow_mode(layer, GTextOverflowModeFill);
	text_layer_set_text_alignment(layer, GTextAlignmentCenter);
}

int fonts_center_layer_y(int height, int lines, struct PB_Font_Info *info) {
	return (height - info->height*lines*1.3) / 2;
}

void layers_destroy(Layer *layer) {
	if (NULL != layer) {
		layer_destroy(layer);
	}
}
