#ifndef _PTILES_H_
#define _PTILES_H_

#include <pebble.h>
#include "ptiles_layers.h"

#define PB_INFO struct PB_Instance_Info

#define PB_INFO_P struct PB_Instance_Info *info
#define PINFO(T) struct T *pinfo = (struct T *) info->data;

#define PB_INFO_P_SIZE sizeof(PB_INFO*)

#define LAYERS_SET_PB_INFO(layer) PB_INFO **ref = layer_get_data(layer); *ref = info;

#define LAYERS_EXTRACT_PB_INFO(layer) PB_INFO **ref = layer_get_data(layer); PB_INFO_P = *ref;

#define TIMER_STOP(timer) if (NULL != timer) {app_timer_cancel(timer); timer = NULL;}

#define LAYER_REFRESH(layer) if (NULL != layer) {layer_mark_dirty(layer);}

#define LAYER(info) ((info->plugin->can_fullscreen) && (info->state == STATE_ACTIVE) ? info->flayer: info->layer)

#define FULLSCREEN_RECT GRect(1, 1, FULLSCREEN_SIZE, FULLSCREEN_SIZE)

#define IN_FULLSCREEN(info) (info->state == STATE_ACTIVE)

#define STOP_TIMER(timer) if (timer != NULL) {app_timer_cancel(timer); timer = NULL;}

#define PB_INSTANCE_MAX 99

#define FULLSCREEN_SIZE 140

#define KEY_CMD 0
#define KEY_ID 1
#define KEY_TYPE 2
#define KEY_CMD_RESET 0
#define KEY_CMD_INSTANCE 1
#define KEY_CMD_REQUEST_RESET 2
#define KEY_CMD_INSTANCE_CMD 3
#define KEY_CMD_INSTANCE_REQUEST_CMD 4

#define STATE_NOT_SELECTED 0
#define STATE_SELECTED 1
#define STATE_ACTIVE 2

#define SELECTION_HIDDEN 0
#define SELECTION_VISIBLE 1
#define SELECTION_SELECTED 2

#define CHECK_TUPLE if (NULL == tuple){ WHEN_DEBUG APP_LOG(APP_LOG_LEVEL_WARNING, "Tuple is NULL"); return;}

#define FIRST_TUPLE(it, tuple) Tuple *tuple = root_next_tuple(it, NULL, 2); CHECK_TUPLE

#define NEXT_TUPLE(it, tuple) tuple = root_next_tuple(it, tuple, 0); CHECK_TUPLE
#define WITH_TUPLE(it, tuple) tuple = tuple != NULL ? root_next_tuple(it, tuple, 0): NULL; if (NULL != tuple)

#define PT_DEBUG 0
#define WHEN_DEBUG if (PT_DEBUG == 1)
static Window *window;
static Layer  *black_box;
static Layer  *window_layer;
static Layer  *fullscreen_box;
static Layer  *fullscreen_info;
static int selected_instance = -1;
static int selection_state = SELECTION_HIDDEN;

struct PB_Plugin_Info;
struct PB_Instance_Info;

struct PB_Instance_Info {
	struct PB_Plugin_Info *plugin;
	GRect bounds;
	uint32_t id;
	Layer *layer;
	Layer *flayer;
	int state;
	void *data;
	bool offline;
};

struct PB_Plugin_Info {
	char *tag;
	bool can_fullscreen;
	unsigned int storage_cells;
	void (* on_start)(PB_INFO_P);
	void (* on_restore)(PB_INFO_P);
	void (* on_stop)(PB_INFO_P);
	void (* on_render)(PB_INFO_P);
	void (* on_enter)(PB_INFO_P);
	void (* on_button)(PB_INFO_P, ButtonId btn);
	void (* on_battery_state)(PB_INFO_P, uint8_t percent, bool is_charging, bool is_plugged);
	void (* on_connection)(PB_INFO_P, bool is_connected);
	void (* on_message)(PB_INFO_P, DictionaryIterator *iterator);
};

static struct PB_Instance_Info *PB_Instances[PB_INSTANCE_MAX];
static unsigned int PB_Instance_num = 0;


void root_request_battery_state(PB_INFO_P);
Tuplet root_request_instance_data(PB_INFO_P);
bool root_send_message(int type, Tuplet data[], int size);
int root_smart_timer(int sec);
void root_vibro(int type);

bool root_request_select(PB_INFO_P);
bool root_exit_selection(PB_INFO_P);
bool root_instance_cmd(PB_INFO_P, Tuplet data[], int size);

Tuple* root_next_tuple(DictionaryIterator *iterator, Tuple *tuple, uint32_t id);
bool root_request_focus(PB_INFO_P);
void root_request_redraw();
#endif
