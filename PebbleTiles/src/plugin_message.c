#include "ptiles.h"
#include "ptiles_layers.h"
#include "ptiles_storage.h"

struct MESSAGE_INFO {
	Layer *layer;
	Layer *flayer;
	bool has_data;
	char title[41];
	char message[256];
	int vibro;
	int line;
	int font;
};

static void message_on_start(PB_INFO_P) {
	struct MESSAGE_INFO *pinfo = malloc(sizeof(struct MESSAGE_INFO));
	info->data = pinfo;
	pinfo->has_data = false;
	pinfo->font = 0;
}

static void message_on_stop(PB_INFO_P) {
	PINFO(MESSAGE_INFO)
	layers_destroy(pinfo->layer);
	layers_destroy(pinfo->flayer);
	free(info->data);
}

static void message_render(struct Layer *layer, GContext *ctx) {
	LAYERS_EXTRACT_PB_INFO(layer)
	GRect rect = layer_get_bounds(layer);
	PINFO(MESSAGE_INFO)
	if (!pinfo->has_data) {
		return;
	}
	if (!IN_FULLSCREEN(info)) {
		graphics_context_set_text_color(ctx, GColorWhite);
		int title_height = fonts_height_by_index(pinfo->font);
		graphics_draw_text(ctx, (char *)&pinfo->title, fonts_get_by_index(pinfo->font), GRect(2, FONTS_TOP_GAP, rect.size.w-4, title_height), GTextOverflowModeTrailingEllipsis, GTextAlignmentLeft, NULL);
	} else {
		graphics_context_set_fill_color(ctx, GColorWhite);
		graphics_context_set_text_color(ctx, GColorBlack);
		graphics_fill_rect(ctx, GRect(1, 1, rect.size.w-2, rect.size.h-2), 0, GCornerNone);
		int title_height = fonts_height_by_index(1);
		graphics_draw_text(ctx, (char *)&pinfo->title, fonts_get_by_index(1), GRect(2, FONTS_TOP_GAP, rect.size.w-4, title_height), GTextOverflowModeTrailingEllipsis, GTextAlignmentLeft, NULL);
		int startFrom = 0;
		if (pinfo->line>0) {
			// Skip
			int found = 0;
			for (unsigned int i = 0; i < strlen(pinfo->message); i++) {
				if (pinfo->message[i] == '\n') {
					found++;
					startFrom = i+1;
					if (found == pinfo->line) {
						break;
					}
				}
			}
			pinfo->line = found;
		}
		graphics_draw_text(ctx, (char *)&pinfo->message[startFrom], fonts_get_by_index(0), GRect(2, FONTS_TOP_GAP+title_height, rect.size.w-4, rect.size.h - title_height), GTextOverflowModeTrailingEllipsis, GTextAlignmentLeft, NULL);
	}
}

static void message_on_button(PB_INFO_P, ButtonId btn) {
	PINFO(MESSAGE_INFO)
	if (btn == BUTTON_ID_SELECT) {
		root_exit_selection(info);
	}
	if (btn == BUTTON_ID_DOWN) {
		pinfo->line++;
	}
	if (btn == BUTTON_ID_UP) {
		if (pinfo->line>0) {
			pinfo->line--;
		}
	}
}

static void message_on_render(PB_INFO_P) {
	PINFO(MESSAGE_INFO)
	GRect rect = LAYERS_INSTANCE_RECT(info);
	{
		pinfo->layer = layers_create(rect, info->layer, &message_render, PB_INFO_P_SIZE);
		LAYERS_SET_PB_INFO(pinfo->layer)
		layer_add_child(info->layer, pinfo->layer);
	}
	{
		pinfo->flayer = layers_create(FULLSCREEN_RECT, info->flayer, &message_render, PB_INFO_P_SIZE);
		LAYERS_SET_PB_INFO(pinfo->flayer)
		layer_add_child(info->flayer, pinfo->flayer);
	}
}

static void message_on_message(PB_INFO_P, DictionaryIterator *it) {
	PINFO(MESSAGE_INFO)
	FIRST_TUPLE(it, tuple)
	int read_type = tuple->value->int16;
	if (read_type == 0) {
		// First part
		NEXT_TUPLE(it, tuple)
		strcpy((char *)&pinfo->title, tuple->value->cstring);
		NEXT_TUPLE(it, tuple)
		strcpy((char *)&pinfo->message, tuple->value->cstring);
		// Other parts
		NEXT_TUPLE(it, tuple)
		pinfo->vibro = tuple->value->int16;
		if (-1 != pinfo->vibro) {
			root_vibro(pinfo->vibro);
		}
		NEXT_TUPLE(it, tuple)
		pinfo->font = tuple->value->int16;
		pinfo->line = 0;
		pinfo->has_data = true;
		light_enable_interaction();
		persist_write_int(storage_key(info, 2), pinfo->font);
	} else {
		NEXT_TUPLE(it, tuple)
		strcat((char *)&pinfo->title, tuple->value->cstring);
		NEXT_TUPLE(it, tuple)
		strcat((char *)&pinfo->message, tuple->value->cstring);
	}
	persist_write_string(storage_key(info, 0), (char *)pinfo->title);
	persist_write_string(storage_key(info, 1), (char *)pinfo->message);
	root_request_redraw();
}

static void message_on_restore(PB_INFO_P) {
	PINFO(MESSAGE_INFO)
	persist_read_string(storage_key(info, 0), (char *)pinfo->title, 40);
	persist_read_string(storage_key(info, 1), (char *)pinfo->message, 256);
	pinfo->font = persist_read_int(storage_key(info, 2));
	pinfo->line = 0;
	if (strlen(pinfo->message) > 0) {
		pinfo->has_data = true;
	}
}
