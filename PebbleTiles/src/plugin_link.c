#include "ptiles.h"

struct LINK_INFO {
	Layer *layer;
	bool connected;
};

static void link_render(struct Layer *layer, GContext *ctx) {
	LAYERS_EXTRACT_PB_INFO(layer)
	PINFO(LINK_INFO)
	GRect rect = LAYERS_INSTANCE_RECT(info);
	int cw = rect.size.w / 2;
	int ch = rect.size.h / 4;
	int cs = cw;
	if (ch<cw) {
		cs = ch;
	}
	int x = (rect.size.w - 2*cs) / 2;
	int y = (rect.size.h - 4*cs) / 2;
	if (pinfo->connected) {
		// Black on white
		graphics_context_set_stroke_color(ctx, GColorWhite);
		graphics_context_set_fill_color(ctx, GColorBlack);
	} else {
		// White on black
		graphics_context_set_stroke_color(ctx, GColorBlack);
		graphics_context_set_fill_color(ctx, GColorWhite);
	}
	graphics_fill_rect(ctx, GRect(0, 0, rect.size.w, rect.size.h), 0, GCornerNone);
	graphics_draw_line(ctx, GPoint(x+1*cs, y), GPoint(x+1*cs, y+4*cs));
	graphics_draw_line(ctx, GPoint(x+1*cs, y), GPoint(x+2*cs, y+1*cs));
	graphics_draw_line(ctx, GPoint(x+2*cs, y+1*cs), GPoint(x+0*cs, y+3*cs));
	graphics_draw_line(ctx, GPoint(x+0*cs, y+1*cs), GPoint(x+2*cs, y+3*cs));
	graphics_draw_line(ctx, GPoint(x+2*cs, y+3*cs), GPoint(x+1*cs, y+4*cs));
}

static void link_on_start(PB_INFO_P) {
	struct LINK_INFO *pinfo = malloc(sizeof(struct LINK_INFO));
	info->data = pinfo;
	pinfo->connected = bluetooth_connection_service_peek();
}

static void link_on_stop(PB_INFO_P) {
	PINFO(LINK_INFO)
	layers_destroy(pinfo->layer);
	free(info->data);
}

static void link_on_render(PB_INFO_P) {
	PINFO(LINK_INFO)
	Layer *layer = layers_create(LAYERS_INSTANCE_RECT(info), info->layer, &link_render, PB_INFO_P_SIZE);
	layer_add_child(info->layer, layer);
	pinfo->layer = layer;
	LAYERS_SET_PB_INFO(pinfo->layer)
}

static void link_on_connection(PB_INFO_P, bool connected) {
	PINFO(LINK_INFO)
	pinfo->connected = connected;
	if (connected) {
		// Double vibro
		root_vibro(2);
	} else {
		// Long vibro
		root_vibro(1);
	}
	root_request_redraw();
}
