#ifndef _PTILES_DIGITAL_H_
#define _PTILES_DIGITAL_H_

#include "ptiles.h"

struct PB_Area_Info {
	struct Layer *layer;
	GContext *ctx;
	GRect frame;
	bool invert;
	bool bold;
};

void digital_render(struct PB_Area_Info info, const char *text);

#endif
