#include "ptiles.h"
#include "plugin_time.c"
#include "plugin_charge.c"
#include "plugin_countdown.c"
#include "plugin_link.c"
#include "plugin_notify.c"
#include "plugin_alarm.c"
#include "plugin_stopwatch.c"
#include "plugin_message.c"

#include "ptiles_storage.h"

#define PB_PLUGINS_NUM 8

static struct PB_Plugin_Info PB_Plugins[PB_PLUGINS_NUM] = {{
	.tag = "time",
	.can_fullscreen = false,
	.storage_cells = 7,
	.on_start = time_on_start,
	.on_stop = time_on_stop,
	.on_render = time_on_render,
	.on_enter = time_on_enter,
	.on_button = time_on_button,
	.on_message = time_on_message,
	.on_restore = time_on_restore
}, {
	.tag = "stopwatch",
	.can_fullscreen = true,
	.on_start = stop_on_start,
	.on_stop = stop_on_stop,
	.on_render = stop_on_render,
	.on_enter = stop_on_enter,
	.on_button = stop_on_button,
}, {
	.tag = "charge",
	.on_start = charge_on_start,
	.on_stop = charge_on_stop,
	.on_render = charge_on_render,
	.on_battery_state = charge_on_battery,
	.on_message = charge_on_message
}, {
	.tag = "cdown",
	.storage_cells = 4,
	.on_start = cdown_start,
	.on_stop = cdown_stop,
	.on_render = cdown_on_render,
	.on_message = cdown_message,
	.on_button = cdown_button,
	.on_restore = cdown_restore
}, {
	.tag = "link",
	.on_start = link_on_start,
	.on_stop = link_on_stop,
	.on_render = link_on_render,
	.on_connection = link_on_connection
}, {
	.tag = "notify",
	.can_fullscreen = true,
	.on_start = notify_on_start,
	.on_stop = notify_on_stop,
	.on_render = notify_on_render,
	.on_message = notify_on_message,
	.on_button = notify_on_button
}, {
	.tag = "alarm",
	.storage_cells = 5,
	.can_fullscreen = true,
	.on_start = alarm_on_start,
	.on_stop = alarm_on_stop,
	.on_render = alarm_on_render,
	.on_message = alarm_on_message,
	.on_button = alarm_on_button,
	.on_restore = alarm_on_restore
}, {
	.tag = "message",
	.can_fullscreen = true,
	.storage_cells = 3,
	.on_start = message_on_start,
	.on_stop = message_on_stop,
	.on_render = message_on_render,
	.on_message = message_on_message,
	.on_button = message_on_button,
	.on_restore = message_on_restore
}};
static AppTimer *vibro_timer = NULL;
static AppTimer *fullscreen_clock_timer = NULL;
static bool vibro_timer_active;
#define VIBRO_TIMER_DELAY 1500
#define FULLSCREEN_TIMER_DELAY 30

static AppTimer *selection_drop_timer = NULL;
#define SELECTION_AUTO_DROP_SEC 20

static void root_selection_layer(struct Layer *layer, GContext *ctx) {
	GRect rect = layer_get_bounds(layer);
	PB_INFO **ref = layer_get_data(layer);
	// graphics_context_set_fill_color(ctx, GColorBlack);
	// graphics_fill_rect(ctx, rect, 0, GCornerNone);
	graphics_context_set_stroke_color(ctx, GColorWhite);
	if ((*ref)->state == STATE_SELECTED) {
		// Selected - draw border
		graphics_draw_rect(ctx, layer_get_bounds(layer));
	}
	if ((*ref)->state == STATE_ACTIVE) {
		// Selected - draw border
		graphics_draw_rect(ctx, rect);
		graphics_draw_rect(ctx, GRect(rect.origin.x+1, rect.origin.y+1, rect.size.w-2, rect.size.h-2));
	}
}

static void root_fullscreen_layer(struct Layer *layer, GContext *ctx) {
	GRect rect = layer_get_bounds(layer);
	PB_INFO **ref = layer_get_data(layer);
	graphics_context_set_fill_color(ctx, GColorBlack);
	graphics_fill_rect(ctx, rect, 0, GCornerNone);
	graphics_context_set_stroke_color(ctx, GColorWhite);
	graphics_draw_rect(ctx, layer_get_bounds(layer));
}

static void create_instance_layers(PB_INFO_P, Layer *root) {
	info->layer = layers_create(GRect(
				info->bounds.origin.x+2,
				info->bounds.origin.y+2,
				info->bounds.size.w,
				info->bounds.size.h),
			root, &root_selection_layer, sizeof(PB_INFO*));
	PB_INFO **ref = layer_get_data(info->layer);
	*ref = info;
	if (info->plugin->can_fullscreen) {
		info->flayer = layers_create(GRect(1, 20, FULLSCREEN_SIZE+2, FULLSCREEN_SIZE+2), fullscreen_box, &root_fullscreen_layer, sizeof(PB_INFO*));
		ref = layer_get_data(info->flayer);
		*ref = info;
		layer_set_hidden(info->flayer, true);
	}
}

static void drop_instance_layers(PB_INFO_P) {
	layers_destroy(info->layer);
	layers_destroy(info->flayer);
}

static void stop_instances() {
	if (NULL != selection_drop_timer) {
		// Stop timer
		app_timer_cancel(selection_drop_timer);
		selection_drop_timer = NULL;
	}
	for (unsigned int i = 0; i < PB_Instance_num; i++) {
		PB_INFO_P = PB_Instances[i];
		if (NULL != info->plugin->on_stop) {
			// Have on_stop handler
			info->plugin->on_stop(info);
		}
		drop_instance_layers(info);
		storage_clear_instance(info, i);
		free(info);
	}
	PB_Instance_num = 0;
	selected_instance = -1;
	selection_state = SELECTION_HIDDEN;
	storage_clear();
}

#define IN_BOUNDS(WHAT, MAX) if (WHAT>=0 && (unsigned int)WHAT<MAX)

static void root_fullscreen_on_timer(void *data) {
	STOP_TIMER(fullscreen_clock_timer)
	root_request_redraw();
	fullscreen_clock_timer = app_timer_register(FULLSCREEN_TIMER_DELAY * 1000, (AppTimerCallback)&root_fullscreen_on_timer, NULL);
}

static void root_refresh_layer(PB_INFO_P) {
	bool fullscreen = false;
	if (info->flayer != NULL) {
		// Have fullscreen
		fullscreen = info->state == STATE_ACTIVE;
		layer_set_hidden(info->flayer, !fullscreen);
	}
	layer_set_hidden(fullscreen_info, !fullscreen);
	root_request_redraw();
}

static void root_enter_selection() {
	IN_BOUNDS(selected_instance, PB_Instance_num) {
		PB_INFO_P = PB_Instances[selected_instance];
		info->state = STATE_ACTIVE;
		selection_state = SELECTION_SELECTED;
		if (NULL != info->plugin->on_enter) {
			info->plugin->on_enter(info);
		}
		root_refresh_layer(info);
	}
}

static void root_instance_button(ButtonId btn) {
	IN_BOUNDS(selected_instance, PB_Instance_num) {
		PB_INFO_P = PB_Instances[selected_instance];
		if (NULL != info->plugin->on_button) {
			info->plugin->on_button(info, btn);
			root_request_redraw();
		}
	}
}

static void root_on_drop_selection_timer(void *data) {
	selection_drop_timer = NULL;
	if (selection_state != SELECTION_VISIBLE) {
		// Other than selected
		return;
	}
	IN_BOUNDS(selected_instance, PB_Instance_num) {
		// We have selected instance
		PB_Instances[selected_instance]-> state = STATE_NOT_SELECTED;
		root_refresh_layer(PB_Instances[selected_instance]);
		selection_state = SELECTION_HIDDEN;
	}
}

static int root_instance_index(uint32_t id) {
	for (unsigned int i = 0; i < PB_Instance_num; i++) {
		PB_INFO_P = PB_Instances[i];
		if (info->id == id) {
			return (int)i;
		}
	}
	return -1;
}

bool root_request_select(PB_INFO_P) {
	int index = root_instance_index(info->id);
	if (-1 == index) {
		// Not found
		return false;
	}
	IN_BOUNDS(selected_instance, PB_Instance_num) {
		PB_Instances[selected_instance]-> state = STATE_NOT_SELECTED;
		root_refresh_layer(PB_Instances[selected_instance]);
	}
	selected_instance = index;
	root_enter_selection();
	return true;
}

static void root_move_selection(int inc) {
	IN_BOUNDS(selected_instance, PB_Instance_num) {
		PB_Instances[selected_instance]-> state = STATE_NOT_SELECTED;
		root_refresh_layer(PB_Instances[selected_instance]);
	}
	selected_instance += inc;
	if (selection_state == SELECTION_HIDDEN && selected_instance == -1) {
		// First selection = move to top
		selected_instance = 0;
	}
	if (selected_instance<0) {
		selected_instance = PB_Instance_num-1;
	}
	if (selected_instance >= (int)PB_Instance_num) {
		selected_instance = 0;
	}
	selection_state = SELECTION_HIDDEN;
	IN_BOUNDS(selected_instance, PB_Instance_num) {
		PB_Instances[selected_instance] -> state = STATE_SELECTED;
		root_refresh_layer(PB_Instances[selected_instance]);
		selection_state = SELECTION_VISIBLE;
		if (NULL != selection_drop_timer) {
			// Stop timer
			app_timer_cancel(selection_drop_timer);
		}
		selection_drop_timer = app_timer_register(1000 * SELECTION_AUTO_DROP_SEC, &root_on_drop_selection_timer, NULL);
	}
}

bool root_request_focus(PB_INFO_P) {
	if (selection_state == SELECTION_SELECTED) {
		return false;
	}

	int index = root_instance_index(info->id);
	if (-1 == index) {
		// Not found
		return false;
	}
	IN_BOUNDS(selected_instance, PB_Instance_num) {
		// We have selected instance
		PB_Instances[selected_instance]-> state = STATE_NOT_SELECTED;
		root_refresh_layer(PB_Instances[selected_instance]);
		selection_state = SELECTION_HIDDEN;
	}
	selected_instance = index;
	root_move_selection(0);
	return true;
}

bool root_exit_selection(PB_INFO_P) {
	int index = root_instance_index(info->id);
	if (-1 == index || selected_instance != index) {
		// Not found or other selected
		return false;
	}
	if (selection_state != SELECTION_SELECTED) {
		return false;
	}
	root_move_selection(0);
	return true;
}

bool root_send_message(int type, Tuplet data[], int size) {
	Tuplet type_tup = TupletInteger(KEY_CMD, type);
	DictionaryIterator *it;
	app_message_outbox_begin(&it);
	if (it == NULL) {
		// Failed
		return false;
	}
	dict_write_tuplet(it, &type_tup);
	for (int i = 0; i < size; i++) {
		dict_write_tuplet(it, &data[i]);
	}
	dict_write_end(it);
	app_message_outbox_send();
	return true;
}

Tuple* root_next_tuple(DictionaryIterator *iterator, Tuple *tuple, uint32_t id) {
	Tuple *t = dict_read_first(iterator);
	uint32_t lookFor = id;
	if (NULL != tuple) {
		// Look for next ID
		lookFor = tuple->key + 1;
	}
	uint32_t found = 0;
	Tuple *foundT = NULL;
	while (NULL != t) {
		if (t->key == lookFor) {
			// Found
			return t;
		}
		if (t->key > lookFor) {
			// At least it's bigger
			if ((found == 0) || (t->key < found)) {
				// Closer
				found = t->key;
				foundT = t;
			}
		}
		t = dict_read_next(iterator);
	}
	return foundT;
}

static void on_incoming_message(DictionaryIterator *iterator, void *context) {
	Tuple *tuple = root_next_tuple(iterator, NULL, 0);
	CHECK_TUPLE
	if (tuple->key != KEY_CMD) {
		// Invalid first key
		return;
	}
	if (tuple->value->uint32 == KEY_CMD_RESET) {
		stop_instances(); // Stop all existing
		PB_Instance_num = 0;
		return;
	}
	if (tuple->value->uint32 == KEY_CMD_INSTANCE_CMD) {
		// Instance message
		tuple = root_next_tuple(iterator, tuple, 0);
		CHECK_TUPLE
		uint32_t id = tuple->value->uint32;
		for (unsigned int i = 0; i < PB_Instance_num; i++) {
			PB_INFO_P = PB_Instances[i];
			if (info->id == id && info->plugin->on_message) {
				// Found
				info->plugin->on_message(info, iterator);
				return;
			}
		}
		return;
	}
	if (tuple->value->uint32 == KEY_CMD_INSTANCE) {
		// New instance
		struct PB_Plugin_Info *plugin;
		// Create instance
		tuple = root_next_tuple(iterator, tuple, 0);
		CHECK_TUPLE
		uint32_t id = tuple->value->uint32;
		tuple = root_next_tuple(iterator, tuple, 0);
		CHECK_TUPLE
		bool found = false;
		for (int j = 0; j < PB_PLUGINS_NUM; j++) {
			// Search plugin by name
			if (strcmp(tuple->value->cstring, PB_Plugins[j].tag) == 0) {
				// Found
				plugin = &PB_Plugins[j];
				found = true;
				break;
			}
		}
		if (!found) {
			// Not found
			return;
		}
		struct PB_Instance_Info *instance = malloc(sizeof(struct PB_Instance_Info));
		instance->plugin = plugin;
		instance->id = id;
		instance->state = STATE_NOT_SELECTED;
		instance->layer = NULL;
		instance->flayer = NULL;
		instance->offline = false;
		read_grect(&instance->bounds, iterator, tuple);
		PB_Instances[PB_Instance_num++] = instance;
		if (NULL != plugin->on_start) {
			// Call on_start
			plugin->on_start(instance);
		}
		create_instance_layers(instance, black_box);
		if (NULL != plugin->on_render) {
			// Call on_start
			plugin->on_render(instance);
		}
		storage_save_instance(instance, PB_Instance_num-1);
	}
}

static void root_fullscreen_layer_op(struct Layer *layer, GContext *ctx) {
}

char timeBuffer[9];

static void root_fullscreen_info_op(struct Layer *layer, GContext *ctx) {
	layers_black_box(layer, ctx);
	time_t tm = time(NULL);
	tm = (time_t) ((int)tm+FULLSCREEN_TIMER_DELAY);
	strftime((char *)&timeBuffer, 9, "%H:%M", localtime(&tm));
	graphics_draw_text(ctx, (char *)&timeBuffer, fonts_get_by_index(1), layer_get_bounds(layer), GTextOverflowModeTrailingEllipsis, GTextAlignmentCenter, NULL);
}

static void root_restore_instances() {
	unsigned int instances = storage_instances();
	char tag_buffer[64];
	for (unsigned int i = 0; i < instances; i++) {
		struct PB_Plugin_Info *plugin;
		bool found = false;
		if (!storage_read_plugin((char *)&tag_buffer, 64, i)) {
			continue;
		}
		for (int j = 0; j < PB_PLUGINS_NUM; j++) {
			// Search plugin by name
			if (strcmp((char *)&tag_buffer, PB_Plugins[j].tag) == 0) {
				// Found
				plugin = &PB_Plugins[j];
				found = true;
				break;
			}
		}
		if (!found) {
			// Not found
			return;
		}
		struct PB_Instance_Info *instance = malloc(sizeof(struct PB_Instance_Info));
		instance->plugin = plugin;
		instance->state = STATE_NOT_SELECTED;
		instance->layer = NULL;
		instance->flayer = NULL;
		instance->offline = true;
		storage_read_instance(instance, i);
		PB_Instances[PB_Instance_num++] = instance;
		if (NULL != plugin->on_start) {
			// Call on_start
			plugin->on_start(instance);
		}
		create_instance_layers(instance, black_box);
		if (NULL != plugin->on_render) {
			// Call on_start
			plugin->on_render(instance);
		}
		if (NULL != plugin->on_restore) {
			// Call on_restore
			plugin->on_restore(instance);
		}
	}
}

static void window_load(Window *window) {
	window_layer = window_get_root_layer(window);
	GRect bounds = layer_get_bounds(window_layer);
	black_box = layers_create(bounds, window_layer, &layers_black_box, 0);
	fullscreen_box = layers_create(bounds, window_layer, &root_fullscreen_layer_op, 0);
	fullscreen_info = layers_create(GRect(0, 0, bounds.size.w, 20), fullscreen_box, &root_fullscreen_info_op, 0);
	layer_set_hidden(fullscreen_info, true);
	root_restore_instances();
	root_send_message(KEY_CMD_REQUEST_RESET, NULL, 0);
	root_fullscreen_on_timer(NULL);
}

static void window_unload(Window *window) {
	layer_destroy(black_box);
	layer_destroy(fullscreen_box);
	layer_destroy(fullscreen_info);
	STOP_TIMER(fullscreen_clock_timer)
}

static void single_button_handler(ClickRecognizerRef recognizer, void *data) {
	ButtonId btn = click_recognizer_get_button_id(recognizer);
	if (vibro_timer_active) {
		// Any key press - stop vibro
		vibro_timer_active = false;
		return;
	}
	if (selection_state == SELECTION_HIDDEN) {
		// Hidden - show selection
		root_move_selection(0);
		return;
	}
	if (selection_state == SELECTION_VISIBLE) {
		// Navigation
		if (btn == BUTTON_ID_UP) {
			root_move_selection(-1);
		}
		if (btn == BUTTON_ID_DOWN) {
			root_move_selection(1);
		}
		if (btn == BUTTON_ID_SELECT) {
			root_enter_selection();
		}
	} else {
		root_instance_button(btn);
	}
}

#define LONG_CLICK_MSEC 700

static void long_button_handler(ClickRecognizerRef *ref, void *data) {
	ButtonId btn = click_recognizer_get_button_id(ref);
	if (selection_state == SELECTION_SELECTED) {
		// Stop selection
		PB_INFO_P = PB_Instances[selected_instance];
		selection_state = SELECTION_VISIBLE;
		info->state = STATE_SELECTED;
		root_refresh_layer(info);
		if (btn == BUTTON_ID_UP) {
			root_move_selection(-1);
		}
		if (btn == BUTTON_ID_DOWN) {
			root_move_selection(1);
		}
		if (btn == BUTTON_ID_SELECT) {
			root_move_selection(0);
		}
	}
}

static void clicks_provider(Window *window) {
	window_single_click_subscribe(BUTTON_ID_DOWN, &single_button_handler);
	window_single_click_subscribe(BUTTON_ID_UP, &single_button_handler);
	window_single_click_subscribe(BUTTON_ID_SELECT, &single_button_handler);
	window_long_click_subscribe(BUTTON_ID_DOWN, LONG_CLICK_MSEC, (ClickHandler)&long_button_handler, NULL);
	window_long_click_subscribe(BUTTON_ID_UP, LONG_CLICK_MSEC, (ClickHandler)&long_button_handler, NULL);
	window_long_click_subscribe(BUTTON_ID_SELECT, LONG_CLICK_MSEC, (ClickHandler)&long_button_handler, NULL);
}

static void root_battery_listener(BatteryChargeState charge) {
	for (unsigned int i = 0; i < PB_Instance_num; i++) {
		PB_INFO_P = PB_Instances[i];
		if (NULL != info->plugin->on_battery_state) {
			// Have on_battery_state handler
			info->plugin->on_battery_state(info, charge.charge_percent, charge.is_charging, charge.is_plugged);
		}
	}
}

void root_request_battery_state(PB_INFO_P) {
	BatteryChargeState charge = battery_state_service_peek();
	if (NULL != info->plugin->on_battery_state) {
		// Have on_battery_state handler
		info->plugin->on_battery_state(info, charge.charge_percent, charge.is_charging, charge.is_plugged);
	}

}

Tuplet root_request_instance_data(PB_INFO_P) {
	return TupletInteger(KEY_ID, (int)info->id);
}

static void root_connection_listener(bool connected) {
	for (unsigned int i = 0; i < PB_Instance_num; i++) {
		PB_INFO_P = PB_Instances[i];
		if (NULL != info->plugin->on_connection) {
			// Have handler
			info->plugin->on_connection(info, connected);
		}
	}
}

static void init(void) {
	app_message_open(1024, app_message_outbox_size_maximum());
	app_message_register_inbox_received(&on_incoming_message);
	window = window_create();
	window_set_fullscreen(window, true);
	window_set_window_handlers(window, (WindowHandlers) {
		.load = window_load,
		.unload = window_unload,
	});
	window_stack_push(window, false); // Animated
	window_set_click_config_provider(window, (ClickConfigProvider) &clicks_provider);
	battery_state_service_subscribe(&root_battery_listener);
	bluetooth_connection_service_subscribe(&root_connection_listener);
}

static void deinit(void) {
  window_destroy(window);
  app_message_deregister_callbacks();
}

int main(void) {
  init();
  app_event_loop();
  deinit();
}

int root_smart_timer(int sec) {
	if (sec>6000) {
		return 60*20;
	}
	if (sec>600) {
		return 60*5;
	}
	if (sec>120) {
		return 60;
	}
	if (sec>60) {
		return 30;
	}
	if (sec>30) {
		return 10;
	}
	if (sec>15) {
		return 5;
	}
	if (sec>5) {
		return 2;
	}
	return 1;
}


static void vibro_on_timer(void *data) {
	if (vibro_timer_active) {
		root_vibro(CDOWN_VIBRO_LONG);
		vibro_timer = app_timer_register(VIBRO_TIMER_DELAY, (AppTimerCallback)&vibro_on_timer, data);
	}
}

void root_vibro(int type) {
	if (type < 0) {
		// No vibro
		return;
	}
	uint32_t segments[1] = {type};
	VibePattern pat = {
	  .durations = segments,
	  .num_segments = ARRAY_LENGTH(segments)
	};
	switch (type) {
		case CDOWN_VIBRO_SHORT:
			vibes_short_pulse();
			break;
		case CDOWN_VIBRO_LONG:
			vibes_long_pulse();
			break;
		case CDOWN_VIBRO_DOUBLE:
			vibes_double_pulse();
			break;
		case CDOWN_VIBRO_REPEAT:
			// Run vibro timer
			TIMER_STOP(vibro_timer)
			vibro_timer_active = true;
			vibro_on_timer(NULL);
			break;
		default:
			// Custom
			vibes_enqueue_custom_pattern(pat);
	}
}
void root_request_redraw() {
	layer_mark_dirty(window_layer);
}
