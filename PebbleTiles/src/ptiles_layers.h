#ifndef _PTILES_LAYERS_H_
#define _PTILES_LAYERS_H_

#define LAYERS_GAP 2
#define FONTS_TOP_GAP -3
#define LAYERS_INSTANCE_RECT(info) GRect(LAYERS_GAP, LAYERS_GAP, info->bounds.size.w-2*LAYERS_GAP, info->bounds.size.h-2*LAYERS_GAP)


#include "ptiles.h"

struct PB_Font_Info {
	int height;
	int width_x_10;
	const char *name;
};

void read_grect(GRect *frame, DictionaryIterator *iterator, Tuple *tuple);


void layers_black_box(struct Layer *layer, GContext *ctx);

Layer* layers_create(GRect frame, Layer *parent, LayerUpdateProc update_proc, size_t sz);

void layers_destroy(Layer *layer);

void init_text_layer(TextLayer *layer, struct PB_Font_Info *info);

int fonts_find_font(GSize size, int chars, int lines);

int fonts_center_layer_y(int height, int lines, struct PB_Font_Info *info);

GFont fonts_get_by_index(int index);

int fonts_height_by_index(int index);
int fonts_width_by_index(int index, int chars);
#endif
