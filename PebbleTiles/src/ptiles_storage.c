#include "ptiles_storage.h"

bool storage_save_instance(PB_INFO_P, unsigned int position) {
  persist_write_int(position+1, info->id);
  persist_write_string(info->id*INSTANCE_MUL, info->plugin->tag);
  persist_write_int(info->id*INSTANCE_MUL+1, info->bounds.origin.x);
  persist_write_int(info->id*INSTANCE_MUL+2, info->bounds.origin.y);
  persist_write_int(info->id*INSTANCE_MUL+3, info->bounds.size.w);
  persist_write_int(info->id*INSTANCE_MUL+4, info->bounds.size.h);
  persist_write_int(0, position+1); // How many we have
  return false;
}

uint32_t storage_key(PB_INFO_P, uint32_t key) {
  return info->id*INSTANCE_MUL+INSTANCE_SKIP+key;
}

bool storage_clear_instance(PB_INFO_P, unsigned int position) {
  if (!persist_exists(position+1)) {
    return false;
  }
  persist_delete(position+1);
  for (unsigned int i = 0; i < INSTANCE_SKIP+info->plugin->storage_cells; i++) {
    if (persist_exists(info->id*INSTANCE_MUL+i)) {
      persist_delete(info->id*INSTANCE_MUL+i);
    }
  }
  return true;
}

bool storage_clear() {
  persist_write_int(0, 0);
  return true;
}

unsigned int storage_instances() {
  return persist_read_int(0);
}

bool storage_read_plugin(char *buffer, int size, unsigned int position) {
  uint32_t id = persist_read_int(position+1);
  int bytes = persist_read_string(id*INSTANCE_MUL, buffer, size);
  return bytes != E_DOES_NOT_EXIST;
}

bool storage_read_instance(PB_INFO_P, unsigned int position) {
  info->id = persist_read_int(position+1);
  info->bounds.origin.x = persist_read_int(info->id*INSTANCE_MUL+1);
  info->bounds.origin.y = persist_read_int(info->id*INSTANCE_MUL+2);
  info->bounds.size.w = persist_read_int(info->id*INSTANCE_MUL+3);
  info->bounds.size.h = persist_read_int(info->id*INSTANCE_MUL+4);
  return true;
}
