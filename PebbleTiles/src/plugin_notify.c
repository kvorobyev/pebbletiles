#include "ptiles.h"
#include "ptiles_layers.h"

struct NOTIFY_INFO {
	AppTimer *timer;
	Layer *layer;
	Layer *flayer;
	bool has_data;
	char title[41];
	char message[256];
	int vibro;
};

static void notify_on_start(PB_INFO_P) {
	struct NOTIFY_INFO *pinfo = malloc(sizeof(struct NOTIFY_INFO));
	info->data = pinfo;
	pinfo->timer = NULL;
	pinfo->has_data = false;
}

static void notify_on_stop(PB_INFO_P) {
	PINFO(NOTIFY_INFO)
	TIMER_STOP(pinfo->timer)
	layers_destroy(pinfo->layer);
	layers_destroy(pinfo->flayer);
	free(info->data);
}

static void notify_render(struct Layer *layer, GContext *ctx) {
	LAYERS_EXTRACT_PB_INFO(layer)
	GRect rect = layer_get_bounds(layer);
	PINFO(NOTIFY_INFO)
	if ((!pinfo->has_data) && (info->state != STATE_ACTIVE)) {
		return; // Nothing to render
	}
	graphics_context_set_fill_color(ctx, GColorWhite);
	graphics_context_set_text_color(ctx, GColorBlack);
	graphics_fill_rect(ctx, GRect(1, 1, rect.size.w-2, rect.size.h-2), 0, GCornerNone);
	int title_height = fonts_height_by_index(1);
	graphics_draw_text(ctx, (char *)&pinfo->title, fonts_get_by_index(1), GRect(2, FONTS_TOP_GAP, rect.size.w-4, title_height), GTextOverflowModeTrailingEllipsis, GTextAlignmentLeft, NULL);
	graphics_draw_text(ctx, (char *)&pinfo->message, fonts_get_by_index(0), GRect(2, FONTS_TOP_GAP+title_height, rect.size.w-4, rect.size.h - title_height), GTextOverflowModeTrailingEllipsis, GTextAlignmentLeft, NULL);
}

static void notify_on_button(PB_INFO_P, ButtonId btn) {
	PINFO(NOTIFY_INFO)
	if (btn == BUTTON_ID_SELECT) {
		pinfo->has_data = false;
		root_exit_selection(info);
		Tuplet data[] = {
			root_request_instance_data(info)
		};
		root_send_message(KEY_CMD_INSTANCE_REQUEST_CMD, data, 1);
	}
	root_request_redraw();
}

static void notify_on_render(PB_INFO_P) {
	PINFO(NOTIFY_INFO)
	GRect rect = LAYERS_INSTANCE_RECT(info);
	{
		pinfo->layer = layers_create(rect, info->layer, &notify_render, PB_INFO_P_SIZE);
		LAYERS_SET_PB_INFO(pinfo->layer)
		layer_add_child(info->layer, pinfo->layer);
	}
	{
		pinfo->flayer = layers_create(FULLSCREEN_RECT, info->flayer, &notify_render, PB_INFO_P_SIZE);
		LAYERS_SET_PB_INFO(pinfo->flayer)
		layer_add_child(info->flayer, pinfo->flayer);
	}
}

static void notify_on_timer(PB_INFO_P) {
	PINFO(NOTIFY_INFO)
	pinfo->timer = NULL;
	if (info->state == STATE_ACTIVE) {
		// Currently in active mode - do not hide notifications
		return;
	}
	pinfo->has_data = false;
	root_exit_selection(info);
	root_request_redraw();
}

static void notify_on_message(PB_INFO_P, DictionaryIterator *it) {
	PINFO(NOTIFY_INFO)
	FIRST_TUPLE(it, tuple)
	int read_type = tuple->value->int16;
	if (read_type == 0) {
		// First part
		TIMER_STOP(pinfo->timer)
		NEXT_TUPLE(it, tuple)
		strcpy((char *)&pinfo->title, tuple->value->cstring);
		NEXT_TUPLE(it, tuple)
		strcpy((char *)&pinfo->message, tuple->value->cstring);
		if (strlen(pinfo->message) == 0 && strlen(pinfo->title) == 0) {
			// Hide message
			pinfo->has_data = false;
			if (info->state == STATE_ACTIVE) {
				// Currently in active mode - exit selection
				root_exit_selection(info);
			}
			root_request_redraw();
			return;
		}
		// Other parts
		NEXT_TUPLE(it, tuple)
		pinfo->vibro = tuple->value->int16;
		pinfo->has_data = true;
		if (-1 != pinfo->vibro) {
			root_vibro(pinfo->vibro);
		}
		NEXT_TUPLE(it, tuple)
		int auto_hide = tuple->value->int16;
		if (auto_hide>0) {
			// Start auto-hide timer
			pinfo->timer = app_timer_register(auto_hide*1000, (AppTimerCallback)&notify_on_timer, info);
		}
		light_enable_interaction();
		// root_request_select(info);
		root_request_focus(info);
		WITH_TUPLE(it, tuple) {
			bool show_fullscreen = tuple->value->int16 > 0;
			if (show_fullscreen) {
				root_request_select(info);
			}
		}
	} else {
		NEXT_TUPLE(it, tuple)
		strcat((char *)&pinfo->title, tuple->value->cstring);
		NEXT_TUPLE(it, tuple)
		strcat((char *)&pinfo->message, tuple->value->cstring);
	}
	root_request_redraw();
}
