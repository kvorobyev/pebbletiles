#ifndef _PTILES_STORAGE_H_
#define _PTILES_STORAGE_H_

#include "ptiles.h"

#define INSTANCE_MUL 100
#define INSTANCE_SKIP 10

#define INSTANCE_MAX 32

bool storage_save_instance(PB_INFO_P, unsigned int position);
uint32_t storage_key(PB_INFO_P, uint32_t key);
bool storage_clear_instance(PB_INFO_P, unsigned int position);
bool storage_clear();

unsigned int storage_instances();
bool storage_read_plugin(char *buffer, int size, unsigned int position);
bool storage_read_instance(PB_INFO_P, unsigned int position);
#endif
